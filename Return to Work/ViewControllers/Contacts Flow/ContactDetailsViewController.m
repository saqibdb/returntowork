//
//  ContactDetailsViewController.m
//  Return to Work
//
//  Created by iBuildx-Macbook on 08/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "ContactDetailsViewController.h"
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "UserModel.h"
#import "UserDetails.h"

@import Firebase;
@import FirebaseAuth;

@interface ContactDetailsViewController ()

@property (nonatomic, weak) IBOutlet UIView *borderVw;

@end

@implementation ContactDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self basicLayout];
    [self addShadow];
}

-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}

-(void)basicLayout {
    
    [self.contactSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
    [self createNavigationTitleViewWithTitle:@"Contacts"];
    
    self.mainTypeImageView.image = self.selectedMainContact.icon;
    self.mainTypeName.text = self.selectedMainContact.name;
    self.mainTypeSubtitle.text = [NSString stringWithFormat:@"%@ Contacts",self.selectedMainContact.name];
    
    //    self.contactNameTxt.text = self.selectedContact.name;
    //    self.contactAddressTxt.text = self.selectedContact.address;
    //    self.contactEmailTxt.text = self.selectedContact.email;
    //    self.contactPhoneTxt.text = self.selectedContact.phone;
    
    self.contactNameTxt.text = self.contactDetails.contactName;
    self.contactAddressTxt.text = self.contactDetails.contactAddress;
    self.contactEmailTxt.text = self.contactDetails.contactEmail;
    self.contactPhoneTxt.text = self.contactDetails.contactPhone;
    
    
    [self.contactSegment setHidden:!self.isFromtblSel];
    
    
    [self textFieldActivity:false];
    if (self.isFromtblSel == false) {
        [self textFieldActivity:true];
        [self.btnName setHidden:!self.isFromtblSel];
        [self.btnAddress setHidden:!self.isFromtblSel];
        [self.btnEmail setHidden:!self.isFromtblSel];
        [self.btnPhone setHidden:!self.isFromtblSel];
    }
}


-(void)textFieldActivity:(BOOL*)isEditable {

    self.contactNameTxt.userInteractionEnabled = isEditable;
    self.contactNameTxt.enabled = isEditable;
    self.contactAddressTxt.userInteractionEnabled = isEditable;
    self.contactAddressTxt.enabled = isEditable;
    self.contactEmailTxt.userInteractionEnabled = isEditable;
    self.contactEmailTxt.enabled = isEditable;
    self.contactPhoneTxt.userInteractionEnabled = isEditable;
    self.contactPhoneTxt.enabled = isEditable;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(20, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 100, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 100, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}




- (IBAction)contactEditAction:(UIButton *)sender {
    self.contactNameTxt.enabled = YES;
    [self.contactNameTxt becomeFirstResponder];
    self.contactNameTxt.userInteractionEnabled = YES;

}

- (IBAction)addressEditAction:(UIButton *)sender {
    self.contactAddressTxt.enabled = YES;
    [self.contactAddressTxt becomeFirstResponder];
    self.contactAddressTxt.userInteractionEnabled = YES;

}

- (IBAction)emailEditAction:(UIButton *)sender {
    self.contactEmailTxt.enabled = YES;
    [self.contactEmailTxt becomeFirstResponder];
    self.contactEmailTxt.userInteractionEnabled = YES;

}

- (IBAction)phoneEditAction:(UIButton *)sender {
    self.contactPhoneTxt.enabled = YES;
    [self.contactPhoneTxt becomeFirstResponder];
    self.contactPhoneTxt.userInteractionEnabled = YES;

}

- (IBAction)contactSegmentChanged:(UISegmentedControl *)sender {
    NSLog(@"Selected Index is %li" , (long)sender.selectedSegmentIndex);
    
    if (sender.selectedSegmentIndex == 0) {
        NSString *phNo = self.contactDetails.contactPhone;
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            if (@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:phoneUrl options:@{} completionHandler:nil];
            } else {
                // Fallback on earlier versions
                [[UIApplication sharedApplication] openURL:phoneUrl];
            }
            
        } else {
            UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [calert show];
        }
    }
    else if(sender.selectedSegmentIndex == 1){
        [self sendSMS];
    }
    else if(sender.selectedSegmentIndex == 2){
        [self launchMailAppOnDevice];
    }
    [self.contactSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
}

- (IBAction)saveAction:(UIButton *)sender {
    
    [self addContactWithName:self.contactNameTxt.text withAddress:self.contactAddressTxt.text withEmail:self.contactEmailTxt.text withPhone:self.contactPhoneTxt.text];
}


//Update Contact

-(void)addContactWithName:(NSString*)name withAddress:(NSString*)address withEmail:(NSString*)email withPhone:(NSString*)phone {
    
    
    if (self.isFromtblSel) {
        FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Contact"] child:@"Contact"];
        FIRDatabaseReference* contactRef = [correspondenceRef child:self.contactDetails.contactId];

        NSDictionary *requestDict = @{@"contactName": name,
                                      @"contactAddress": address,
                                      @"contactEmail": email,
                                      @"contactPhone": phone
                                      };

        
        [SVProgressHUD showWithStatus:@"Saving..."];
        [contactRef updateChildValues:requestDict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            [SVProgressHUD dismiss];
            if (error) {
                [self showSuccessAlert:false];
            } else {
                [self showSuccessAlert:true];
            }
        }];
    } else {
        FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Contact"] child:@"Contact"];
        FIRDatabaseReference* contactRef = [correspondenceRef childByAutoId];
        UserModel* userData = [UserDetails fetchUserData];
        NSDictionary *requestDict = @{@"contactName": name,
                                      @"contactAddress": address,
                                      @"contactEmail": email,
                                      @"contactPhone": phone,
                                      @"contactId" : contactRef.key,
                                      @"type" : self.selectedMainContact.name,
                                      @"userId": userData.userId
                                      };
        
        [SVProgressHUD showWithStatus:@"Saving..."];
        [contactRef setValue:requestDict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            [SVProgressHUD dismiss];
            if (error) {
                [self showSuccessAlert:false];
            } else {
                [self showSuccessAlert:true];
            }
        }];
    }
}

-(void)showSuccessAlert:(Boolean)success{
    
    NSString *title = @"Success";
    NSString *message = @"Contact updated successfully";
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok;
    
    if(!success){
        title = @"Failure";
        message = @"There was an error in updating the contact";
        ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
    }
    else{
        ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    
    
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}


-(void)sendSMS {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc]init];
    
    if([MFMessageComposeViewController canSendText]) {
        
        controller.body = @"";
        controller.recipients = [NSArray arrayWithObjects:self.contactDetails.contactPhone, nil];
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    }
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    if (result == MessageComposeResultCancelled)
        NSLog(@"Message cancelled");
    else if (result == MessageComposeResultSent)
        NSLog(@"Message sent");
    else
        NSLog(@"Message failed");
}


-(void)launchMailAppOnDevice {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController * mail = [[MFMailComposeViewController alloc]init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"Subject"];
        [mail setMessageBody:@"body" isHTML:nil];
        [mail setToRecipients:@[self.contactDetails.contactEmail]];
        [self presentViewController:mail animated:YES completion:NULL];
        
    }
    else {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[@"mailto://" stringByAppendingString:self.contactDetails.contactEmail]]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"mailto://" stringByAppendingString:self.contactDetails.contactEmail]]];
        }
        else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cannot send email" message:@"Email is not available. Please check your settings." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:true completion:nil];
        }
        NSLog(@"This device cannot send email");
    }
}




- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
