//
//  ContactDetailsViewController.h
//  Return to Work
//
//  Created by iBuildx-Macbook on 08/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"
#import <MessageUI/MessageUI.h>

@interface ContactDetailsViewController : UIViewController<MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *mainTypeImageView;
@property (weak, nonatomic) IBOutlet UILabel *mainTypeName;
@property (weak, nonatomic) IBOutlet UILabel *mainTypeSubtitle;

@property (weak, nonatomic) IBOutlet UITextField *contactNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *contactAddressTxt;
@property (weak, nonatomic) IBOutlet UITextField *contactEmailTxt;
@property (weak, nonatomic) IBOutlet UITextField *contactPhoneTxt;





@property (weak, nonatomic) IBOutlet UISegmentedControl *contactSegment;


@property (nonatomic) Contact *selectedContact;

- (IBAction)contactEditAction:(UIButton *)sender;
- (IBAction)addressEditAction:(UIButton *)sender;
- (IBAction)emailEditAction:(UIButton *)sender;
- (IBAction)phoneEditAction:(UIButton *)sender;

@property (nonatomic ,assign) IBOutlet UIButton *btnName;
@property (assign) IBOutlet UIButton *btnAddress;
@property (assign) IBOutlet UIButton *btnEmail;
@property (assign) IBOutlet UIButton *btnPhone;


- (IBAction)contactSegmentChanged:(UISegmentedControl *)sender;
- (IBAction)saveAction:(UIButton *)sender;

@property (nonatomic, strong) Contact *contactDetails;
@property (nonatomic) Contact *selectedMainContact;
@property (nonatomic, assign) BOOL isFromtblSel;
@property (nonatomic, strong) NSString *type;


@end
