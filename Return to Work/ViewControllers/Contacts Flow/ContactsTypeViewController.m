//
//  ContactsTypeViewController.m
//  Return to Work
//
//  Created by iBuildx-Macbook on 08/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "ContactsTypeViewController.h"
#import "ContactsTableViewCell.h"
#import "ContactsViewController.h"
#import "CorrespondenceAPIManager.h"
#import "Contact.h"
#import "UserDetails.h"
#import "UserModel.h"



@interface ContactsTypeViewController (){
    NSMutableArray *allContacts;
}

@property (nonatomic, strong) UserModel* userData;

@end

@implementation ContactsTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createNavigationTitleViewWithTitle:@"Contacts"];
    
    [self addDummyContacts];
    
    self.userData = [UserDetails fetchUserData];
    
    
    self.maintableView.delegate = self;
    self.maintableView.dataSource = self;
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self fetchAllContacts];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchAllContacts {
    [CorrespondenceAPIManager fetchAllContactWithCompletion:^(ContactList *contacts) {
        self.contactList = contacts;
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return allContacts.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsTableViewCell"];
    Contact *contact = allContacts[indexPath.row];
    [cell addDataOnUI:contact];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    self.selectedContact = allContacts[indexPath.row];
    Contact *contact = allContacts[indexPath.row];
    [self checkContactType:contact.name];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}


-(void)addDummyContacts {
    allContacts = [[NSMutableArray alloc] init];
    Contact *newContact = [[Contact alloc] initWithName:@"Employer" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"employerIcon"]];
    [allContacts addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Rehabilitation Provider" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"rehabilitationIcon"]];
    [allContacts addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Insurance Company" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"insuranceIcon"]];
    [allContacts addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Nominated Treating Doctor" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"nominatedIcon"]];
    [allContacts addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Physiotherapist" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"PhysiotherapistIcon"]];
    [allContacts addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Psychologist" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"psychologistIcon"]];
    [allContacts addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Other" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"otherIcon"]];
    [allContacts addObject:newContact];
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(20, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 100, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 100, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}


-(void)checkContactType:(NSString*)name {
    
    NSMutableArray* contacts = [[NSMutableArray alloc] init];
    
    for(int index = 0; index < self.contactList.contactArr.count; index++) {
        Contact* contact = self.contactList.contactArr[index];
        if ([contact.type  isEqual:name]) {
            [contacts addObject:contact];
        }
    }
    
    ContactsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactsViewController"];    
    vc.typeContact = contacts;
    vc.selectedContact = self.selectedContact;
    [self.navigationController pushViewController:vc animated:true];
    
}



 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if ([segue.identifier isEqualToString:@"ToViewContacts"]) {
         ContactsViewController *vc = segue.destinationViewController;
         vc.selectedContact = self.selectedContact;
     }
 }

@end
