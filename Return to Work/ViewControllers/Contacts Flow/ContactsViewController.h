//
//  ContactsViewController.h
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"

@interface ContactsViewController : UIViewController <UITableViewDelegate , UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *maintableView;


@property (nonatomic) Contact *selectedContact;
@property (nonatomic) Contact *selectedMainContact;

@property (weak, nonatomic) IBOutlet UIImageView *mainContactTypeIcon;
@property (weak, nonatomic) IBOutlet UILabel *contactTypeTxt;
@property (weak, nonatomic) IBOutlet UILabel *contactTypeSubtitle;
- (IBAction)addAction:(UIButton *)sender;

@property (nonatomic, strong) ContactList *allContacts;
@property (nonatomic, strong) NSMutableArray<Contact*> *typeContact;


@end
