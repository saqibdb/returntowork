
//
//  ContactsViewController.m
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "ContactsViewController.h"
#import "ContactsTableViewCell.h"
#import "ContactDetailsViewController.h"
#import "CorrespondenceAPIManager.h"
#import "Correspondence.h"
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import <SVProgressHUD/SVProgressHUD.h>


@import Firebase;

@interface ContactsViewController (){
    NSArray *colorsArrray;
}

@property (nonatomic, weak) IBOutlet UIView *borderVw;

@end

@implementation ContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self setUpInterface];
    [self addShadow];
    
    if(self.typeContact.count == 0) {
        //[self showErrorAlert];
    }
}


-(void)viewDidAppear:(BOOL)animated{
    [self refreshContact];
}

-(void)showErrorAlert{
    
    NSString *title = @"Oops!";
    NSString *message = @"No contacts found";
    
   
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}


-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}

-(void)setUpInterface {
    
    [self createNavigationTitleViewWithTitle:@"Contacts"];
    
    self.mainContactTypeIcon.image = self.selectedContact.icon;
    self.contactTypeTxt.text = self.selectedContact.name;
    self.contactTypeSubtitle.text = [NSString stringWithFormat:@"%@ Contacts",self.selectedContact.name];
    colorsArrray = [[NSArray alloc] initWithObjects:[UIColor purpleColor], [UIColor yellowColor], [UIColor blueColor], [UIColor greenColor], nil];

    self.maintableView.delegate = self;
    self.maintableView.dataSource = self;
    self.maintableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.contactTypeTxt.text = self.selectedContact.name;
    //self.contactTypeTxt.text = self.typeContact[0].type;
//    if (self.typeContact.count > 0) {
//        self.contactTypeTxt.text = self.selectedContact.name;
//    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) deleteContact: (UIButton *)sender {
    Contact *modal = self.typeContact[sender.tag];
    
    FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Contact"] child:@"Contact"];
    FIRDatabaseReference* contactRef = [correspondenceRef child:modal.contactId];
    
    [SVProgressHUD showWithStatus:@"Deleting..."];
    
    [contactRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        
        [SVProgressHUD dismiss];
        if (error) {
            //[self showSuccessAlert:false];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
            
            
        } else {
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Contact has been deleted." andCancelButton:NO forAlertType:AlertSuccess];
            [alert show];
            
            
            [self refreshContact];
            //[self showSuccessAlert:true];
        }
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //return self.allContacts.contactArr.count;
    return self.typeContact.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsTableViewCell"];
    
//    Contact *contact = self.allContacts.contactArr[indexPath.row];
//    cell.contactName.text = contact.contactName;
//    cell.contactEdit.text = [NSString stringWithFormat:@"Last Edit : %@" , contact.lastEdit];
////    cell.imageCellIcon.image = self.selectedContact.icon;
//    cell.colorView.backgroundColor = colorsArrray[indexPath.row % 4];
//    return cell;
    
    
    Contact *modal = self.typeContact[indexPath.row];
    cell.contactName.text = modal.contactName;
    cell.colorView.backgroundColor = colorsArrray[indexPath.row % 4];
    cell.deleteBtn.tag = indexPath.row;
    [cell.deleteBtn addTarget: self action: @selector(deleteContact:) forControlEvents: UIControlEventTouchUpInside];
    cell.deleteBtn.hidden = YES;
    return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    [self performSegueWithIdentifier:@"ToContactDetails" sender:self];
    self.selectedMainContact = self.allContacts.contactArr[indexPath.row];
    [self moveToContactDetails:self.typeContact[indexPath.row] headerContact:self.selectedContact];
}


-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        NSLog(@"Action to perform with Delete");
        
        
        
        
        Contact *modal = self.typeContact[indexPath.row];
        
        FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Contact"] child:@"Contact"];
        FIRDatabaseReference* contactRef = [correspondenceRef child:modal.contactId];
        
        [SVProgressHUD showWithStatus:@"Deleting..."];
        
        [contactRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            
            [SVProgressHUD dismiss];
            if (error) {
                //[self showSuccessAlert:false];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                
                
            } else {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Contact has been deleted." andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
                
                
                [self refreshContact];
                //[self showSuccessAlert:true];
            }
        }];
        
        
        
        
    }];
    button.backgroundColor = [UIColor redColor]; //arbitrary color
    
    
    
    return @[button]; //array with all the buttons you want. 1,2,3, etc...
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES; //tableview must be editable or nothing will work...
}




-(void)moveToContactDetails:(Contact*)contact headerContact:(Contact*)selContact {
 
    ContactDetailsViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactDetailsViewController"];
    newVC.contactDetails = contact;
    newVC.selectedMainContact = selContact;
    newVC.isFromtblSel = true;
    [self.navigationController pushViewController:newVC animated:true];
}

-(void)addDummyContacts {
//    allContacts = [[NSMutableArray alloc] init];
//    Contact *newContact = [[Contact alloc] initWithName:@"John Doe" andAddress:@"123 Avenue Detroit" andEmail:@"john@doe.com" andPhone:@"123456789"];
//    [allContacts addObject:newContact];
//
//    newContact = [[Contact alloc] initWithName:@"Jane Doe" andAddress:@"123 Avenue Detroit" andEmail:@"john@doe.com" andPhone:@"123456789"];
//    [allContacts addObject:newContact];
//
//    newContact = [[Contact alloc] initWithName:@"Johnny Doe" andAddress:@"123 Avenue Detroit" andEmail:@"john@doe.com" andPhone:@"123456789"];
//    [allContacts addObject:newContact];
//
//    newContact = [[Contact alloc] initWithName:@"Jannice Doe" andAddress:@"123 Avenue Detroit" andEmail:@"john@doe.com" andPhone:@"123456789"];
//    [allContacts addObject:newContact];

}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(20, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-100, 22, 300, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 100, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ToContactDetails"]) {
        ContactDetailsViewController *vc = segue.destinationViewController;
        vc.selectedContact = self.selectedMainContact;
        vc.selectedMainContact = self.selectedContact;

    }
}


- (IBAction)addAction:(UIButton *)sender {
    
//    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Under Construction" andText:@"This Feature is currently Under Construction. Please check back on next builds." andCancelButton:NO forAlertType:AlertInfo];
//    [alert show];
    
    
    ContactDetailsViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactDetailsViewController"];
//    newVC.contactDetails = contact;
    newVC.selectedMainContact = self.selectedContact;
    newVC.isFromtblSel = false;
    [self.navigationController pushViewController:newVC animated:true];
}



-(void)refreshContact {
    NSLog(@"Need Contacts to be Refreshed Here");
    
    [CorrespondenceAPIManager fetchContactWithType:@"type" andValue:self.selectedContact.name andCompletion:^(ContactList *cList) {
        self.typeContact = [[NSMutableArray alloc] initWithArray:cList.contactArr];
        
        [self.maintableView reloadData];
        
    }];
}




@end
