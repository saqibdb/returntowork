//
//  ContactsTypeViewController.h
//  Return to Work
//
//  Created by iBuildx-Macbook on 08/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"

@interface ContactsTypeViewController : UIViewController <UITableViewDelegate , UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *maintableView;

@property (nonatomic) Contact *selectedContact;
@property (nonatomic, strong) ContactList *contactList;


@end
