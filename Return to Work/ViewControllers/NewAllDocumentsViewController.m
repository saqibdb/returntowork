//
//  NewAllDocumentsViewController.m
//  Return to Work
//
//  Created by iBuildx-Macbook on 08/02/2018.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import "NewAllDocumentsViewController.h"
#import "DocumentationCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ImageViewer.h"
#import "Return_to_Work-Swift.h"

@import Firebase;
@import FirebaseDatabase;


@interface NewAllDocumentsViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>{
    NSMutableArray *imagesLinks;
}
@property (nonatomic, weak) IBOutlet UICollectionView *docCollectionView;

@end

@implementation NewAllDocumentsViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavigationTitleViewWithTitle:self.titleNavigation];

    
    
    self.mainTitleText.text = self.titleMain;
    
    self.subTitletext.text = self.subTitle;
    
    self.imageIconImageView.image = self.iconImage;
    
    self.specialTitleText.text = self.specialTitle;
    
    imagesLinks = [[NSMutableArray alloc] init];
    if (self.job.jobDocPath.length) {
        imagesLinks = [[self.job.jobDocPath componentsSeparatedByString:@","] mutableCopy];
    }
    if (self.resumeMain.coverLetterPath.length) {
        imagesLinks = [[self.resumeMain.coverLetterPath componentsSeparatedByString:@","] mutableCopy];
    }
    if (self.resumeAnother.resumePath.length) {
        imagesLinks = [[self.resumeAnother.resumePath componentsSeparatedByString:@","] mutableCopy];
    }
    
    if (self.medicalDocs.planImage.length) {
        imagesLinks = [[self.medicalDocs.planImage componentsSeparatedByString:@","] mutableCopy];
    }
    
    
    [self.docCollectionView reloadData];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    DocumentationCell *cell = (DocumentationCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"DocumentationCell" forIndexPath:indexPath];
    [cell.docImage sd_setImageWithURL:[NSURL URLWithString:imagesLinks[indexPath.row]]];
    [cell.btnDelete setTag:indexPath.row];
    [cell.btnDelete addTarget:self action:@selector(deleteDocument:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDelete setHidden:YES];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    DocumentationCell *cell = (DocumentationCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [[TestSwift new] showTheFullImageWithImageView:cell.docImage viewController:self];
}





-(void)deleteDocument:(UIButton*)sender {
    
    UIAlertController * alert=[UIAlertController
                               
                               alertControllerWithTitle:@"" message:@"Are you sure, you want to Delete?"preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    
                                    
                                    
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return imagesLinks.count;
}


@end
