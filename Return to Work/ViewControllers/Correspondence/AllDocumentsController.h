//
//  AllDocumentsController.h
//  Return to Work
//
//  Created by Anshumaan on 25/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DocumentationModel.h"

@interface AllDocumentsController : UIViewController

@property (nonatomic, strong) DocumentationList* docList;
@property (nonatomic, strong) NSString* type;




@property (nonatomic, strong) UIImage* imageIcon;
@property (nonatomic, strong) NSString* mainTitle;
@property (nonatomic, strong) NSString* subTitle;


@end
