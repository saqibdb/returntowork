//
//  DocumentationListController.h
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocumentationListController : UIViewController

@property (nonatomic) BOOL isRWP;

@end
