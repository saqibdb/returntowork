//
//  DocumentationListController.mDocumentationListController
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "DocumentationListController.h"
#import "ContactsTableViewCell.h"
#import "DocumentationDetailsController.h"
#import "DocumentationModel.h"
#import "DocumentationAPIManager.h"
#import "DocumentsCollectionController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "Return_to_Work-Swift.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NewAllDocumentsViewController.h"
#import "MailListController.h"

@interface DocumentationListController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UIImageView *docIcon;
@property (nonatomic, weak) IBOutlet UILabel *docNameLbl;
@property (nonatomic, weak) IBOutlet UILabel *lastEditLbl;
@property (nonatomic, weak) IBOutlet UITableView *docListTable;
@property (nonatomic, weak) IBOutlet UIView *borderVw;

@property (nonatomic, strong) NSArray *colorsArray;
@property (nonatomic, strong) NSMutableArray<DocumentationModel*>* docList;

@end

@implementation DocumentationListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavigationTitleViewWithTitle:@"Correspondence"];
    _colorsArray = [[NSArray alloc] initWithObjects:[UIColor purpleColor], [UIColor yellowColor], [UIColor blueColor], [UIColor greenColor], nil];
    
    [self addShadow];
    
    [self fetchAllDocuments];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ContactsTableViewCell *cell = (ContactsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ContactsTableViewCell class]) forIndexPath:indexPath];
//    if (self.isRWP) {
//        [cell.contactName setText:[NSString stringWithFormat:@"RTWP %ld", indexPath.row+1]];
//    } else {
//        [cell.contactName setText:[NSString stringWithFormat:@"IMP %ld", indexPath.row+1]];
//    }
    [cell.contactName setText:self.docList[indexPath.row].planName];
    [cell.colorView setBackgroundColor:self.colorsArray[indexPath.row%4]];
    cell.emailBtn.tag = indexPath.row;
    [cell.emailBtn addTarget:self action:@selector(emailToParties:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.docList.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*
    DocumentsCollectionController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DocumentsCollectionController class])];
    if (self.isRWP) {
        controller.type = @"doc_return_to_work_plan";
    } else {
        controller.type = @"doc_injury_management_plan";
    }
//    controller.docList = self.docList;
    controller.planID = self.docList[indexPath.row].planId;
    [self.navigationController pushViewController:controller animated:YES];*/
    DocumentationModel *doc = self.docList[indexPath.row];
    UIStoryboard *dashboard = [UIStoryboard storyboardWithName:@"JobSeeking" bundle:[NSBundle mainBundle]];

    NewAllDocumentsViewController *vc = [dashboard instantiateViewControllerWithIdentifier:NSStringFromClass([NewAllDocumentsViewController class])];
    vc.medicalDocs = doc;
    vc.titleNavigation = @"Correspondence";
    vc.titleMain = @"Documentation";
    vc.subTitle = @"RTWP|IMP";
    vc.specialTitle = @"RTWP All Documents";
    vc.iconImage = [UIImage imageNamed:@"Docx_RTWP_imp"];
    
    [self.navigationController pushViewController:vc animated:YES];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        NSLog(@"Action to perform with Delete");
        
        DocumentationModel *doc = self.docList[indexPath.row];
        
        FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"RTWPlan"] child:@"RTWPlan"];
        FIRDatabaseReference* contactRef = [correspondenceRef child:doc.planId];
        [SVProgressHUD showWithStatus:@"Deleting..."];
        
        [contactRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            
            [SVProgressHUD dismiss];
            if (error) {
                //[self showSuccessAlert:false];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                
                
            } else {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"CoverLetter has been deleted." andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
                
                
                [self fetchAllDocuments];

                
            }
        }];
        
        
    }];
    button.backgroundColor = [UIColor redColor]; //arbitrary color
    
    
    
    return @[button]; //array with all the buttons you want. 1,2,3, etc...
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES; //tableview must be editable or nothing will work...
}

-(void)emailToParties:(UIButton*)sender {
    
    DocumentationModel *doc = self.docList[sender.tag];

    UIStoryboard *dashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:[NSBundle mainBundle]];
    MailListController *controller = [dashboard instantiateViewControllerWithIdentifier:NSStringFromClass([MailListController class])];
    controller.contactType = nil;
    controller.selectedDocument = doc;
    [self.navigationController pushViewController:controller animated:YES];
}



-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}

-(void)fetchAllDocuments {
    [DocumentationAPIManager fetchAllDocumentationWithCompletion:^(DocumentationList *documentsList) {
        if (self.isRWP == YES){
           self.docList = [self fetchDocumentsWithType:@"doc_return_to_work_plan" andDocumentation:documentsList];
        } else {
           self.docList =  [self fetchDocumentsWithType:@"doc_injury_management_plan" andDocumentation:documentsList];
        }
        [self.docListTable reloadData];
    }];
}

-(NSMutableArray<DocumentationModel*>*)fetchDocumentsWithType:(NSString*)type andDocumentation:(DocumentationList*)allDocs{
    
    if ([allDocs fetchDocumentation:type].count == 0) {
        //[self showErrorAlert];
    } else {
    
        return [allDocs fetchDocumentation:type];
    }
    return nil;
}

-(void)showErrorAlert{
    
    NSString *title = @"Oops";
    NSString *message = @"No previous documents found";
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}

@end
