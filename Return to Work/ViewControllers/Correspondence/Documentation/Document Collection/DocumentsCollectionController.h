//
//  DocumentsCollectionController.h
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DocumentationModel.h"

@interface DocumentsCollectionController : UIViewController

@property (nonatomic, strong) NSMutableArray<DocumentationModel*>* docList;
@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSString* planID;

@property (weak, nonatomic) IBOutlet UILabel *mainTitleText;

@property (weak, nonatomic) IBOutlet UILabel *subTitletext;

@property (weak, nonatomic) IBOutlet UIImageView *imageIconImageView;

@property (weak, nonatomic) IBOutlet UILabel *specialTitleText;















@property (nonatomic, strong) UIImage* imageIcon;
@property (nonatomic, strong) NSString* mainTitle;
@property (nonatomic, strong) NSString* subTitle;



@end
