//
//  DocumentsCollectionController.m
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "DocumentsCollectionController.h"
#import "DocumentationCell.h"
#import "DocumentationAPIManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ImageViewer.h"
#import "Return_to_Work-Swift.h"


@import Firebase;
@import FirebaseDatabase;


@interface DocumentsCollectionController ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, weak) IBOutlet UICollectionView *docCollectionView;
@end

@implementation DocumentsCollectionController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavigationTitleViewWithTitle:@"Correspondence"];
    [self fetchAllDocuments];
    
    
    if (self.mainTitle.length) {

        self.mainTitleText.text = self.mainTitle;
        
        self.subTitletext.text = self.subTitle;
        
        self.imageIconImageView.image = self.imageIcon;
        
        self.specialTitleText.text = self.mainTitle;
        
    }
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    DocumentationCell *cell = (DocumentationCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"DocumentationCell" forIndexPath:indexPath];
    [cell.docImage sd_setImageWithURL:[NSURL URLWithString:self.docList[indexPath.row].planImage]];
     [cell.btnDelete setTag:indexPath.row];

    [cell.btnDelete addTarget:self action:@selector(deleteDocument:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    DocumentationCell *cell = (DocumentationCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [[TestSwift new] showTheFullImageWithImageView:cell.docImage viewController:self];
    
    
    
    /*
    ImageViewer *controller = [[ImageViewer alloc] initWithNibName:NSStringFromClass([ImageViewer class]) bundle:[NSBundle mainBundle]];
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    [controller fetchImageUrl:self.docList[indexPath.row].planImage];

    controller.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:controller.view];
     
     */
}





-(void)deleteDocument:(UIButton*)sender {
    
    UIAlertController * alert=[UIAlertController
                               
                               alertControllerWithTitle:@"" message:@"Are you sure, you want to delete this document?"preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    FIRDatabaseReference *ref = [[[[FIRDatabase database] reference] child:@"RTWPlan"] child:@"RTWPlan"];
                                    NSString* key = self.docList[sender.tag].planId;
                                    [[ref child:key] removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                                        [self fetchAllDocuments];
                                    }];
                                    
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.docList.count;
}

 -(void)fetchAllDocuments {
     [DocumentationAPIManager fetchAllDocumentationWithCompletion:^(DocumentationList *documentsList) {
         self.docList = [self fetchDocumentsWithType:self.type andDocumentation:documentsList];
         
         
         
         
         
         
         if (self.docList.count == 0) {
             //[self.navigationController popViewControllerAnimated:true];
         } else {
             [self.docCollectionView reloadData];
         }
     }];
 }

 -(NSMutableArray<DocumentationModel*>*)fetchDocumentsWithType:(NSString*)type andDocumentation:(DocumentationList*)allDocs{
     NSMutableArray<DocumentationModel*>* docArr = [[NSMutableArray alloc] init];
     for(DocumentationModel* doc in [allDocs fetchDocumentation:type]) {
         if([doc.planId isEqualToString:self.planID]) {
             [docArr addObject:doc];
         }
     }
     
     return docArr;
 }

@end
