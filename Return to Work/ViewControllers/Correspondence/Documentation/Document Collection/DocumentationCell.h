//
//  DocumentationCell.h
//  Return to Work
//
//  Created by Anshumaan on 25/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocumentationCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView* docImage;
@property (nonatomic, weak) IBOutlet UIButton* btnDelete;

@end
