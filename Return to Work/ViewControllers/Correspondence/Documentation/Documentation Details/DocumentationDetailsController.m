//
//  DocumentationDetailsController.m
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "DocumentationDetailsController.h"
#import "UploadDocumentController.h"

@interface DocumentationDetailsController ()
@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UIView *borderVw;


@end

@implementation DocumentationDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UploadDocumentController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([UploadDocumentController class])];
    [self addChildViewController:controller];
    controller.planType = @"doc_return_to_work_plan";
    [controller didMoveToParentViewController:self];
    [self.containerView addSubview:controller.view];
    
    controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    [self addShadow];
    [self createNavigationTitleViewWithTitle:@"Correspondence"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}


-(IBAction)segmentedControlSelected:(UISegmentedControl*)sender {
    
    [self.containerView.subviews[0] removeFromSuperview];
    UploadDocumentController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([UploadDocumentController class])];
    [self addChildViewController:controller];
    if (sender.selectedSegmentIndex == 0) {
        controller.planType = @"doc_return_to_work_plan";
    } else {
        controller.planType = @"doc_injury_management_plan";
    }
    [controller didMoveToParentViewController:self];
    [self.containerView addSubview:controller.view];
   
    controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    
    self.navigationItem.titleView = newView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
