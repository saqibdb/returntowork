//
//  CorrespondenceHome.m
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "CorrespondenceHome.h"
#import "CorrespondenceListCell.h"
#import "Correspondence.h"
#import "AppointmentListController.h"
#import "MedicalInformationController.h"
#import "DocumentationListController.h"
#import "ReceiptController.h"
#import "DocumentationDetailsController.h"
#import "DocumentationAPIManager.h"
#import "DocumentationModel.h"
#import "TravelsController.h"
#import "InjuryProgressController.h"

@interface CorrespondenceHome () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *correspondenceListTbl;

@property (nonatomic, strong) DocumentationList* docLists;

@end

@implementation CorrespondenceHome

NSMutableArray *allCorrespondence;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createNavigationTitleViewWithTitle:@"Correspondence"];
    [self addDummyContacts];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allCorrespondence.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    CorrespondenceListCell* cell = (CorrespondenceListCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CorrespondenceListCell class]) forIndexPath:indexPath];
    Correspondence* correspondence = allCorrespondence[indexPath.row];
    [cell addDataToCell:correspondence];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        AppointmentListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AppointmentListController class])];
        [self.navigationController pushViewController:controller animated:YES];
    } else if (indexPath.row == 2) {
        MedicalInformationController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MedicalInformationController class])];
        [self.navigationController pushViewController:controller animated:YES];
    } else if (indexPath.row == 1) {
        DocumentationDetailsController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DocumentationDetailsController class])];
        [self.navigationController pushViewController:controller animated:YES];
    } else if (indexPath.row == 5) {
        ReceiptController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ReceiptController class])];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row == 4) {
        TravelsController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([TravelsController class])];
        [self.navigationController pushViewController:controller animated:YES];
    } else if (indexPath.row == 3) {
        InjuryProgressController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([InjuryProgressController class])];
        [self.navigationController pushViewController:controller animated:YES];
    }
}




-(void)addDummyContacts {
        allCorrespondence = [[NSMutableArray alloc] init];
    
    Correspondence *newCorrespondence = [[Correspondence alloc] initWithName:@"Appointments" withDescription:@"" andLastEdit:@"Today" andIcon:@"appointment"];
    [allCorrespondence addObject:newCorrespondence];
    
    newCorrespondence = [[Correspondence alloc] initWithName:@"Documentation" withDescription:@"RTWP|IMP" andLastEdit:@"Today" andIcon:@"Docx_RTWP_imp"];
    [allCorrespondence addObject:newCorrespondence];
    
    newCorrespondence = [[Correspondence alloc] initWithName:@"Medical Information" withDescription:@"Med Cert|Referrals|Medical Reports" andLastEdit:@"Today" andIcon:@"medi_info"];
    [allCorrespondence addObject:newCorrespondence];
    
    newCorrespondence = [[Correspondence alloc] initWithName:@"Injury Progress" withDescription:@"" andLastEdit:@"Today" andIcon:@"injury_process"];
    [allCorrespondence addObject:newCorrespondence];
    
    newCorrespondence = [[Correspondence alloc] initWithName:@"Travel" withDescription:@"" andLastEdit:@"Today" andIcon:@"Travel"];
    [allCorrespondence addObject:newCorrespondence];
    
    newCorrespondence = [[Correspondence alloc] initWithName:@"Reciepts" withDescription:@"" andLastEdit:@"Today" andIcon:@"Medical_Reports"];
    [allCorrespondence addObject:newCorrespondence];
    
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end


