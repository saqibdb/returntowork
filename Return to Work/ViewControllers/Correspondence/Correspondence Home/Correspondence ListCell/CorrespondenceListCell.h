//
//  CorrespondenceListCell.h
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Correspondence.h"

@interface CorrespondenceListCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *listImageView;
@property (nonatomic, weak) IBOutlet UILabel *listNameLbl;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLbl;
@property (nonatomic, weak) IBOutlet UILabel *editTimeLbl;
@property (nonatomic, weak) IBOutlet UIView *borderVw;

-(void)addDataToCell:(Correspondence *)correspondenceDetails;

@end
