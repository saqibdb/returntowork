//
//  CorrespondenceListCell.m
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "CorrespondenceListCell.h"

@implementation CorrespondenceListCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)addDataToCell:(Correspondence *)correspondenceDetails {
    
    [self.listImageView setImage:[UIImage imageNamed:correspondenceDetails.icon]];
    [self.listNameLbl setText:correspondenceDetails.name];
    [self.descriptionLbl setText:correspondenceDetails.desc];
    [self.editTimeLbl setText:correspondenceDetails.lastEdit];
    
    [self.listImageView.layer setCornerRadius:5];
    [self.listImageView setClipsToBounds:true];
    
    [self addShadow];
}

-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}


@end
