//
//  AppointmentListController.m
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "AppointmentListController.h"
#import "Contact.h"
#import "ContactsTableViewCell.h"
#import "AddAppointmentController.h"
#import "Correspondence.h"
#import "CorrespondenceAPIManager.h"

@interface AppointmentListController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) NSMutableArray *availableAppointments;
@property (nonatomic, weak) IBOutlet UITableView *appointmentsTable;
@property (nonatomic, strong) Appointments *appointmentArr;
@property (nonatomic, strong) ContactList *contacts;
@end

@implementation AppointmentListController

-(void)loadWithAppointments:(NSArray *)appointments{
    self.availableAppointments = appointments;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addDummyContacts];
    [self fetchAllContacts];
    
    [self fetchAllContactList];
    [self createNavigationTitleViewWithTitle:@"Correspondence"];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchAllContacts{
    [CorrespondenceAPIManager fetchAllAppointmentsWithCompletion:^(Appointments *appointments) {
        self.appointmentArr = appointments;
    }];
}

#pragma mark UItableView Datasource & Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.availableAppointments.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsTableViewCell"];
    
    Contact *contact = _availableAppointments[indexPath.row];
    [cell addDataOnUI:contact];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
        AddAppointmentController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AddAppointmentController class])];
        Contact* contact = self.availableAppointments[indexPath.row];
        
        [controller getContactDetails:contact withAppointmnets:[self.contacts fetchContactsWithType:contact.name] andAppointments:[self.appointmentArr fetchAppointmentsWithType:contact.name]];
        [self.navigationController pushViewController:controller animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}


-(void)addDummyContacts {
    _availableAppointments = [[NSMutableArray alloc] init];
    Contact *newContact = [[Contact alloc] initWithName:@"Employer" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"employerIcon"]];
    [_availableAppointments addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Rehabilitation Provider" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"rehabilitationIcon"]];
    [_availableAppointments addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Nominated Treating Doctor" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"nominatedIcon"]];
    [_availableAppointments addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Physiotherapist" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"PhysiotherapistIcon"]];
    [_availableAppointments addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Psychologist" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"psychologistIcon"]];
    [_availableAppointments addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Other" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"otherIcon"]];
    [_availableAppointments addObject:newContact];
}




-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    
    
    self.navigationItem.titleView = newView;
}

-(void)fetchAllContactList {
    [CorrespondenceAPIManager fetchAllContactWithCompletion:^(ContactList *contacts) {
        
        self.contacts = contacts;
        //        self.maintableView.reloadData;
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
