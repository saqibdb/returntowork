//
//  ReceiptController.h
//  Return to Work
//
//  Created by Anshumaan on 18/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiptController : UIViewController<UICollectionViewDataSource , UICollectionViewDelegate>


@property (weak, nonatomic) IBOutlet UICollectionView *documentsCollectionView;

@end
