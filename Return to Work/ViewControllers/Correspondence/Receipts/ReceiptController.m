//
//  ReceiptController.m
//  Return to Work
//
//  Created by Anshumaan on 18/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "ReceiptController.h"
#import "DateSelectionController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "DocumentationModel.h"
#import "DocumentationAPIManager.h"
#import "AllDocumentsController.h"
#import "MailListController.h"
#import "Return_to_Work-Swift.h"
#import <ELCImagePickerController/ELCImagePickerController.h>



@interface ReceiptController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate , ELCImagePickerControllerDelegate>{
    NSMutableArray *selectedImages;
}

@property (nonatomic, weak) IBOutlet UILabel *dateLbl;
@property (nonatomic, weak) IBOutlet UITextField *certNameLbl;
@property (nonatomic, weak) IBOutlet UITextField *noteNameLbl;
@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UIView *borderVw;

@property (nonatomic, strong) NSString* imagePath;
@property (nonatomic, strong) NSString* imageUrl;

@end

@implementation ReceiptController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    selectedImages = [[NSMutableArray alloc] init];
    [self.view layoutIfNeeded];
    
    self.documentsCollectionView.dataSource = self;
    self.documentsCollectionView.delegate = self;
    [self.documentsCollectionView reloadData];
    // Do any additional setup after loading the view.
    [self.documentsCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd yyyy"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    self.dateLbl.text = [dateFormatter stringFromDate:[NSDate date]];
    
    [self addShadow];
    // Do any additional setup after loading the view.
    [self createNavigationTitleViewWithTitle:@"Receipts"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}

-(IBAction)emailToParties:(UIButton*)sender {
    UIStoryboard *dashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:[NSBundle mainBundle]];
    MailListController *controller = [dashboard instantiateViewControllerWithIdentifier:NSStringFromClass([MailListController class])];
    [self.navigationController pushViewController:controller animated:YES];
}

-(IBAction)editCerTDetails:(UIButton*)sender {
    [self.certNameLbl becomeFirstResponder];
}

-(IBAction)uploadCert:(UIButton*)sender {
    UIAlertController* alertCtrl = [UIAlertController alertControllerWithTitle:nil
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
    //Create an action
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action)
                             {
                                 UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
                                 imagePicker.delegate = self;
                                 imagePicker.allowsEditing = true;
                                 imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                 [self presentViewController:imagePicker animated:YES completion:nil];
                             }];
    UIAlertAction *imageGallery = [UIAlertAction actionWithTitle:@"Image Gallery"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action)
                                   {
                                       ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] init];
                                       elcPicker.maximumImagesCount = 100; //Set the maximum number of images to select to 100
                                       elcPicker.imagePickerDelegate = self;
                                       [self presentViewController:elcPicker animated:YES completion:nil];
                                   }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action)
                             {
                                 [self dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    
    //Add action to alertCtrl
    [alertCtrl addAction:camera];
    [alertCtrl addAction:imageGallery];
    [alertCtrl addAction:cancel];
    [self presentViewController:alertCtrl animated:YES completion:^{
    }];
}

-(IBAction)saveDetails:(UIButton*)sender {
    if(self.certNameLbl.text.length == 0){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"InComplete" message:@"Please Enter a Name of file before uploading." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:true completion:nil];
        return;
    }
    
    
    if (selectedImages.count) {
        [self uploadImagePath:self.imagePath];
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"InComplete" message:@"Please select a document before uploading." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:true completion:nil];
    }
}

-(IBAction)selectDate:(UIButton*)sender {
    
    DateSelectionController *picker = [[DateSelectionController alloc] initWithNibName:NSStringFromClass([DateSelectionController class]) bundle:[NSBundle mainBundle]];
    [picker loadDatePicker:^(id  _Nullable responseObject) {
        [self.dateLbl setText:responseObject];
    }];
    //    UIViewController* baseController = [self getController];
    [self addChildViewController:picker];
    [picker didMoveToParentViewController:self];
    [self.view addSubview:picker.view];
    [UIView animateWithDuration:0.3 animations:^{
        [picker.view setFrame:self.view.frame];
    } completion:^(BOOL finished) {
        
    }];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
    [selectedImages addObject:selectedImage];
    [self.documentsCollectionView reloadData];
    NSData *webData = [self compress:selectedImage];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", self.certNameLbl.text]];
    [webData writeToFile:localFilePath atomically:YES];
    self.imagePath = localFilePath;
    //    [self uploadImageData:selectedImage andPath:localFilePath];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(NSData*)compress:(UIImage*)image {
    int kMaxUploadSize = 150000;
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    NSData* imageData;
    do {
        imageData = UIImageJPEGRepresentation(image, compression);
        compression -= 0.10;
    } while ([imageData length] > kMaxUploadSize && compression > maxCompression);
    
    return imageData;
}

-(void)uploadImagePath:(NSString*)imagePath {
    [SVProgressHUD showWithStatus:@"Please Wait..."];
    __block NSString *imageUrl = @"";
    __block int i = 0;
    for (UIImage *image in selectedImages) {
        i = i + 1;
        FIRStorageReference *storageRef = [[FIRStorage storage] reference];
        FIRStorageReference *imageRef = [storageRef child:[NSString stringWithFormat:@"%@%i.jpg", self.certNameLbl.text, i]];
        [imageRef putData:[self compress:image] metadata:nil completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
            i = i - 1;
            if (!error) {
                NSLog(@"image URL = %@" , [metadata.downloadURLs[0] absoluteString]);
                if (imageUrl.length == 0) {
                    imageUrl = [metadata.downloadURLs[0] absoluteString];
                }
                else{
                    imageUrl = [NSString stringWithFormat:@"%@,%@" , imageUrl , [metadata.downloadURLs[0] absoluteString]];
                }
            }
            if (i == 0) {
                [SVProgressHUD dismiss];
                [self saveDocumentDetailsWithTitle:self.certNameLbl.text withDate:self.dateLbl.text andResumePath:imageUrl];
            }
        }];
    }
}

-(void)saveDocumentDetailsWithTitle:(NSString*)title withDate:(NSString*)date andResumePath:(NSString*)resumePath {

    DocumentationModel *docModel = [[DocumentationModel alloc] initWithPlanDate:date withPlanName:title andPlanImage:resumePath withExtraNote:@"" andPlanType:@"medical_reports_plan"];
    
    [DocumentationAPIManager saveDocumentsForRTWWithDocumentDetails:docModel onError:^(NSError *err) {
        if (err != nil) {
            [self showSuccessAlert:false];
        } else {
            [self showSuccessAlert:true];
        }
        self.certNameLbl.text = @"";
        [selectedImages removeAllObjects];
        [self.documentsCollectionView reloadData];
    }];
}

-(void)showSuccessAlert:(Boolean)success{
    
    NSString *title = @"Success";
    NSString *message = @"Document Saved Successfully";
    
    if(!success){
        title = @"Failure";
        message = @"There was an error in saving document";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(IBAction)segmentedControlChanged:(UISegmentedControl*)sender {
    AllDocumentsController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AllDocumentsController class])];
    if (sender.selectedSegmentIndex == 0) {
        if (self.containerView.subviews.count > 1) {
            [self.containerView.subviews[1] removeFromSuperview];
        }
    } else {
        controller.type = @"medical_reports_plan";
        //        controller.docList = self.docList;
        
        controller.mainTitle = @"Receipts";
        controller.subTitle = @"";
        controller.imageIcon = [UIImage imageNamed:@"Medical_Reports"];
        
        
        [self addChildViewController:controller];
        [controller didMoveToParentViewController:self];
        [self.containerView addSubview:controller.view];
        
        controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    }
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}


#pragma mark ELCImagePickerControllerDelegate Methods
- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info{
    for (NSDictionary *dict in info) {
        UIImage *selectedImage = dict[UIImagePickerControllerOriginalImage];
        [selectedImages addObject:selectedImage];
    }
    [self.documentsCollectionView reloadData];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return selectedImages.count + 1;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    UIImageView *previousImageView = [cell viewWithTag:100];
    [previousImageView removeFromSuperview];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    imageView.clipsToBounds = YES;
    if (indexPath.row == selectedImages.count) {
        imageView.image = [UIImage imageNamed:@"upload_small"];
    }
    else{
        imageView.image = selectedImages[indexPath.row];
    }
    imageView.tag = 100;
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    [cell addSubview:imageView];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(50 , 50);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == selectedImages.count) {
        [self uploadCert:nil];
        
        
        
    }
    else{
        UIAlertController* alertCtrl = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        //Create an action
        UIAlertAction *camera = [UIAlertAction actionWithTitle:@"View" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
            UIImageView *imageView = [cell viewWithTag:100];
            [[TestSwift new] showTheFullImageWithImageView:imageView viewController:self];
        }];
        UIAlertAction *imageGallery = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
            [selectedImages removeObjectAtIndex:indexPath.row];
            [self.documentsCollectionView reloadData];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        
        //Add action to alertCtrl
        [alertCtrl addAction:camera];
        [alertCtrl addAction:imageGallery];
        [alertCtrl addAction:cancel];
        [self presentViewController:alertCtrl animated:YES completion:nil];
    }
}








@end
