//
//  PreviousAppointmentListController.h
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Correspondence.h"

@interface PreviousAppointmentListController : UIViewController

-(void)fetchPrviousAppointments:(NSMutableArray*)appointmnets;

@end
