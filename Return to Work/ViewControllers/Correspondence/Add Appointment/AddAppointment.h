//
//  AddAppointment.h
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Correspondence.h"
#import "Contact.h"

@interface AddAppointment : UIViewController{
    
    
}

-(void)loadAppointmentWithType:(NSString*)appontmentType withAppointment:(Contact*)appointment;


@end
