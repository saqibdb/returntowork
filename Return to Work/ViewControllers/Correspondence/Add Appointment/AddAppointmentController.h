//
//  AddAppointmentController.h
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"
#import "Correspondence.h"
#import "PreviousAppointmentListController.h"

@interface AddAppointmentController : UIViewController

-(void)getContactDetails:(Contact*)contact withAppointmnets:(NSMutableArray*)contacts andAppointments:(NSMutableArray*)appointments;

-(void)refreshAppointmentsWithController :(PreviousAppointmentListController *)controller;


@end
