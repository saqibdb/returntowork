//
//  AddLocationController.h
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Correspondence.h"
#import "Contact.h"

@interface AddLocationController : UIViewController

@property (nonatomic) CGRect frame;

-(void)loadWithDate:(NSString*)date andTime:(NSString*)time appointmentType:(NSString*)type withAppontment:(Contact*)appointment;

@end
