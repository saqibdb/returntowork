//
//  AddLocationController.m
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "AddLocationController.h"
#import "CorrespondenceAPIManager.h"
#import <EventKit/EventKit.h>


@interface AddLocationController ()
@property (nonatomic, weak) IBOutlet UITextField *locationTextField;

@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) Contact* appointment;
@end

@implementation AddLocationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadWithDate:(NSString*)date andTime:(NSString*)time appointmentType:(NSString*)type withAppontment:(Contact*)appointment; {
    self.date = date;
    self.time = time;
    self.type = type;
    self.appointment = appointment;
}
- (IBAction)viewCalendarAction:(UIButton *)sender {
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"calshow://"]];

    
    
    
}

-(IBAction)saveAppointment:(UIButton*)sender {
    if ([self.locationTextField.text isEqualToString:@""]) {
        
        [self showErrorAlert:@"Please add location for your appointment"];
        
    } else if (self.appointment.contactName == nil) {
        [self showErrorAlert:@"Please select a contact for appointment"];
    } else {
        
        [self.locationTextField resignFirstResponder];
        
        [CorrespondenceAPIManager addAppointmentWithDate:self.date withTime:self.time andLocation:self.locationTextField.text appointmrntType:self.type withAppontment:self.appointment onError:^(NSError *err) {
            if (err != nil) {
                [self showSuccessAlert:false];
            } else {
                EKEventStore *store = [EKEventStore new];
                [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
                    if (!granted) {
                        return;
                    }
                    EKEvent *event = [EKEvent eventWithEventStore:store];
                    event.title = self.appointment.contactName;
                    event.location = self.locationTextField.text;
                    
                    NSString *dateString = [NSString stringWithFormat:@"%@-%@", self.date , self.time];
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"MMMM dd yyyy-EEEE h:mm a"];
                    NSDate *dateFromString = [dateFormatter dateFromString:dateString];
                    
                    
                    
                    NSTimeInterval aInterval2hrs = -120 * 60;
                    EKAlarm *alaram2hrs = [EKAlarm alarmWithRelativeOffset:aInterval2hrs];
                    [event addAlarm:alaram2hrs];
                    
                    
                    NSTimeInterval aInterval24hrs = -1440 * 60;
                    EKAlarm *alaram24hrs = [EKAlarm alarmWithRelativeOffset:aInterval24hrs];
                    [event addAlarm:alaram24hrs];
                    
                    
                    
                    event.startDate = dateFromString;
                    event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
                    event.calendar = [store defaultCalendarForNewEvents];
                    NSError *err = nil;
                    [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.locationTextField.text = @"";
                    });

                }];
                
                
                [self showSuccessAlert:true];
            }
        }];
    }
}

-(void)showSuccessAlert:(Boolean)success{
    
    NSString *title = @"Success";
    NSString *message = @"Appointment Saved Successfully. Auto reminder will be sent 24 hours and 2 hours before the appointment ";
    
    if(!success){
        title = @"Failure";
        message = @"There was an error in saving appointment";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}

-(void)showErrorAlert:(NSString*)message {
    NSString *title = @"Error";
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
