//
//  PreviousAppointmentListController.m
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "PreviousAppointmentListController.h"
#import "ContactsTableViewCell.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "CorrespondenceAPIManager.h"
#import "AddAppointmentController.h"


@import Firebase;
@import FirebaseDatabase;

@interface PreviousAppointmentListController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) IBOutlet UITableView* appointmentListController;
@property (nonatomic, strong) NSArray *colorsArray;
@property (nonatomic, strong) NSMutableArray *appointments;
@end

@implementation PreviousAppointmentListController

- (void)viewDidLoad {
    [super viewDidLoad];
    _colorsArray = [[NSArray alloc] initWithObjects:[UIColor purpleColor], [UIColor yellowColor], [UIColor blueColor], [UIColor greenColor], nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchPrviousAppointments:(NSMutableArray*)appointmnets {
    if (appointmnets.count == 0) {
        [self showErrorAlert];
    } else {
        self.appointments = appointmnets;
        [self.appointmentListController reloadData];
    }
}

-(void)showErrorAlert{
    
    NSString *title = @"Oops";
    NSString *message = @"No previous appointments found";
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ContactsTableViewCell *cell = (ContactsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ContactsTableViewCell class]) forIndexPath:indexPath];
    [cell.contactName setText:[NSString stringWithFormat:@"Appointment%ld", indexPath.row+1]];
    [cell.colorView setBackgroundColor:self.colorsArray[indexPath.row%4]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Correspondence *appointment = self.appointments[indexPath.row];
    [self displayDataAlert:[NSString stringWithFormat:@"Appointment %ld", indexPath.row+1] withDetails:appointment];
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.appointments.count;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        NSLog(@"Action to perform with Delete");
        
        
        
        
        Correspondence *appointment = self.appointments[indexPath.row];

        FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Appointment"] child:@"Appointment"];
        FIRDatabaseReference* contactRef = [correspondenceRef child:appointment.contactId];
        
        [SVProgressHUD showWithStatus:@"Deleting..."];
        
        [contactRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            
            [SVProgressHUD dismiss];
            if (error) {
                //[self showSuccessAlert:false];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                
                
            } else {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Appointment has been deleted." andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];

                for (UIViewController *controller in self.navigationController.viewControllers) {
                    if ([controller isKindOfClass:[AddAppointmentController class]]) {
                        AddAppointmentController *cont = (AddAppointmentController *)controller;
                        [cont refreshAppointmentsWithController:self];
                        break;
                    }
                    
                    
                    
                }
                
                
                //[self showSuccessAlert:true];
            }
        }];
        
        
        
        
    }];
    button.backgroundColor = [UIColor redColor]; //arbitrary color
    
    
    
    return @[button]; //array with all the buttons you want. 1,2,3, etc...
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES; //tableview must be editable or nothing will work...
}







-(void)displayDataAlert:(NSString*)appointmentName withDetails: (Correspondence*)appointment {
    
    NSString *alertMsg = @"Time | Date | Location";
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Appointment with %@", appointment.appointmentContactName] message:alertMsg preferredStyle:UIAlertControllerStyleAlert];
    NSString *title1 = appointment.appointmentTime;
    if (title1 == nil) {
        title1 = @"";
    }
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:title1 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    NSString *title2 = appointment.appointmentDate;
    if (title2 == nil) {
        title2 = @"";
    }
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:title2 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    NSString *title3 = appointment.appontmentLocation;
    if (title3 == nil) {
        title3 = @"";
    }
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:title3 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:action1];
    [alert addAction:action2];
    [alert addAction:action3];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end
