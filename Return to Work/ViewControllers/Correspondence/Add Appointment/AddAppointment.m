//
//  AddAppointment.m
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "AddAppointment.h"
#import "AddLocationController.h"
#import "Contact.h"

@interface AddAppointment ()<UIPickerViewDelegate>
@property(nonatomic, weak) IBOutlet UISegmentedControl *appointmentSegment;
@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UIDatePicker* datePicker;
@property (nonatomic, weak) IBOutlet UILabel *dateLbl;
@property (nonatomic, weak) IBOutlet UIButton *doneBtn;

@property (nonatomic, strong) Contact* appointment;

@property (nonatomic, strong) NSString *timeString;
@property (nonatomic, strong) NSString *dateString;

@property (nonatomic, strong) NSString* type;
@end

@implementation AddAppointment

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self.dateLbl setText:[self getTimeString:self.datePicker.date]];
    [self.dateLbl setText:[NSString stringWithFormat:@"Time and Date - %@", [self getTimeString:self.datePicker.date]]];

    self.timeString = [self getTimeString:self.datePicker.date];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadAppointmentWithType:(NSString*)appontmentType withAppointment:(Contact*)appointment {
    self.type = appontmentType;
    self.appointment = appointment;
}


-(IBAction)segmentedControlSelected:(UISegmentedControl*)sender {
    
    AddLocationController* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AddLocationController class])];
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    if (sender.selectedSegmentIndex == 0) {
        [self.datePicker setDatePickerMode:UIDatePickerModeTime];
        //[self.dateLbl setText:[self getTimeString:self.datePicker.date]];
        [self.dateLbl setText:[NSString stringWithFormat:@"Time and Date - %@", [self getTimeString:self.datePicker.date]]];

        self.timeString = [self getTimeString:self.datePicker.date];
        if (self.containerView.subviews.count > 1) {
            [[self.containerView.subviews objectAtIndex:1] removeFromSuperview];
        }
        [self.doneBtn setHidden:false];

    } else if (sender.selectedSegmentIndex == 1) {
        [self.datePicker setDatePickerMode:UIDatePickerModeDate];
        //[self.dateLbl setText:[self getDateString:self.datePicker.date]];
        [self.dateLbl setText:[NSString stringWithFormat:@"Time and Date - %@ | %@", [self getTimeString:self.datePicker.date], [self getDateString:self.datePicker.date]]];

        self.dateString = [self getDateString:self.datePicker.date];
        if (self.containerView.subviews.count > 1) {
            [[self.containerView.subviews objectAtIndex:1] removeFromSuperview];
        }
        [self.doneBtn setHidden:false];
        
    } else if (sender.selectedSegmentIndex == 2){
//        [self.datePicker removeFromSuperview];
        controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
        [self.dateLbl setText:[NSString stringWithFormat:@"Time and Date - %@ | %@", [self getTimeString:self.datePicker.date], [self getDateString:self.datePicker.date]]];
        [self.containerView addSubview:controller.view];
        [controller loadWithDate:[self getDateString:self.datePicker.date] andTime:[self getTimeString:self.datePicker.date] appointmentType:self.type withAppontment:self.appointment];
        [self.doneBtn setHidden:true];
    }
}


-(void)addSegmentsWithIndex:(int)index {
    
}

-(NSString*)getDateString:(NSDate*)date {
    NSDate* selectedDate = date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Convert to new Date Format
    [dateFormatter setDateFormat:@"MMMM dd yyyy"];
    NSString *newDate = [dateFormatter stringFromDate:selectedDate];
    return newDate;
}

-(NSString*)getTimeString:(NSDate*)date {
    NSDate* selectedDate = date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Convert to new Date Format
    [dateFormatter setDateFormat:@"EEEE h:mm a"];
    [dateFormatter setAMSymbol:@"am"];
    [dateFormatter setPMSymbol:@"pm"];
    NSString *newDate = [dateFormatter stringFromDate:selectedDate];
    return newDate;
}

-(IBAction)datePickerValueChanged:(UIDatePicker*)sender {
    if (self.appointmentSegment.selectedSegmentIndex == 0) {
        [self.dateLbl setText:[NSString stringWithFormat:@"Time and Date - %@", [self getTimeString:self.datePicker.date]]];
    } else if (self.appointmentSegment.selectedSegmentIndex == 1) {
        [self.dateLbl setText:[NSString stringWithFormat:@"Time and Date - %@ | %@", [self getTimeString:self.datePicker.date], [self getDateString:self.datePicker.date]]];
    }
    [self saveDateAndTime:nil];
}


-(IBAction)saveDateAndTime:(UIButton*)sender {
    if (self.appointmentSegment.selectedSegmentIndex == 0) {
         self.timeString = [self getTimeString:self.datePicker.date];
    } else if (self.appointmentSegment.selectedSegmentIndex == 1) {
        self.dateString = [self getDateString:self.datePicker.date];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
