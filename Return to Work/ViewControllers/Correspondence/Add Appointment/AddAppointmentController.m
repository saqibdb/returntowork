//
//  AddAppointmentController.m
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "AddAppointmentController.h"
#import "AddAppointment.h"
#import "SelectAppointmentController.h"
#import "CorrespondenceAPIManager.h"


@interface AddAppointmentController ()
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic, weak) IBOutlet UIImageView *appointmentIcon;
@property (nonatomic, weak) IBOutlet UILabel* nameLbl;
@property (nonatomic, weak) IBOutlet UILabel* employerLbl;
@property (nonatomic, weak) IBOutlet UIView* containerView;
@property (nonatomic, weak) IBOutlet UIView *borderVw;
@property (nonatomic, weak) IBOutlet UITextField *nameTxtFld;
@property (nonatomic, weak)IBOutlet UIView *txtView;

@property (nonatomic, weak) Contact* ContactDetails;
@property (nonatomic, strong) NSMutableArray* appointments;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) Contact* selectedAppontment;
@property (nonatomic, strong) NSMutableArray* contacts;

@end

@implementation AddAppointmentController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavigationTitleViewWithTitle:@"Correspondence"];
    AddAppointment* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AddAppointment class])];
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    [self.containerView addSubview:controller.view];
    controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    Contact *appointment = self.ContactDetails;
    self.type = self.ContactDetails.name;
    [controller loadAppointmentWithType:self.type withAppointment:appointment];
    [self.txtView setHidden:NO];
    
    [self.nameTxtFld setText:self.ContactDetails.name];
    
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    [self.nameLbl setText:self.ContactDetails.name];
    [self.employerLbl setText:self.ContactDetails.name];

    
    [self.appointmentIcon setImage:_ContactDetails.icon];
    [self addShadow];
//    controller.view. = self.containerView.frame.size.height
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}


-(void)getContactDetails:(Contact*)contact withAppointmnets:(NSMutableArray*)contacts andAppointments:(NSMutableArray*)appointments {
    self.ContactDetails = contact;
    self.appointments = appointments;
    self.contacts = contacts;
}


-(IBAction)segmentedControlSelected:(UISegmentedControl*)sender {
    [self addSegmentsWithIndex:sender.selectedSegmentIndex];
}


-(void)addSegmentsWithIndex:(int)index {
   
    AddAppointment* addAppointmentController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AddAppointment class])];
    [self addChildViewController:addAppointmentController];
    [addAppointmentController didMoveToParentViewController:self];
    
    PreviousAppointmentListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PreviousAppointmentListController class])];
    [controller fetchPrviousAppointments:self.appointments];
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    
    
    if (index == 0) {
        
        [controller.view removeFromSuperview];
        [self.containerView addSubview:addAppointmentController.view];
        addAppointmentController.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
        [addAppointmentController loadAppointmentWithType:self.type withAppointment:self.selectedAppontment];
        [self.txtView setHidden:NO];
        
    } else if (index == 1) {
        
        [addAppointmentController.view removeFromSuperview];
        [self.containerView addSubview:controller.view];
        controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
        [self.txtView setHidden:YES];
        [CorrespondenceAPIManager fetchAllAppointmentsWithCompletion:^(Appointments *appArray) {
            //[self.appointmentArr fetchAppointmentsWithType:contact.name]
            self.appointments = [appArray fetchAppointmentsWithType:self.ContactDetails.name];
            [controller fetchPrviousAppointments:self.appointments];
        }];
    }
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    self.navigationItem.titleView = newView;
}

-(IBAction)selectAppointment:(UIButton*)sender {
    SelectAppointmentController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SelectAppointmentController class])];
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    [controller loadAppointmentList:self.contacts withCallback:^(id  _Nullable responseObject) {
        Contact* appointment = responseObject;
        self.selectedAppontment = appointment;
        [self.nameTxtFld setText:appointment.contactName];
        self.type = appointment.type;
        [self addSegmentsWithIndex:0];
    }];
    
    [self.view addSubview:controller.view];
    controller.view.frame = CGRectMake(self.nameTxtFld.frame.origin.x + self.txtView.frame.origin.x, self.nameTxtFld.frame.origin.y + self.nameTxtFld.superview.frame.origin.y + self.nameTxtFld.frame.size.height, self.nameTxtFld.frame.size.width, 120);
}


-(void)refreshAppointmentsWithController :(PreviousAppointmentListController *)controller {
    [CorrespondenceAPIManager fetchAllAppointmentsWithCompletion:^(Appointments *appArray) {
        self.appointments = [appArray fetchAppointmentsWithType:self.ContactDetails.name];
        [controller fetchPrviousAppointments:self.appointments];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
