//
//  SelectAppointmentController.h
//  Return to Work
//
//  Created by Anshumaan on 31/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectAppointmentController : UIViewController

-(void)loadAppointmentList :(NSMutableArray*_Nullable)contacts withCallback:(void (^_Nullable)(id  _Nullable responseObject))completion;

@end
