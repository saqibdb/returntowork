//
//  SelectAppointmentController.m
//  Return to Work
//
//  Created by Anshumaan on 31/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "SelectAppointmentController.h"
#import "ContactsTableViewCell.h"

@interface SelectAppointmentController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) IBOutlet UITableView *appointmentTbl;
@property (nonatomic, strong) NSMutableArray *availableAppointments;
@property (nonatomic, strong, nullable) void (^callback)(id  _Nullable responseObject);
@end

@implementation SelectAppointmentController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self addDummyContacts];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.availableAppointments.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsTableViewCell"];
    
    
    if ([self.availableAppointments[indexPath.row] isKindOfClass:[Contact class]]){
        Contact *contact = _availableAppointments[indexPath.row];
        [cell.contactName setText:contact.contactName];
    } else {
        [cell.contactName setText:self.availableAppointments[indexPath.row]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.availableAppointments[indexPath.row] isKindOfClass:[Contact class]]){
        Contact *appointment = self.availableAppointments[indexPath.row];
        self.callback(appointment);
    } else {
        self.callback(self.availableAppointments[indexPath.row]);
    }
    
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 20;
}


-(void)loadAppointmentList :(NSMutableArray*)contacts withCallback:(void (^_Nullable)(id  _Nullable responseObject))completion {
    self.callback = completion;
    self.availableAppointments = contacts;
}

-(void)addDummyContacts {
    _availableAppointments = [[NSMutableArray alloc] init];
    Contact *newContact = [[Contact alloc] initWithName:@"Employer" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"employerIcon"]];
    [_availableAppointments addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Rehabilitation Provider" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"rehabilitationIcon"]];
    [_availableAppointments addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Nominated Treating Doctor" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"nominatedIcon"]];
    [_availableAppointments addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Physiotherapist" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"PhysiotherapistIcon"]];
    [_availableAppointments addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Psychologist" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"psychologistIcon"]];
    [_availableAppointments addObject:newContact];
    
    newContact = [[Contact alloc] initWithName:@"Other" andLastEdit:@"Today" andIcon:[UIImage imageNamed:@"otherIcon"]];
    [_availableAppointments addObject:newContact];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
