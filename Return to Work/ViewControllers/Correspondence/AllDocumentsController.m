//
//  AllDocumentsController.m
//  Return to Work
//
//  Created by Anshumaan on 25/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "AllDocumentsController.h"
#import "ContactsTableViewCell.h"
#import "DocumentsCollectionController.h"
#import "DocumentationAPIManager.h"
#import "SelectAppointmentController.h"
#import "MailListController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "NewAllDocumentsViewController.h"


@interface AllDocumentsController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *docListTable;
@property (nonatomic, weak) IBOutlet UILabel *typeLbl;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *viewHeight;

@property (nonatomic, strong) NSArray *colorsArray;
@property (nonatomic, strong) NSMutableArray<DocumentationModel*>* tableData;
@property (nonatomic, strong) NSMutableArray<DocumentationModel*>* filteredData;
@property (nonatomic, strong) NSArray *filterArray;

@end

@implementation AllDocumentsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _colorsArray = [[NSArray alloc] initWithObjects:[UIColor purpleColor], [UIColor yellowColor], [UIColor blueColor], [UIColor greenColor], nil];
    self.filterArray = [[NSArray alloc] initWithObjects:@"Medical Certificates", @"Medical Reports", @"Referrals", @"All", nil];
    // Do any additional setup after loading the view.
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self fetchAllDocuments];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)emailAction:(UIButton*)sender
{
    UIStoryboard *dashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:[NSBundle mainBundle]];
    MailListController *controller = [dashboard instantiateViewControllerWithIdentifier:NSStringFromClass([MailListController class])];
    controller.selectedDocument = self.filteredData[sender.tag];
    [self.navigationController pushViewController:controller animated:YES];
    
}



- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ContactsTableViewCell *cell = (ContactsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ContactsTableViewCell class]) forIndexPath:indexPath];
  
    DocumentationModel* model = self.filteredData[indexPath.row];
    if ([self.type  isEqual: @"travel_plan"]) {
        [cell.contactName setText:[NSString stringWithFormat:@"%@ %ld | %@ | %@ | %@km", self.type, indexPath.row+1, model.planDate, model.planName, model.extraNote]];
    } else {
        [cell.contactName setText:model.planName];
    }
    cell.emailBtn.tag = indexPath.row;
    [cell.emailBtn addTarget:self action:@selector(emailAction:) forControlEvents:UIControlEventTouchUpInside];

    [cell.colorView setBackgroundColor:self.colorsArray[indexPath.row%4]];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredData.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.type  isEqual: @"travel_plan"]) {
    }
    else{
        
        /*
        DocumentsCollectionController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DocumentsCollectionController class])];
        
        controller.type = self.type;
        controller.planID = self.filteredData[indexPath.row].planId;

        if (self.mainTitle.length) {
            controller.mainTitle = self.mainTitle;
            controller.subTitle = self.subTitle;
            controller.imageIcon = self.imageIcon;
        }
        [self.navigationController pushViewController:controller animated:YES];*/
        UIStoryboard *dashboard = [UIStoryboard storyboardWithName:@"JobSeeking" bundle:[NSBundle mainBundle]];

        NewAllDocumentsViewController *vc = [dashboard instantiateViewControllerWithIdentifier:NSStringFromClass([NewAllDocumentsViewController class])];
        vc.medicalDocs = self.filteredData[indexPath.row];
        vc.titleNavigation = @"Correspondence";
        vc.titleMain = self.mainTitle;
        vc.subTitle = self.subTitle;
        vc.specialTitle = @"Medical Information";
        vc.iconImage = self.imageIcon;
        
        [self.navigationController pushViewController:vc animated:YES];
        [self.navigationItem.backBarButtonItem setTitle:@" "];
    }
    
}


-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        NSLog(@"Action to perform with Delete");
        
        
        
        
        
        FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"RTWPlan"] child:@"RTWPlan"];
        FIRDatabaseReference* contactRef = [correspondenceRef child:self.filteredData[indexPath.row].planId];
        
        [SVProgressHUD showWithStatus:@"Deleting..."];
        
        [contactRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            
            [SVProgressHUD dismiss];
            if (error) {
                //[self showSuccessAlert:false];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                
                
            } else {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Document has been deleted." andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
                
                
                [self fetchAllDocuments];
                
                
                //[self showSuccessAlert:true];
            }
        }];
        
        
        
        
    }];
    button.backgroundColor = [UIColor redColor]; //arbitrary color
    
    
    
    return @[button]; //array with all the buttons you want. 1,2,3, etc...
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES; //tableview must be editable or nothing will work...
}





-(void)fetchDocumentsWithType:(NSString*)type andDocumentation:(DocumentationList*)allDocs{
    self.tableData = [allDocs fetchDocumentation:type];
    
    if (self.tableData.count == 0) {
        [self showErrorAlert];
    } else {
        self.filteredData = self.tableData;
        if ([type isEqualToString:@"Medical"]) {
            self.viewHeight.constant = 71;
        }else {
            self.viewHeight.constant = 0;
        }
        [self.docListTable reloadData];
    }
}


-(IBAction)selectType:(UIButton*)sender {
    SelectAppointmentController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SelectAppointmentController class])];
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    [controller loadAppointmentList:self.filterArray withCallback:^(id  _Nullable responseObject) {
        [self.typeLbl setText:responseObject];
        [self filterWithType:responseObject];
    }];
    
    [self.view addSubview:controller.view];
    controller.view.frame = CGRectMake(0, 0, self.view.frame.size.width, 120);
}

-(void)filterWithType:(NSString*)filterType {
    
    NSMutableArray<DocumentationModel*>* dataArr = [[NSMutableArray alloc] init];
    if ([filterType isEqualToString:@"Medical Certificates"]) {
        for(DocumentationModel* model in self.tableData) {
            if ([model.planType isEqualToString:@"medical_medical_certificate_plan"]) {
                [dataArr addObject:model];
            }
        }
    } else if ([filterType isEqualToString:@"Medical Reports"]) {
        for(DocumentationModel* model in self.tableData) {
            if ([model.planType isEqualToString:@"medical_medical_report_plan"]) {
                [dataArr addObject:model];
            }
        }
    } else if ([filterType isEqualToString:@"Referrals"]) {
        for(DocumentationModel* model in self.tableData) {
            if ([model.planType isEqualToString:@"medical_referrals_plan"]) {
                [dataArr addObject:model];
            }
        }
    } else {
        dataArr = self.tableData;
    }
    
    self.filteredData = dataArr;
    [self.docListTable reloadData];
}

-(void)fetchAllDocuments {
    [DocumentationAPIManager fetchAllDocumentationWithCompletion:^(DocumentationList *documentations) {
        [self fetchDocumentsWithType:self.type andDocumentation:documentations];
    }];
}

-(void)showErrorAlert{
    
    NSString *title = @"Oops";
    NSString *message = @"No previous documents found";
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
