//
//  MailListController.h
//  Return to Work
//
//  Created by Anshumaan on 31/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "DocumentationModel.h"
#import "Jobs.h"
#import "Resume.h"



@interface MailListController : UIViewController


@property (nonatomic, strong) NSString* contactType;
@property (nonatomic, strong) DocumentationModel* selectedDocument;

@property (nonatomic, strong) Jobs* selectedJob;
@property (nonatomic, strong) Resume* selectedResume;
@property (nonatomic, strong) CoverLetter* selectedCoverLetter;

//selectedCoverLetter

@end
