//
//  MailListController.m
//  Return to Work
//
//  Created by Anshumaan on 31/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "MailListController.h"
#import "Contact.h"
#import "ContactsTableViewCell.h"
#import "CorrespondenceAPIManager.h"

@interface MailListController () <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property (nonatomic, strong) ContactList *contactList;
@property (nonatomic, strong) NSMutableArray<NSString*>* mailListArr;

@property (nonatomic, weak) IBOutlet UITableView* contactTbl;
@property (nonatomic, weak) IBOutlet UIView* headrView;

@end

@implementation MailListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.contactTbl setTableFooterView:self.headrView];
    self.mailListArr = [[NSMutableArray alloc] init];
    [self createNavigationTitleViewWithTitle:@"Email"];
    [self fetchAllContacts];
    // Do any additional setup after loading the view.
    
    NSLog(@"Type = %@" , self.selectedDocument.planType);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    for (int index = 0; index < self.contactList.contactArr.count; index ++) {
//        self.contactList.contactArr[index].isEmailSent = false;
//    }
}

-(void)fetchAllContacts {
    [CorrespondenceAPIManager fetchAllContactWithCompletion:^(ContactList *contacts) {
        NSMutableArray<Contact*>* contactArrTemp = [[NSMutableArray alloc] init];

        if (self.contactType) {
            for (Contact *contact in contacts.contactArr) {
                if ([contact.type isEqualToString:self.contactType]) {
                    contact.isEmailSent = false;
                    [contactArrTemp addObject:contact];
                }
                else{
                    NSLog(@"hala %@" , contact.type);
                }
            }
            contacts.contactArr = [[NSMutableArray alloc] initWithArray:contactArrTemp];
        }
        else{
            NSLog(@"no contact type");
        }
        
        self.contactList = contacts;
        [self.contactTbl reloadData];
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.contactList.contactArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsTableViewCell"];
    Contact *contact = self.contactList.contactArr[indexPath.row];
    [cell.contactName setText:contact.contactName];
    [cell.contactEdit setText:contact.contactEmail];
    if (contact.isSelected == YES) {
        [cell.contentView setBackgroundColor:[UIColor lightGrayColor]];
    } else {
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
    
    if (contact.isEmailSent == true) {
        [cell.emailSentLbl setHidden:false];
    } else {
        [cell.emailSentLbl setHidden:true];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Contact *contact = self.contactList.contactArr[indexPath.row];
    [contact setIsSelected:!contact.isSelected];
    self.contactTbl.reloadData;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}



-(void)mailForJobNavigationsWithContacts :(NSMutableArray<NSString*> *) contactArr{
    NSString * combinedStuff = [contactArr componentsJoinedByString:@","];
    NSMutableString *body = [NSMutableString string];
    NSString *subject = @"Subject";
    if (self.selectedJob != nil) {
        subject = @"Job Logs";
        [body appendString:@"<p>To Whom It May Concern, please see attached my job logs.<br><br> Thanks</p>"];
        NSArray *links = [self.selectedJob.jobDocPath componentsSeparatedByString:@","];
        for (int i = 1; i <= links.count; i++) {
            NSString *link = [links objectAtIndex:i - 1];
            [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
        }
    }
    else if (self.selectedResume != nil) {
        subject = @"Resume";
        NSArray *links = [self.selectedResume.resumePath componentsSeparatedByString:@","];
        for (int i = 1; i <= links.count; i++) {
            NSString *link = [links objectAtIndex:i - 1];
            [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
        }
    }
    else if (self.selectedCoverLetter != nil) {
        subject = @"Cover Letter";
        NSArray *links = [self.selectedCoverLetter.coverLetterPath componentsSeparatedByString:@","];
        for (int i = 1; i <= links.count; i++) {
            NSString *link = [links objectAtIndex:i - 1];
            [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
        }
    }
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController * mail = [[MFMailComposeViewController alloc]init];
        mail.mailComposeDelegate = self;
        [mail setMessageBody:body isHTML:YES];
        [mail setSubject:subject];
        [mail setToRecipients:contactArr];
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[@"mailto://" stringByAppendingString:combinedStuff]]]){
        NSString* to = combinedStuff;
        NSString *encodedSubject = [NSString stringWithFormat:@"SUBJECT=%@", [subject stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSString *encodedBody = [NSString stringWithFormat:@"BODY=%@", [body stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSString *encodedTo = [to stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedURLString = [NSString stringWithFormat:@"mailto:%@?%@&%@", encodedTo, encodedSubject, encodedBody];
        NSURL *mailtoURL = [NSURL URLWithString:encodedURLString];
        [[UIApplication sharedApplication] openURL:mailtoURL];
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cannot send email" message:@"Email is not available. Please check your settings." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:true completion:nil];
    }

}



-(IBAction)sendMail:(UIButton*)sender {
    NSMutableArray<NSString*> *contactArr = [[NSMutableArray alloc] init];
    for (int index = 0; index < self.contactList.contactArr.count; index ++) {
        if (self.contactList.contactArr[index].isSelected) {
            [contactArr addObject:self.contactList.contactArr[index].contactEmail];
        }
    }
    
    
    if (self.selectedJob != nil || self.selectedResume != nil || self.selectedCoverLetter != nil) {
        
        [self mailForJobNavigationsWithContacts:contactArr];
        return;
    }
    
    
    if ([MFMailComposeViewController canSendMail]) {
                
        MFMailComposeViewController * mail = [[MFMailComposeViewController alloc]init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"Subject"];
        if (self.selectedDocument) {
            NSMutableString *body = [NSMutableString string];
            // add HTML before the link here with line breaks (\n)
            
            
            if ([self.selectedDocument.planType isEqualToString:@"medical_medical_certificate_plan"]) {
                [mail setSubject:@"Medical Certificate"];
                [body appendString:@"<div>To Whom It May Concern,\n\n Please see attached my medical certificate.\n\nThanks</div>\n"];
                
                NSArray *links = [self.selectedDocument.planImage componentsSeparatedByString:@","];
                for (int i = 1; i <= links.count; i++) {
                    NSString *link = [links objectAtIndex:i - 1];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
                }
                
                [mail setMessageBody:body isHTML:YES];
            }
            else if ([self.selectedDocument.planType isEqualToString:@"medical_medical_report_plan"]){
                [mail setSubject:@"Medical Reports"];
                [body appendString:@"<div>To Whom It May Concern,\n\n Please see attached my medical reports.\n\nThanks</div>\n"];
                NSArray *links = [self.selectedDocument.planImage componentsSeparatedByString:@","];
                for (int i = 1; i <= links.count; i++) {
                    NSString *link = [links objectAtIndex:i - 1];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
                }
                [mail setMessageBody:body isHTML:YES];
            }
            else if ([self.selectedDocument.planType isEqualToString:@"medical_referrals_plan"]){
                [mail setSubject:@"Referral"];
                [body appendString:[NSString stringWithFormat:@"<div>To Whom It May Concern,\n\n Please see attached my %@ referral for approval.\n\nThanks</div>\n", self.selectedDocument.planName]];

                NSArray *links = [self.selectedDocument.planImage componentsSeparatedByString:@","];
                for (int i = 1; i <= links.count; i++) {
                    NSString *link = [links objectAtIndex:i - 1];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
                }
                [mail setMessageBody:body isHTML:YES];
            //planName
            }
            else if ([self.selectedDocument.planType isEqualToString:@"medical_reports_plan"]){
                [mail setSubject:@"Receipt Reimbursement"];
                [body appendString:[NSString stringWithFormat:@"<div>To Whom It May Concern,\n\n Please see attached my %@ receipt for reimbursement.\n\nThanks</div>\n", self.selectedDocument.planName]];
                NSArray *links = [self.selectedDocument.planImage componentsSeparatedByString:@","];
                for (int i = 1; i <= links.count; i++) {
                    NSString *link = [links objectAtIndex:i - 1];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
                }
                [mail setMessageBody:body isHTML:YES];
                //reciepts Please see attached my xxxxx (insert receipt name) receipt for reimbursement.
            }
            else if([self.selectedDocument.planType isEqualToString:@"doc_return_to_work_plan"] || [self.selectedDocument.planType isEqualToString:@"doc_injury_management_plan"]){
                if ([self.selectedDocument.planType isEqualToString:@"doc_return_to_work_plan"]) {
                    [mail setSubject:@"RTWP"];
                    [body appendString:@"<p>To Whom It May Concern, please see attached my Return to Work Plan.<br><br> Thanks</p>"];
                }
                else{
                    [mail setSubject:@"IMP"];
                    [body appendString:@"<p>To Whom It May Concern, please see attached my Injury Management Plan.<br><br> Thanks</p>"];
                }

                NSArray *links = [self.selectedDocument.planImage componentsSeparatedByString:@","];
                for (int i = 1; i <= links.count; i++) {
                    NSString *link = [links objectAtIndex:i - 1];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
                }
                [mail setMessageBody:body isHTML:YES];
            }
            else{
                UserModel* userData = [UserDetails fetchUserData];
                if (self.selectedDocument.planImage.length) {
                    [body appendString:[NSString stringWithFormat:@"<div>To Whom It May Concern, Please provide reimbursement for  %@ km for travel to my appointment with %@. Thanks %@</div>\n",self.selectedDocument.extraNote ,self.selectedDocument.planName, userData.userName]];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment</a>\n" , self.selectedDocument.planImage]];
                    [mail setMessageBody:body isHTML:YES];
                }
                else{
                    [body appendString:[NSString stringWithFormat:@"<div>To Whom It May Concern, Please provide reimbursement for  %@ km for travel to my appointment with %@. Thanks %@</div>\n" ,self.selectedDocument.extraNote,self.selectedDocument.planName, userData.userName ]];
                    [mail setMessageBody:body isHTML:YES];
                }
            }

        }
        else{
            UserModel* userData = [UserDetails fetchUserData];

            [mail setMessageBody:[NSString stringWithFormat:@"To Whom It May Concern, Please provide reimbursement for  x km for travel to my appointment with . Thanks %@", userData.userName ] isHTML:nil];
        }
        
        
        
        
        
        [mail setToRecipients:contactArr];
        [self presentViewController:mail animated:YES completion:NULL];
        
    }
    else {
        NSString * combinedStuff = [contactArr componentsJoinedByString:@","];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[@"mailto://" stringByAppendingString:combinedStuff]]]) {
            
            
            
            NSString* subject = @"Subject";
            //NSString* body = @"To Whom It May Concern, Please provide reimbursement for  x km for travel to my appointment with xxxxx. Thanks xxxxx";
            NSMutableString *body = [NSMutableString string];
            // add HTML before the link here with line breaks (\n)
            
            
            if ([self.selectedDocument.planType isEqualToString:@"medical_medical_certificate_plan"]) {
                subject = @"Medical Certificate";
                [body appendString:@"<div>To Whom It May Concern,\n\n Please see attached my medical certificate.\n\nThanks</div>\n"];
                
                NSArray *links = [self.selectedDocument.planImage componentsSeparatedByString:@","];
                for (int i = 1; i <= links.count; i++) {
                    NSString *link = [links objectAtIndex:i - 1];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
                }
                
                
            }
            else if ([self.selectedDocument.planType isEqualToString:@"medical_medical_report_plan"]){
                subject = @"Medical Reports";
                [body appendString:@"<div>To Whom It May Concern,\n\n Please see attached my medical reports.\n\nThanks</div>\n"];
                NSArray *links = [self.selectedDocument.planImage componentsSeparatedByString:@","];
                for (int i = 1; i <= links.count; i++) {
                    NSString *link = [links objectAtIndex:i - 1];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
                }
                
            }
            else if ([self.selectedDocument.planType isEqualToString:@"medical_referrals_plan"]){
                subject = @"Referral";
                [body appendString:[NSString stringWithFormat:@"<div>To Whom It May Concern,\n\n Please see attached my %@ referral for approval.\n\nThanks</div>\n", self.selectedDocument.planName]];
                NSArray *links = [self.selectedDocument.planImage componentsSeparatedByString:@","];
                for (int i = 1; i <= links.count; i++) {
                    NSString *link = [links objectAtIndex:i - 1];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
                }
                //planName
            }
            else if ([self.selectedDocument.planType isEqualToString:@"medical_reports_plan"]){
                subject = @"Receipt Reimbursement";
                [body appendString:[NSString stringWithFormat:@"<div>To Whom It May Concern,\n\n Please see attached my %@ receipt for reimbursement.\n\nThanks</div>\n", self.selectedDocument.planName]];
                NSArray *links = [self.selectedDocument.planImage componentsSeparatedByString:@","];
                for (int i = 1; i <= links.count; i++) {
                    NSString *link = [links objectAtIndex:i - 1];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
                }
                //reciepts Please see attached my xxxxx (insert receipt name) receipt for reimbursement.
            }
            else if([self.selectedDocument.planType isEqualToString:@"doc_return_to_work_plan"] || [self.selectedDocument.planType isEqualToString:@"doc_injury_management_plan"]){
                if ([self.selectedDocument.planType isEqualToString:@"doc_return_to_work_plan"]) {
                    subject = @"RTWP";
                    [body appendString:@"<p>To Whom It May Concern, please see attached my Return to Work Plan.<br><br> Thanks</p>"];
                    
                }
                else{
                    subject = @"IMP";
                    [body appendString:@"<p>To Whom It May Concern, please see attached my Injury Management Plan.<br><br> Thanks</p>"];
                }
                

                
                NSArray *links = [self.selectedDocument.planImage componentsSeparatedByString:@","];
                for (int i = 1; i <= links.count; i++) {
                    NSString *link = [links objectAtIndex:i - 1];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment %i</a>\n<br>" , link , i]];
                }
            }
            else{
                UserModel* userData = [UserDetails fetchUserData];

                
                if (self.selectedDocument.planImage.length) {
                    [body appendString:[NSString stringWithFormat:@"<div>To Whom It May Concern, Please provide reimbursement for  %@ km for travel to my appointment with %@. Thanks %@</div>\n" ,self.selectedDocument.extraNote ,self.selectedDocument.planName, userData.userName]];
                    
                }
                else{
                    [body appendString:[NSString stringWithFormat:@"<div>To Whom It May Concern, Please provide reimbursement for  %@ km for travel to my appointment with %@. Thanks %@</div>\n" ,self.selectedDocument.extraNote ,self.selectedDocument.planName, userData.userName]];
                    [body appendString:[NSString stringWithFormat:@"<a href=\"%@\">Attachment</a>\n" , self.selectedDocument.planImage]];
                    
                }
            }
            
            
            
            
            
            
            
            
            
            NSString* to = combinedStuff;
            
            NSString *encodedSubject = [NSString stringWithFormat:@"SUBJECT=%@", [subject stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSString *encodedBody = [NSString stringWithFormat:@"BODY=%@", [body stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSString *encodedTo = [to stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSString *encodedURLString = [NSString stringWithFormat:@"mailto:%@?%@&%@", encodedTo, encodedSubject, encodedBody];
            NSURL *mailtoURL = [NSURL URLWithString:encodedURLString];
            
            
            
            
            
            [[UIApplication sharedApplication] openURL:mailtoURL];
        }
        else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cannot send email" message:@"Email is not available. Please check your settings." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:true completion:nil];
        }
        NSLog(@"This device cannot send email");
    }
}





- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            for (int index = 0; index < self.contactList.contactArr.count; index ++) {
                if (self.contactList.contactArr[index].isSelected) {
                    self.contactList.contactArr[index].isEmailSent = true;
                }
            }
            [self.contactTbl reloadData];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    if (result == MessageComposeResultCancelled)
        NSLog(@"Message cancelled");
    else if (result == MessageComposeResultSent){
        for (int index = 0; index < self.contactList.contactArr.count; index ++) {
            if (self.contactList.contactArr[index].isSelected) {
                self.contactList.contactArr[index].isEmailSent = true;
            }
        }
        [self.contactTbl reloadData];
        NSLog(@"Message sent");
    }
    else
        NSLog(@"Message failed");
}


-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}

@end
