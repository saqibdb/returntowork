//
//  SaveInjuryProgress.m
//  Return to Work
//
//  Created by Anshumaan on 07/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import "SaveInjuryProgress.h"
#import "EmojiView.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "UserModel.h"
#import "UserDetails.h"

@import Firebase;
@import FirebaseDatabase;

@interface SaveInjuryProgress ()

@property (nonatomic, weak) IBOutlet UISlider* painSlider;
@property (nonatomic, weak) IBOutlet UITextView* notesTxtVw;
@property (nonatomic, weak) IBOutlet UIView *emojiCollectionVw;

@property (weak, nonatomic) IBOutlet UIDatePicker *progressDatePicker;




@property (nonatomic) int painIndex;
@property (nonatomic) int feelIndex;

@end

@implementation SaveInjuryProgress

- (void)viewDidLoad {
    [super viewDidLoad];
    
    int i = 1;
    _feelIndex = 1;
    self.painIndex = 0;
    for (EmojiView* subview in self.emojiCollectionVw.subviews) {
        
        
        EmojiView* emojiView = [subview viewWithTag:1];
        [emojiView showImageHighlight:YES];
        
        subview.tag = i;
        i = i + 1;
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [subview addGestureRecognizer:singleFingerTap];
        
        
        
    }
    
    // Do any additional setup after loading the view.
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer{
    
    
    for (EmojiView* subview in self.emojiCollectionVw.subviews) {
        [subview showImageHighlight:false];
    }
    
    EmojiView* selectedView = (EmojiView*)recognizer.view;
    [selectedView showImageHighlight:YES];
    _feelIndex =(int) selectedView.tag;
    
    //Do stuff here...
    
    //[self saveProgress:nil];
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)sliderValueChanged:(UISlider*)sender {
    //[self setEmojisWithPainIndex:sender.value];
    self.painIndex = sender.value;
}

-(void)setEmojisWithPainIndex:(int)value {
    
    int tag = (value + 1)/2;
    for (EmojiView* subview in self.emojiCollectionVw.subviews) {
        [subview showImageHighlight:false];
    }
    
    for (EmojiView* subview in self.emojiCollectionVw.subviews) {
        EmojiView* emojiView = [subview viewWithTag:tag];
        [emojiView showImageHighlight:YES];
    }
}


-(IBAction)saveProgress:(UIButton*)sender {
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    FIRDatabaseReference *ref = [[FIRDatabase database] reference];
    FIRDatabaseReference *injuryRef = [[[ref child:@"Injury Progress"] child:@"Injury Progress"] childByAutoId];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd yyyy"];
    NSLog(@"%@",[dateFormatter stringFromDate:self.progressDatePicker.date]);
    
    
    NSString *painIndexString = [NSString stringWithFormat:@"%d",self.painIndex];
    
    if (self.painIndex == 0) {
        painIndexString = @"No Pain";
    }
    else if(self.painIndex == 10){
        painIndexString = @"Extreme Pain";
    }
    

    
    
    UserModel* userData = [UserDetails fetchUserData];
    NSDictionary *requestDict = @{@"injuryId": injuryRef.key,
                                  @"painIndex": painIndexString,
                                  @"notes": self.notesTxtVw.text,
                                  @"userId": userData.userId,
                                  @"feelingIndex" : [NSString stringWithFormat:@"%i",_feelIndex],
                                  @"progressDate" :[dateFormatter stringFromDate:self.progressDatePicker.date]
                                  };
    [injuryRef setValue:requestDict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        [SVProgressHUD dismiss];
        if(error){
            [self showSuccessAlert:false];
        }else{
            [self showSuccessAlert:true];
        }
    }];
}

-(void)showSuccessAlert:(Boolean)success{
    
    NSString *title = @"Success";
    NSString *message = @"Progress Saved Successfully";
    
    if(!success){
        title = @"Failure";
        message = @"There was an error in saving progress";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
