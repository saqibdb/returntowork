//
//  InjuryProgressController.m
//  Return to Work
//
//  Created by Anshumaan on 07/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import "InjuryProgressController.h"
#import "SaveInjuryProgress.h"
#import "ProgressListController.h"

@interface InjuryProgressController ()

@property (nonatomic, weak) IBOutlet UIView *containerView;

@end

@implementation InjuryProgressController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SaveInjuryProgress* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SaveInjuryProgress class])];
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    [self.containerView addSubview:controller.view];
    controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    
    [self createNavigationTitleViewWithTitle:@"Correspondence"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)segmentedControlChanged:(UISegmentedControl*)sender {
    if (sender.selectedSegmentIndex == 0) {
        [[self.containerView.subviews objectAtIndex:0] removeFromSuperview];
        SaveInjuryProgress* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SaveInjuryProgress class])];
        [self addChildViewController:controller];
        [controller didMoveToParentViewController:self];
        [self.containerView addSubview:controller.view];
        controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    } else {
        ProgressListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ProgressListController class])];
        [self addChildViewController:controller];
        [controller didMoveToParentViewController:self];
        [self.containerView addSubview:controller.view];
        controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    }
}


-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    self.navigationItem.titleView = newView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
