//
//  ProgressListController.m
//  Return to Work
//
//  Created by Anshumaan on 08/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import "ProgressListController.h"
#import "InjuryProgressModel.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "ProgressListCell.h"
#import <AMSmoothAlert/AMSmoothAlertView.h>



@import Firebase;
@import FirebaseDatabase;

@interface ProgressListController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *progressListTbl;
@property (nonatomic, strong) NSMutableArray<InjuryProgressModel*>* progressArr;
@property (nonatomic, strong) NSArray *colorsArray;

@end

@implementation ProgressListController

- (void)viewDidLoad {
    [super viewDidLoad];
    _colorsArray = [[NSArray alloc] initWithObjects:[UIColor purpleColor], [UIColor yellowColor], [UIColor blueColor], [UIColor greenColor], nil];
    [self fetchAllInjuryProgress];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchAllInjuryProgress {
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    FIRDatabaseReference* progressRef = [[FIRDatabase database] reference];
    [[[progressRef child:@"Injury Progress"] child:@"Injury Progress"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [SVProgressHUD dismiss];
        InjuryProgressList *injuryProgress = [[InjuryProgressList alloc] initWithDict:snapshot.value];
        if (injuryProgress.progressList.count == 0) {
            //[self showErrorAlert];
        } else {
            self.progressArr = injuryProgress.progressList;
            [self.progressListTbl reloadData];
        }
    }];
}

-(void)showErrorAlert{
    
    NSString *title = @"Oops";
    NSString *message = @"No previous injury progress found";
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.progressArr.count;
}

-(nonnull UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProgressListCell* cell = (ProgressListCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ProgressListCell class]) forIndexPath:indexPath];
    [cell addDataOnUI:self.progressArr[indexPath.row] andIndex:indexPath.row];
    [cell.colorView setBackgroundColor:self.colorsArray[indexPath.row%4]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self displayDataAlert:self.progressArr[indexPath.row]];
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        NSLog(@"Action to perform with Delete");
        
        
        
        
        InjuryProgressModel *injuryProgressModel = self.progressArr[indexPath.row];
        
        FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Injury Progress"] child:@"Injury Progress"];
        FIRDatabaseReference* contactRef = [correspondenceRef child:injuryProgressModel.injuryId];
        
        [SVProgressHUD showWithStatus:@"Deleting..."];
        
        [contactRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            
            [SVProgressHUD dismiss];
            if (error) {
                //[self showSuccessAlert:false];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                
                
            } else {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Injury Progress has been deleted." andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
                
                
                [self fetchAllInjuryProgress];
                
                
                //[self showSuccessAlert:true];
            }
        }];
        
        
        
        
    }];
    button.backgroundColor = [UIColor redColor]; //arbitrary color
    
    
    
    return @[button]; //array with all the buttons you want. 1,2,3, etc...
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES; //tableview must be editable or nothing will work...
}



-(void)displayDataAlert :(InjuryProgressModel*)model {
    
    NSString *alertMsg = @"Feeling | Pain | Date | Notes";
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Progress Details"] message:alertMsg preferredStyle:UIAlertControllerStyleAlert];
    NSString *title1 = [self getFeelingFromPainIndex:model.feelingIndex];
    if (title1 == nil) {
        title1 = @"";
    }
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:title1 style:UIAlertActionStyleDefault handler:nil];
    NSString *title2 = model.painIndex;
    if (title2 == nil) {
        title2 = @"";
    }
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:title2 style:UIAlertActionStyleDefault handler:nil];
    NSString *title3 = model.progressDate;
    if (title3 == nil) {
        title3 = @"";
    }
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:title3 style:UIAlertActionStyleDefault handler:nil];
    NSString *title4 = model.notes;
    if (title4 == nil) {
        title4 = @"";
    }
    UIAlertAction *action4 = [UIAlertAction actionWithTitle:title4 style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:action1];
    [alert addAction:action2];
    [alert addAction:action3];
    [alert addAction:action4];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(NSString*)getFeelingFromPainIndex:(NSString*)painIndex {
    
    
    
    if ([painIndex isEqualToString:@"1"]) {
        return @"Very Happy";
    } else if ([painIndex isEqualToString:@"2"]) {
        return @"Happy";
    } else if ([painIndex isEqualToString:@"3"] ) {
        return @"OK";
    } else if ([painIndex isEqualToString:@"4"]) {
        return @"Sad";
    } else if ([painIndex isEqualToString:@"5"] ) {
        return @"Very Sad";
    }
    return @"";
}



-(void)showAlert:(InjuryProgressModel*)model {
    NSString* title = [NSString stringWithFormat:@"IP%@ -> Notes", model.painIndex];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title message:[NSString stringWithFormat:@"Notes = %@" ,model.notes] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:ok];
     [self presentViewController:alert animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
