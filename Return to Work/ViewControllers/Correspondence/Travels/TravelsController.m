//
//  TravelsController.m
//  Return to Work
//
//  Created by Anshumaan on 18/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "TravelsController.h"
#import "DateSelectionController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "DocumentationModel.h"
#import "DocumentationAPIManager.h"
#import "AllDocumentsController.h"
#import "MailListController.h"

@interface TravelsController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *dateLbl;
@property (nonatomic, weak) IBOutlet UITextField *certNameLbl;
@property (nonatomic, weak) IBOutlet UITextField *noteNameLbl;
@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UIView *borderVw;

@property (nonatomic, strong) NSString* imagePath;
@property (nonatomic, strong) NSString* imageUrl;

@end

@implementation TravelsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createNavigationTitleViewWithTitle:@"Travel"];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd yyyy"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    self.dateLbl.text = [dateFormatter stringFromDate:[NSDate date]];
    
    [self addShadow];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}


-(IBAction)emailToParties:(UIButton*)sender {
    UIStoryboard *dashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:[NSBundle mainBundle]];
    MailListController *controller = [dashboard instantiateViewControllerWithIdentifier:NSStringFromClass([MailListController class])];
    controller.contactType = @"Insurance Company";
    [self.navigationController pushViewController:controller animated:YES];
}

-(IBAction)editCerTDetails:(UIButton*)sender {
    [self.certNameLbl becomeFirstResponder];
}

-(IBAction)uploadCert:(UIButton*)sender {
    UIAlertController* alertCtrl = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //Create an action
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = true;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    UIAlertAction *imageGallery = [UIAlertAction actionWithTitle:@"Image Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.delegate = self;
        imagePicker.allowsEditing = true;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    
    //Add action to alertCtrl
    [alertCtrl addAction:camera];
    [alertCtrl addAction:imageGallery];
    [alertCtrl addAction:cancel];
    [self presentViewController:alertCtrl animated:YES completion:nil];
}

-(IBAction)saveDetails:(UIButton*)sender {
//    if (self.imagePath != nil) {
//        [self uploadImagePath:self.imagePath];
//    }
    [self saveDocumentDetails];
}

-(IBAction)selectDate:(UIButton*)sender {
    
    DateSelectionController *picker = [[DateSelectionController alloc] initWithNibName:NSStringFromClass([DateSelectionController class]) bundle:[NSBundle mainBundle]];
    [picker loadDatePicker:^(id  _Nullable responseObject) {
        [self.dateLbl setText:responseObject];
    }];
//    UIViewController* baseController = [self getController];
    [self addChildViewController:picker];
    [picker didMoveToParentViewController:self];
    [self.view addSubview:picker.view];
    [UIView animateWithDuration:0.3 animations:^{
        [picker.view setFrame:self.view.frame];
    } completion:^(BOOL finished) {
        
    }];
}

-(UIViewController *)getController{
    
    id responder = [self nextResponder];
    do{
        responder = [responder nextResponder];
    }while (![responder  isKindOfClass:[TravelsController class]]);
    return responder;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
    
    NSData *webData = [self compress:selectedImage];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", self.certNameLbl.text]];
    [webData writeToFile:localFilePath atomically:YES];
    self.imagePath = localFilePath;
    //    [self uploadImageData:selectedImage andPath:localFilePath];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(NSData*)compress:(UIImage*)image {
    int kMaxUploadSize = 150000;
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    NSData* imageData;
    do {
        imageData = UIImageJPEGRepresentation(image, compression);
        compression -= 0.10;
    } while ([imageData length] > kMaxUploadSize && compression > maxCompression);
    
    return imageData;
}

-(void)uploadImagePath:(NSString*)imagePath {
    NSURL *localFile = [[NSURL alloc] initFileURLWithPath:imagePath];
    FIRStorageReference *storageRef = [[FIRStorage storage] reference];
    if (![self.certNameLbl.text isEqualToString:@""]) {
        FIRStorageReference *imageRef = [storageRef child:[NSString stringWithFormat:@"%@.png", self.certNameLbl.text]];
        [SVProgressHUD showWithStatus:@"Please Wait..."];
        [imageRef putFile:localFile metadata:nil completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
            [SVProgressHUD dismiss];
            NSLog(@"imageAdded");
            self.imageUrl = [metadata.downloadURLs[0] absoluteString];
            [self saveDocumentDetails];
        }];
    } else {
        NSLog(@"no name");
    }
}

-(void)saveDocumentDetails {
    
    DocumentationModel *docModel = [[DocumentationModel alloc] initWithPlanDate:self.dateLbl.text withPlanName:self.certNameLbl.text andPlanImage:@"" withExtraNote:self.noteNameLbl.text andPlanType:@"travel_plan"];
    
    [DocumentationAPIManager saveDocumentsForRTWWithDocumentDetails:docModel onError:^(NSError *err) {
        if (err != nil) {
            [self showSuccessAlert:false];
        } else {
            [self showSuccessAlert:true];
        }
    }];
}

-(void)showSuccessAlert:(Boolean)success{
    
    NSString *title = @"Success";
    NSString *message = @"Document Saved Successfully";
    
    if(!success){
        title = @"Failure";
        message = @"There was an error in saving document";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(IBAction)segmentedControlChanged:(UISegmentedControl*)sender {
    AllDocumentsController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AllDocumentsController class])];
    if (sender.selectedSegmentIndex == 0) {
        if (self.containerView.subviews.count > 1) {
            [self.containerView.subviews[1] removeFromSuperview];
        }
    } else {
        controller.type = @"travel_plan";
//        controller.docList = self.docList;
        controller.mainTitle = @"Travel";
        controller.subTitle = @"";
        controller.imageIcon = [UIImage imageNamed:@"Travel_Plan"];
        
        [self addChildViewController:controller];
        [controller didMoveToParentViewController:self];
        [self.containerView addSubview:controller.view];
        
        controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    }
}


-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(20, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 100, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 100, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}

@end

