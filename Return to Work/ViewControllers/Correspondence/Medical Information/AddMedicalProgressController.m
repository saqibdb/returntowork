//
//  AddMedicalProgressController.m
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "AddMedicalProgressController.h"
#import "MedicalCertificatesController.h"
#import "MedicalReportsController.h"
#import "ReferralsController.h"

@interface AddMedicalProgressController ()

@property (nonatomic, weak) IBOutlet UIView *containerView;

@end

@implementation AddMedicalProgressController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MedicalCertificatesController* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MedicalCertificatesController class])];
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    [self.containerView addSubview:controller.view];
    controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)segmentedControlSelected:(UISegmentedControl*)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        MedicalCertificatesController* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MedicalCertificatesController class])];
        [self addChildViewController:controller];
        [controller didMoveToParentViewController:self];
        [self.containerView.subviews[0] removeFromSuperview];
        [self.containerView addSubview:controller.view];
        controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    } else if (sender.selectedSegmentIndex == 1) {
        ReferralsController* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ReferralsController class])];
        [self addChildViewController:controller];
        [controller didMoveToParentViewController:self];
        [self.containerView.subviews[0] removeFromSuperview];
        [self.containerView addSubview:controller.view];
        controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    } else {
        MedicalReportsController* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MedicalReportsController class])];
        [self addChildViewController:controller];
        [controller didMoveToParentViewController:self];
        [self.containerView.subviews[0] removeFromSuperview];
        [self.containerView addSubview:controller.view];
        controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
