//
//  MedicalInformationController.m
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "MedicalInformationController.h"
#import "AddMedicalProgressController.h"
#import "AllDocumentsController.h"
#import "DocumentationModel.h"
#import "DocumentationAPIManager.h"

@interface MedicalInformationController ()

@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UIView *borderVw;

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;




@property (nonatomic, strong) DocumentationList* docList;

@end

@implementation MedicalInformationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self createNavigationTitleViewWithTitle:@"Correspondence"];
    
    self.mainImageView.image = [UIImage imageNamed:@"medi_info"];
    
    [self addShadow];
    
    AddMedicalProgressController* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AddMedicalProgressController class])];
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    [self.containerView addSubview:controller.view];
    controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)segmentedControlSelected:(UISegmentedControl*)sender {
    AddMedicalProgressController* controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AddMedicalProgressController class])];
    
    AllDocumentsController *docListController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AllDocumentsController class])];
    if (sender.selectedSegmentIndex == 0) {
        
        [self.containerView.subviews[0] removeFromSuperview];
        [self addChildViewController:controller];
        [controller didMoveToParentViewController:self];
        [self.containerView addSubview:controller.view];
        controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    } else {
        [self.containerView.subviews[0] removeFromSuperview];
        docListController.type = @"Medical";
        docListController.docList = self.docList;
        
        
     
        
        
        docListController.mainTitle = @"Medical Information";
        docListController.subTitle = @"";
        docListController.imageIcon = [UIImage imageNamed:@"medi_info"];
        
        
        
        [self addChildViewController:docListController];
        [docListController didMoveToParentViewController:self];
        [self.containerView addSubview:docListController.view];
        
        docListController.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    }
}


-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
