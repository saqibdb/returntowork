//
//  TrackRTWReviewHrsController.m
//  Return to Work
//
//  Created by Anshumaan on 21/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "TrackRTWReviewHrsController.h"
#import "TrackRTWReviewHrsCell.h"
#import "TrackingModel.h"
#import "DocumentationAPIManager.h"
#import "ContactsTableViewCell.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>


@interface TrackRTWReviewHrsController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak)IBOutlet UITableView *reviewTable;
@property (nonatomic) TrackingList *reviewItems;
@property (nonatomic, strong) NSArray *colorsArray;
@end

@implementation TrackRTWReviewHrsController

- (void)viewDidLoad {
    [super viewDidLoad];
    _colorsArray = [[NSArray alloc] initWithObjects:[UIColor purpleColor], [UIColor yellowColor], [UIColor blueColor], [UIColor greenColor], nil];
    [self fetchAllTrackData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.reviewItems.trackingArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ContactsTableViewCell *cell = (ContactsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ContactsTableViewCell class])];
    
    
    TrackingModel *model = self.reviewItems.trackingArr[indexPath.row];
    [cell.contactName setText:[NSString stringWithFormat:@"%@ | %@ hours", model.trackDate, model.hours]];
    
    
    [cell.colorView setBackgroundColor:self.colorsArray[indexPath.row%4]];
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // view details
    
}


-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        NSLog(@"Action to perform with Delete");
        
        
        
        
        TrackingModel *model = self.reviewItems.trackingArr[indexPath.row];

        FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Tracking"] child:@"Tracking"];
        FIRDatabaseReference* contactRef = [correspondenceRef child:model.trackId];
        
        [SVProgressHUD showWithStatus:@"Deleting..."];
        
        [contactRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            
            [SVProgressHUD dismiss];
            if (error) {
                //[self showSuccessAlert:false];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                
                
            } else {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Tracking has been deleted." andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
                
                
                [self fetchAllTrackData];
                
                
                //[self showSuccessAlert:true];
            }
        }];
        
        
        
        
    }];
    button.backgroundColor = [UIColor redColor]; //arbitrary color
    
    
    
    return @[button]; //array with all the buttons you want. 1,2,3, etc...
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES; //tableview must be editable or nothing will work...
}










-(void)fetchAllTrackData {
    [DocumentationAPIManager fetchAllTrackedDateWithCompletion:^(TrackingList *trackList) {
        
        if (trackList == nil || trackList.trackingArr.count == 0) {
            [self showErrorAlert];
        } else {
            self.reviewItems = trackList;
            [self.reviewTable reloadData];
        }
    }];
}

-(void)showErrorAlert{
    
    NSString *title = @"Oops";
    NSString *message = @"No previous track data found";
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
