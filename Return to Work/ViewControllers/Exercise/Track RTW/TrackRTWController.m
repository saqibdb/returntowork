//
//  TrackRTWController.m
//  Return to Work
//
//  Created by Anshumaan on 21/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "TrackRTWController.h"
#import "TrackRTWAddHrsController.h"
#import "TrackRTWReviewHrsController.h"
#import "DateSelectionController.h"
#import "DocumentationAPIManager.h"
#import "TrackingModel.h"

@interface TrackRTWController ()
@property (nonatomic, weak)IBOutlet UISegmentedControl *segment;
@property (nonatomic, weak) IBOutlet UITextField *hoursTxtFld;
@property (nonatomic, weak) IBOutlet UILabel *dateLbl;
@property (nonatomic, weak)IBOutlet UIView *containerView;
@property (nonatomic, weak)UIViewController *currentController;
@end

@implementation TrackRTWController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd yyyy"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    self.dateLbl.text = [dateFormatter stringFromDate:[NSDate date]];
    
    [self createNavigationTitleViewWithTitle:@"Track Return To Work"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)selectDate:(UIButton*)sender {
    
    DateSelectionController *picker = [[DateSelectionController alloc] initWithNibName:NSStringFromClass([DateSelectionController class]) bundle:[NSBundle mainBundle]];
    [picker loadDatePicker:^(id  _Nullable responseObject) {
        [self.dateLbl setText:responseObject];
    }];
    //    UIViewController* baseController = [self getController];
    [self addChildViewController:picker];
    [picker didMoveToParentViewController:self];
    [self.view addSubview:picker.view];
    [UIView animateWithDuration:0.3 animations:^{
        [picker.view setFrame:self.view.frame];
    } completion:^(BOOL finished) {
        
    }];
}


-(IBAction)segmentedControlChanged:(UISegmentedControl*)sender {
    TrackRTWReviewHrsController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([TrackRTWReviewHrsController class])];
    if (sender.selectedSegmentIndex == 0) {
        if (self.containerView.subviews.count > 1) {
            [self.containerView.subviews[1] removeFromSuperview];
        }
    } else {
        [self addChildViewController:controller];
        [controller didMoveToParentViewController:self];
        [self.containerView addSubview:controller.view];
        
        controller.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    }
}

-(IBAction)saveDetails:(UIButton*)sender {
    
    if (![self.hoursTxtFld.text isEqualToString:@""]) {
        TrackingModel* model = [[TrackingModel alloc] initWithTrackDate:self.dateLbl.text andTrackHours:self.hoursTxtFld.text];
        [DocumentationAPIManager saveTrackingDetails:model];
    }
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(20, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-50, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 100, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
