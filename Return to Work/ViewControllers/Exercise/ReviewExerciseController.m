//
//  ReviewExerciseController.m
//  Return to Work
//
//  Created by Anshumaan on 21/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "ReviewExerciseController.h"
#import "ReviewExerciseCell.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ImageViewer.h"
#import "ExerciseDetailsController.h"

#import "Return_to_Work-Swift.h"



@import Firebase;
@import FirebaseAuth;

@interface ReviewExerciseController ()

@end

@implementation ReviewExerciseController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self fetchAllExerciseWithCompletion:^(ExerciseList *exerice) {
        
        if ((exerice == nil) || (exerice.exerciseArr.count == 0)) {
            [self showErrorAlert];
        } else {
            self.exercises = exerice.exerciseArr;
            [self.collectionView setDataSource:self];
            [self.collectionView setDelegate:self];
            [self.collectionView reloadData];
        }
    }];

}

-(void)showErrorAlert{
    
    NSString *title = @"Oops";
    NSString *message = @"No previous exercise found";
  
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.exercises.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"ReviewExerciseCell";
    ReviewExerciseCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    Exercise *modal = [self.exercises objectAtIndex:indexPath.row];
    
    cell.imgView.image = [UIImage imageNamed:modal.exerciseUrl];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:modal.exerciseUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        printf("image downloaded");
    }];
    [cell.removeBtn setTag:indexPath.item];
    [cell.removeBtn addTarget:self action:@selector(deleteDocument:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"NEED BIG HERE");
    
    
    
    ReviewExerciseCell *cell = (ReviewExerciseCell *)[collectionView cellForItemAtIndexPath:indexPath];

    
    [[TestSwift new] showTheFullImageWithImageView:cell.imgView viewController:self];

    
    
    
    /*
    
    Exercise *modal = [self.exercises objectAtIndex:indexPath.row];
    ImageViewer *controller = [[ImageViewer alloc] initWithNibName:NSStringFromClass([ImageViewer class]) bundle:[NSBundle mainBundle]];
    UIViewController* baseController = [self getController];
    [baseController addChildViewController:controller];
    [controller didMoveToParentViewController:baseController];
    [controller fetchImageUrl:modal.exerciseUrl];
    controller.view.frame = CGRectMake(0, 0, baseController.view.frame.size.width, baseController.view.frame.size.height);
    [baseController.view addSubview:controller.view];
     
     */
}

-(UIViewController *)getController{
    
    id responder = [self nextResponder];
    do{
        responder = [responder nextResponder];
    }while (![responder  isKindOfClass:[ExerciseDetailsController class]]);
    return responder;
}


#pragma mark collection view cell paddings

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.collectionView.frame.size.width/4 - 10 , 90);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

-(void)deleteDocument:(UIButton*)sender {
    FIRDatabaseReference *ref = [[[[FIRDatabase database] reference] child:@"Exercise"] child:@"Exercise"];
    NSString* key = self.exercises[sender.tag].exerciseId;
    [[ref child:key] removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        [self fetchAllExerciseWithCompletion:^(ExerciseList *exercises) {
            self.exercises = exercises.exerciseArr;
            [self.collectionView reloadData];
        }];
    }];
}


-(void)fetchAllExerciseWithCompletion:(void(^)(ExerciseList*))completion {

    [SVProgressHUD showWithStatus:@"Please wait..."];

    FIRDatabaseReference* contactRef = [[FIRDatabase database] reference];
    [[[contactRef child:@"Exercise"] child:@"Exercise"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [SVProgressHUD dismiss];

        ExerciseList *exercise = [[ExerciseList alloc] initWithDict:snapshot.value];
        completion(exercise);
    }];
}


@end
