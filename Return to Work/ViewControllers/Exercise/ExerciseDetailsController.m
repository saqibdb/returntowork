//
//  ExerciseDetailsController.m
//  Return to Work
//
//  Created by Anshumaan on 21/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "ExerciseDetailsController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "ReviewExerciseController.h"
#import "TimerView.h"
#import "CalendarView.h"
#import "UserModel.h"
#import "UserDetails.h"

@import Firebase;
@import FirebaseAuth;
@import FirebaseStorage;

@interface ExerciseDetailsController () {
    ReviewExerciseController *review;
    TimerView *timerView;
    CalendarView *calendarView;
}
@property (nonatomic, weak) IBOutlet UISegmentedControl *exerciseSegment;
@property (nonatomic, weak) IBOutlet UISegmentedControl *accessorySegment;
@property (nonatomic, weak) IBOutlet UIView *accessoryView;
@property (nonatomic, weak) Correspondence* exerciseType;
@property (nonatomic, weak) IBOutlet UILabel *nameLbl;
@property (nonatomic, weak) IBOutlet UIImageView *image;
@property (nonatomic, weak) IBOutlet UIView *borderVw;
@property (nonatomic, weak) IBOutlet UIView *musicView;

@property (weak, nonatomic) IBOutlet UIView *reminderView;
@property (weak, nonatomic) IBOutlet UIDatePicker *reminderTimeAndDatePicker;









@end

@implementation ExerciseDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.nameLbl setText:self.exerciseType.name];
    [self.image setImage:[UIImage imageNamed:self.exerciseType.icon]];
    [self createNavigationTitleViewWithTitle:@"Exercises"];
    [self addShadow];
    [self.reminderView setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}

-(void)loadWithExerciseDetails:(Correspondence*)eType{
    self.exerciseType = eType;    
}

-(IBAction)exerciseSegmentSelected:(UISegmentedControl *)sender{
    
    switch (sender.selectedSegmentIndex) {
        case 0:
            // upload Exercise
            [self removeSubView];
            break;

        case 1:
            //review Exercise
            [self addCollectionView];
            break;
            
        default:
            break;
    }
}

-(void)addCollectionView {
    
    review = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewExerciseController"];
    [review.view setFrame:CGRectMake(0, self.uploadView.frame.origin.y, self.view.frame.size.width, self.uploadView.frame.size.height)];
    [review.view bringSubviewToFront:self.view];
    [self.view addSubview:review.view];
    [review.view setBackgroundColor:[UIColor clearColor]];
    
}

-(void)removeSubView {
    if (review != nil) {
        [review.view removeFromSuperview];
    }
}



-(IBAction)accessorySegmentSelected:(UISegmentedControl *)sender{
    
    [timerView removeFromSuperview];
    [calendarView removeFromSuperview];
    
    switch (sender.selectedSegmentIndex) {
        case 0:
            //music
            [self.reminderView setHidden:YES];
            [self.musicView setHidden:false];
            break;
            
        case 1:
            // timer
            [self.reminderView setHidden:YES];
            [self addTimerView];
            [self.musicView setHidden:true];
            break;
            
        case 2:
            [self addCalendarView];
            [timerView removeFromSuperview];
            [self.musicView setHidden:true];
            [self.reminderView setHidden:NO];
            break;
            
        default:
            break;
    }
    
}



-(IBAction)tapToLaunchMusic:(UIButton*)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"music://"] options:@{} completionHandler:nil];
}

-(void)addTimerView {
    
    timerView = [[[NSBundle mainBundle] loadNibNamed:@"TimerView" owner:self options:nil] objectAtIndex:0];
    [self.accessoryView addSubview:timerView];
    timerView.frame = CGRectMake(0, 0, self.accessoryView.frame.size.width, self.accessoryView.frame.size.height);
    [timerView setBackgroundColor:[UIColor clearColor]];
}

-(void)addCalendarView {
    
    calendarView = [[[NSBundle mainBundle] loadNibNamed:@"CalendarView" owner:self options:nil] objectAtIndex:0];
    [self.accessoryView addSubview:calendarView];
    calendarView.frame = CGRectMake(0, 0, self.accessoryView.frame.size.width, self.accessoryView.frame.size.height);
    [calendarView setBackgroundColor:[UIColor clearColor]];
}


-(IBAction)uploadImage:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
    
    NSData *webData = [self compress:selectedImage];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:@"test.png"];
    [webData writeToFile:localFilePath atomically:YES];
    [self upload:localFilePath];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


-(void)upload:(NSString*)filePath {
    
    NSURL *localFile = [[NSURL alloc] initFileURLWithPath:filePath];
    FIRStorageReference *storageRef = [[FIRStorage storage] reference];

    if (![filePath isEqualToString:@""]) {
    
        FIRStorageReference *imageRef = [storageRef child:[NSString stringWithFormat:@"%@.png", [self randomStringWithLength:10]]];
 
        [SVProgressHUD showWithStatus:@"Please Wait..."];
        [imageRef putFile:localFile metadata:nil completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
            
            [SVProgressHUD dismiss];
            NSLog(@"imageAdded");
            NSString *imageUrl = [metadata.downloadURLs[0] absoluteString];
            [self saveDocumentDetails:imageUrl];
            
        }];
    }

}

-(void)saveDocumentDetails:(NSString*)imageUrl {
    
//    DocumentationModel *docModel = [[DocumentationModel alloc] initWithPlanDate:nil withPlanName:nil andPlanImage:imageUrl withExtraNote:@"" andPlanType:self.planType];
//    [DocumentationAPIManager saveDocumentsForRTWWithDocumentDetails:docModel];
    
    //api call
    [self addExerciseWithImageUrl:imageUrl];
}

-(NSString *) randomStringWithLength: (int) len {
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}

-(NSData*)compress:(UIImage*)image {
    int kMaxUploadSize = 150000;
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    NSData* imageData;
    do {
        imageData = UIImageJPEGRepresentation(image, compression);
        compression -= 0.10;
    } while ([imageData length] > kMaxUploadSize && compression > maxCompression);
    
    return imageData;
}


-(void)addExerciseWithImageUrl:(NSString*)url {
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Exercise"] child:@"Exercise"];
    FIRDatabaseReference* exerciseRef = [correspondenceRef childByAutoId];
    UserModel* userData = [UserDetails fetchUserData];
    NSDictionary *requestDict = @{@"URL": url,
                                  @"ExerciseId": exerciseRef.key,
                                  @"userId": userData.userId
                                  };
    
    [exerciseRef setValue:requestDict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        [SVProgressHUD dismiss];
        if (error) {
            [self showSuccessAlert:false];
        } else {
            [self showSuccessAlert:true];
        }
    }];
}



- (IBAction)erminderSAveAction:(UIButton *)sender {
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{

        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = @"Exercise Reminder";
        
      
        NSDate *dateFromString = self.reminderTimeAndDatePicker.date;
        
        
        
        NSTimeInterval aInterval2hrs = -120 * 60;
        EKAlarm *alaram2hrs = [EKAlarm alarmWithRelativeOffset:aInterval2hrs];
        [event addAlarm:alaram2hrs];
        
        
        
        
        
        event.startDate = dateFromString;
        event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
        event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        

            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Added" message:@"Reminder Has Been Added to Calendar" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [alert addAction:ok];
            [self presentViewController:alert animated:true completion:nil];
        
        });
        
    }];
    
    
    
}





-(void)showSuccessAlert:(Boolean)success{
    
    NSString *title = @"Success";
    NSString *message = @"Exercise Saved Successfully";
    
    if(!success){
        title = @"Failure";
        message = @"There was an error in saving exercise";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(20, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 100, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 100, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}


@end
