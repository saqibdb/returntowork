//
//  ExerciseDetailsController.h
//  Return to Work
//
//  Created by Anshumaan on 21/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Correspondence.h"

@interface ExerciseDetailsController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    
}

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIView *uploadView;


-(void)loadWithExerciseDetails:(Correspondence*)eType;

@end
