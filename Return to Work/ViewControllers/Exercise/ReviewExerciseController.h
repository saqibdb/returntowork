//
//  ReviewExerciseController.h
//  Return to Work
//
//  Created by Anshumaan on 21/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Exercise.h"

@interface ReviewExerciseController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
//@property (strong, nonatomic) NSArray *imgArray;
@property (nonatomic, strong) NSMutableArray<Exercise*> *exercises;


-(void)fetchAllExerciseWithCompletion:(void(^)(ExerciseList*))completion;
//-(void)fetchAllExerciseWithCompletion:(ExerciseList*)completion;

@end
