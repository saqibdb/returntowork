//
//  ExerciseHomeController.m
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "ExerciseHomeController.h"
#import "ExerciseDetailsController.h"
#import "ContactsTableViewCell.h"
#import "Correspondence.h"

@interface ExerciseHomeController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *allContacts;
@property (nonatomic, weak) IBOutlet UITableView *exerciseListTbl;
@end

@implementation ExerciseHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addDummyContacts];
    [self createNavigationTitleViewWithTitle:@"Exercises"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.allContacts.count;
}

-(nonnull UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContactsTableViewCell *cell = (ContactsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ContactsTableViewCell class]) forIndexPath:indexPath];
    Correspondence* correspondence = self.allContacts[indexPath.row];
    [cell addData:correspondence];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ExerciseDetailsController *details = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ExerciseDetailsController class])];
    [details loadWithExerciseDetails:self.allContacts[indexPath.row]];
    [self.navigationController pushViewController:details animated:true];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

-(void)addDummyContacts {
    _allContacts = [[NSMutableArray alloc] init];
    
    Correspondence *newCorrespondence = [[Correspondence alloc] initWithName:@"Physiotherapist Exercises" withDescription:@"" andLastEdit:@"Today" andIcon:@"PhysiotherapistIcon"];
    [_allContacts addObject:newCorrespondence];
    
    
    newCorrespondence = [[Correspondence alloc] initWithName:@"Other Exercises" withDescription:@"" andLastEdit:@"Today" andIcon:@"others"];
    [_allContacts addObject:newCorrespondence];
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(70, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 200, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 200, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}


@end
