//
//  DashboardViewController.m
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "DashboardViewController.h"
#import "TrackRTWController.h"
#import "ExerciseHomeController.h"
#import "ViewController.h"
#import "AppDelegate.h"

@interface DashboardViewController ()

@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tabBarController.moreNavigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.tabBarController.moreNavigationController.navigationBar.barTintColor = [UIColor colorWithRed:(0.0 / 255.0) green:(122.0 / 255.0) blue:(255.0 / 255.0) alpha:1.0];
    [self createNavigationTitleViewWithTitle:@"Home"];

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(20, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 100, 15)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 100, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    [self.navigationController.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
    
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 5, 50, 35)];
    UIButton* logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    UIImage* backImage = [UIImage imageNamed:@"ic_shoppingCart"];
    [logoutBtn setFrame:CGRectMake(0, 5, 60, 30)];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [logoutBtn addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:logoutBtn];
    
    UIBarButtonItem* logout = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.rightBarButtonItem = logout;
}


-(void)logout:(UIButton*)sender {
    
    UIStoryboard* mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([ViewController class])];
    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:controller];
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    
    [UIView transitionWithView:appDelegate.window duration:0.6 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        [[NSUserDefaults standardUserDefaults] synchronize];
        appDelegate.window.rootViewController = navController;
    } completion:nil];
    
}


- (IBAction)contactsAction:(DBTileButton *)sender {
    [self performSegueWithIdentifier:@"ToContacts" sender:self];
}

- (IBAction)correspondenceAction:(DBTileButton *)sender {

    UIStoryboard *jobSeekerBoard  = [UIStoryboard storyboardWithName:@"Correspondence" bundle:[NSBundle mainBundle]];
    UIViewController *nextController =  [jobSeekerBoard instantiateInitialViewController];
    [self.navigationController pushViewController:nextController animated:true];

}

- (IBAction)exersiceAction:(DBTileButton *)sender {
    UIStoryboard *jobSeekerBoard  = [UIStoryboard storyboardWithName:@"Exercise" bundle:[NSBundle mainBundle]];
//    UIViewController *nextController =  [jobSeekerBoard instantiateInitialViewController];
    ExerciseHomeController *controller = [jobSeekerBoard instantiateViewControllerWithIdentifier:NSStringFromClass([ExerciseHomeController class])];
    [self.navigationController pushViewController:controller animated:true];
}


- (IBAction)jobSeekingAction:(id)sender {
    UIStoryboard *jobSeekerBoard  = [UIStoryboard storyboardWithName:@"JobSeeking" bundle:[NSBundle mainBundle]];
    UIViewController *nextController =  [jobSeekerBoard instantiateInitialViewController];
    [self.navigationController pushViewController:nextController animated:true];
}

- (IBAction)trackAction:(DBTileButton *)sender {
    UIStoryboard *jobSeekerBoard  = [UIStoryboard storyboardWithName:@"Exercise" bundle:[NSBundle mainBundle]];
    UIViewController *nextController =  [jobSeekerBoard instantiateViewControllerWithIdentifier:NSStringFromClass([TrackRTWController class])];
    [self.navigationController pushViewController:nextController animated:true];
}

@end
