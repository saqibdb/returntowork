//
//  ResumeViewController.h
//  Return to Work
//
//  Created by Anshumaan on 28/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResumeViewController : UIViewController

@property (nonatomic, strong) NSString *filePath;

@end
