//
//  ResumeListingController.h
//  
//
//  Created by Anshumaan on 15/12/17.
//

#import <UIKit/UIKit.h>
#import "Resume.h"

@interface ResumeListingController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *resumeTbl;

@property (nonatomic, strong) NSMutableArray<Resume*> *resumeList;
-(void)fetchAllResumes:(void(^)(ResumeList*))completion;

@end
