//
//  ResumeListingController.m
//  
//
//  Created by Anshumaan on 15/12/17.
//

#import "ResumeListingController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "JobListingCell.h"
#import "ResumeViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "Return_to_Work-Swift.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NewAllDocumentsViewController.h"
#import "MailListController.h"



@import Firebase;
@import FirebaseAuth;

@interface ResumeListingController ()<UITableViewDelegate , UITableViewDataSource>

@end

@implementation ResumeListingController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.resumeTbl setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self fetchAllResumes:^(ResumeList *resume) {
        if (resume == nil || resume.resumeArr.count == 0) {

        } else {
            self.resumeList = resume.resumeArr;
            [self.resumeTbl setDelegate:self];
            [self.resumeTbl setDataSource:self];
            [self.resumeTbl reloadData];
        }
    }];
}

-(void)showErrorAlert{
    
    NSString *title = @"Oops";
    NSString *message = @"No previous resumes found";
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)fetchAllResumes:(void(^)(ResumeList*))completion {

    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    FIRDatabaseReference* contactRef = [[FIRDatabase database] reference];
    [[[contactRef child:@"Resume"] child:@"Resume"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [SVProgressHUD dismiss];
        ResumeList *jobs = [[ResumeList alloc] initWithDict:snapshot.value];
        completion(jobs);
    }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.resumeList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JobListingCell *cell = [self.resumeTbl dequeueReusableCellWithIdentifier:@"JobListingCell"];
    Resume *resume = self.resumeList[indexPath.row];
    cell.jobDate.text = resume.resumeUploadDate;
    cell.jobTitle.text = resume.resumeTitle;
    [cell.randomColorView setBackgroundColor:[self randomNiceColor]];
    
    
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:resume.resumePath] placeholderImage:[UIImage imageNamed:@"placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        printf("image downloaded");
    }];
    
    cell.emailBtn.tag = indexPath.row;
    [cell.emailBtn addTarget:self action:@selector(emailToParties:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Resume *resume = self.resumeList[indexPath.row];
    NewAllDocumentsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([NewAllDocumentsViewController class])];
    vc.resumeAnother = resume;
    vc.titleNavigation = @"Job Seeking";
    vc.titleMain = @"Resume";
    vc.subTitle = @"";
    vc.specialTitle = @"Review Resume";
    vc.iconImage = [UIImage imageNamed:@"resume"];
    
    [self.navigationController pushViewController:vc animated:YES];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}


-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        NSLog(@"Action to perform with Delete");
        
        
        
        
        Resume *coverLetter = self.resumeList[indexPath.row];

        FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Resume"] child:@"Resume"];
        FIRDatabaseReference* contactRef = [correspondenceRef child:coverLetter.resumeId];
        
        [SVProgressHUD showWithStatus:@"Deleting..."];
        
        [contactRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            
            [SVProgressHUD dismiss];
            if (error) {
                //[self showSuccessAlert:false];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                
                
            } else {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Resume has been deleted." andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
                
                
                [self fetchAllResumes:^(ResumeList *resume) {
                    if (resume == nil || resume.resumeArr.count == 0) {
                        self.resumeList = resume.resumeArr;
                        [self.resumeTbl reloadData];
                    } else {
                        self.resumeList = resume.resumeArr;
                        [self.resumeTbl reloadData];
                    }
                }];
                
            }
        }];
        
        
    }];
    button.backgroundColor = [UIColor redColor]; //arbitrary color
    
    
    
    return @[button]; //array with all the buttons you want. 1,2,3, etc...
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES; //tableview must be editable or nothing will work...
}

-(void)emailToParties:(UIButton*)sender {
    
    Resume *resume = self.resumeList[sender.tag];

    UIStoryboard *dashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:[NSBundle mainBundle]];
    MailListController *controller = [dashboard instantiateViewControllerWithIdentifier:NSStringFromClass([MailListController class])];
    controller.contactType = nil;
    controller.selectedResume = resume;
    [self.navigationController pushViewController:controller animated:YES];
}


-(UIColor *)randomNiceColor {

    // Random hue from 0 to 359 degrees.
    CGFloat hue = (arc4random() % 360) / 359.0f;
    
    // Random saturation from 0.0 to 1.0
    CGFloat saturation = (float)arc4random() / UINT32_MAX;
    
    // Random brightness from 0.0 to 1.0
    CGFloat brightness = (float)arc4random() / UINT32_MAX;
    
    // Limit saturation and brightness to get a nice colors palette.
    // Remove the following 2 lines to generate a color from the full range.
    saturation = saturation < 0.5 ? 0.5 : saturation;
    brightness = brightness < 0.9 ? 0.9 : brightness;
    
    // Return a random UIColor.
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

@end
