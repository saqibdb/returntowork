//
//  ResumeHomeController.m
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "ResumeHomeController.h"
#import "AdResumeController.h"
#import "ResumeListingController.h"

@interface ResumeHomeController ()
@property (nonatomic, weak)IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic, weak)IBOutlet UIView *containerView;
@property (nonatomic, weak)UIViewController *currentController;
@property (nonatomic, weak) IBOutlet UIView *borderVw;
@end

@implementation ResumeHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createNavigationTitleViewWithTitle:@"Job Seeking"];
    // Do any additional setup after loading the view.
    [self addContentForSegment:0];
    [self addShadow];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)segmentSelected:(UISegmentedControl *)sender{
    [self addContentForSegment:sender.selectedSegmentIndex];
}

-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}


-(void)addContentForSegment:(int)index{
    
    if(self.currentController){
        [self.currentController.view removeFromSuperview];
        [self.currentController removeFromParentViewController];
        self.currentController = nil;
    }
    
    UIViewController *targetController = nil;
    switch (index) {
        case 0:
            targetController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AdResumeController class])];
            break;
            
        case 1:
            targetController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ResumeListingController class])];
            break;
            
        default:
            break;
    }
    
    [self.containerView addSubview:targetController.view];
    targetController.view.frame = self.containerView.bounds;
    [self addChildViewController:targetController];
    self.currentController = targetController;
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(20, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 100, 20)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 100, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}

@end
