//
//  AdResumeController.h
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jobs.h"
@interface AdResumeController : UITableViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UICollectionViewDataSource , UICollectionViewDelegate>


@property (weak, nonatomic) IBOutlet UICollectionView *documentsCollectionView;

@property (strong) Jobs *job;

@end
