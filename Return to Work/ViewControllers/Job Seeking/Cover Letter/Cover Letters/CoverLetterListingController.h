//
//  CoverLetterListingController.h
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Resume.h"

@interface CoverLetterListingController : UIViewController<UITableViewDelegate , UITableViewDataSource>



@property (nonatomic, strong) NSMutableArray<CoverLetter*> *coverLetterList;

@end
