//
//  CoverLetterListingController.m
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "CoverLetterListingController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "JobListingCell.h"
#import "ResumeViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "Return_to_Work-Swift.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NewAllDocumentsViewController.h"
#import "MailListController.h"



@import Firebase;
@import FirebaseAuth;

@interface CoverLetterListingController ()

@property (nonatomic, weak) IBOutlet UITableView *listingTbl;

@end

@implementation CoverLetterListingController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.listingTbl setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    self.listingTbl.delegate = self;
    self.listingTbl.dataSource = self;

    [self fetchAllResumes:^(CoverLetterList *cover) {
        
        if (cover == nil || cover.coverListArr.count == 0) {
        } else {
            self.coverLetterList = cover.coverListArr;

            [self.listingTbl reloadData];
        }
    }];
}

-(void)showErrorAlert{
    
    NSString *title = @"Oops";
    NSString *message = @"No previous cover letters found";
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)fetchAllResumes:(void(^)(CoverLetterList*))completion {
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    FIRDatabaseReference* contactRef = [[FIRDatabase database] reference];
    [[[contactRef child:@"CoverLetter"] child:@"CoverLetter"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [SVProgressHUD dismiss];
        CoverLetterList *jobs = [[CoverLetterList alloc] initWithDict:snapshot.value];
        completion(jobs);
    }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.coverLetterList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JobListingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JobListingCell"];
    CoverLetter *resume = self.coverLetterList[indexPath.row];
    cell.jobDate.text = resume.coverLetterUploadDate;
    cell.jobTitle.text = resume.coverLetterTitle;
    [cell.randomColorView setBackgroundColor:[self randomNiceColor]];
    
    
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:resume.coverLetterPath] placeholderImage:[UIImage imageNamed:@"placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        printf("image downloaded");
    }];
    
    cell.emailBtn.tag = indexPath.row;
    [cell.emailBtn addTarget:self action:@selector(emailToParties:) forControlEvents:UIControlEventTouchUpInside];

    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CoverLetter *resume = self.coverLetterList[indexPath.row];
    NewAllDocumentsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([NewAllDocumentsViewController class])];
    vc.resumeMain = resume;
    vc.titleNavigation = @"Job Seeking";
    vc.titleMain = @"Cover Letter";
    vc.subTitle = @"";
    vc.specialTitle = @"Review Cover Letter";
    vc.iconImage = [UIImage imageNamed:@"cover_letter"];
    
    [self.navigationController pushViewController:vc animated:YES];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}


-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        NSLog(@"Action to perform with Delete");

        CoverLetter *resume = self.coverLetterList[indexPath.row];

        FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"CoverLetter"] child:@"CoverLetter"];
        FIRDatabaseReference* contactRef = [correspondenceRef child:resume.coverLetterId];
        [SVProgressHUD showWithStatus:@"Deleting..."];
        
        [contactRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            
            [SVProgressHUD dismiss];
            if (error) {
                //[self showSuccessAlert:false];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                
                
            } else {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"CoverLetter has been deleted." andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
                
                
                [self fetchAllResumes:^(CoverLetterList *cover) {
                    
                    if (cover == nil || cover.coverListArr.count == 0) {
                    } else {
                        self.coverLetterList = cover.coverListArr;
                        
                        [self.listingTbl reloadData];
                    }
                }];
                
            }
        }];
        
        
    }];
    button.backgroundColor = [UIColor redColor]; //arbitrary color
    
    
    
    return @[button]; //array with all the buttons you want. 1,2,3, etc...
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES; //tableview must be editable or nothing will work...
}

-(void)emailToParties:(UIButton*)sender {
    
    CoverLetter *coverLetter = self.coverLetterList[sender.tag];

    UIStoryboard *dashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:[NSBundle mainBundle]];
    MailListController *controller = [dashboard instantiateViewControllerWithIdentifier:NSStringFromClass([MailListController class])];
    controller.contactType = nil;
    controller.selectedCoverLetter = coverLetter;
    [self.navigationController pushViewController:controller animated:YES];
}


-(UIColor *)randomNiceColor {
    
    // Random hue from 0 to 359 degrees.
    CGFloat hue = (arc4random() % 360) / 359.0f;
    
    // Random saturation from 0.0 to 1.0
    CGFloat saturation = (float)arc4random() / UINT32_MAX;
    
    // Random brightness from 0.0 to 1.0
    CGFloat brightness = (float)arc4random() / UINT32_MAX;
    
    // Limit saturation and brightness to get a nice colors palette.
    // Remove the following 2 lines to generate a color from the full range.
    saturation = saturation < 0.5 ? 0.5 : saturation;
    brightness = brightness < 0.9 ? 0.9 : brightness;
    
    // Return a random UIColor.
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

@end
