//
//  AddJobController.m
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "AddJobController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "MailListController.h"
#import "UserModel.h"
#import "UserDetails.h"
#import "DocumentsCollectionViewCell.h"
#import "Return_to_Work-Swift.h"
#import <ELCImagePickerController/ELCImagePickerController.h>





@import Firebase;
@import FirebaseAuth;

@interface AddJobController ()<UIDocumentPickerDelegate, UINavigationControllerDelegate , ELCImagePickerControllerDelegate>{
    NSMutableArray *selectedImages;
}
@property (nonatomic, weak)IBOutlet UILabel *datelabel;
@property (nonatomic, weak)IBOutlet UITextField *jobTitle;
@property (nonatomic, strong) UIDocumentPickerViewController *docPicker;

@property (nonatomic, strong) NSString *filePath;
@end

@implementation AddJobController

- (void)viewDidLoad {
    
    selectedImages = [[NSMutableArray alloc] init];
    
    [super viewDidLoad];
    [self.documentsCollectionView registerClass:[DocumentsCollectionViewCell class] forCellWithReuseIdentifier:@"DocumentsCollectionViewCell"];

    self.tableView.rowHeight = 44;
    
    [self.view layoutIfNeeded];
    
    self.documentsCollectionView.dataSource = self;
    self.documentsCollectionView.delegate = self;
    [self.documentsCollectionView reloadData];
    // Do any additional setup after loading the view.
    [self.documentsCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];

    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd yyyy"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    self.datelabel.text = [dateFormatter stringFromDate:[NSDate date]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)addJob:(id)sender{
    if (_jobTitle.text.length) {
        if (![_filePath isEqualToString:@""]) {
            if (selectedImages.count == 0) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Job Log Image" message:@"No Job Log Was selected." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                [self presentViewController:alert animated:true completion:nil];
                return;
            }
            [SVProgressHUD showWithStatus:@"Please Wait..."];
            __block NSString *imageUrl = @"";
            __block int i = 0;
            for (UIImage *image in selectedImages) {
                i = i + 1;
                FIRStorageReference *storageRef = [[FIRStorage storage] reference];
                FIRStorageReference *imageRef = [storageRef child:[NSString stringWithFormat:@"%@%i.jpg", self.jobTitle.text, i]];
                [imageRef putData:[self compress:image] metadata:nil completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
                    i = i - 1;
                    if (!error) {
                        NSLog(@"image URL = %@" , [metadata.downloadURLs[0] absoluteString]);
                        if (imageUrl.length == 0) {
                            imageUrl = [metadata.downloadURLs[0] absoluteString];
                        }
                        else{
                            imageUrl = [NSString stringWithFormat:@"%@,%@" , imageUrl , [metadata.downloadURLs[0] absoluteString]];
                        }
                    }
                    if (i == 0) {
                        [SVProgressHUD dismiss];
                        [self addJobWithTitle:_jobTitle.text withDate:self.datelabel.text andResumePath:imageUrl];
                    }
                }];
            }
        }
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Job Title" message:@"No Job Title Was Entered." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:true completion:nil];
    }
}


-(IBAction)upload:(id)sender{
    UIAlertController* alertCtrl = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //Create an action
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = true;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    UIAlertAction *imageGallery = [UIAlertAction actionWithTitle:@"Image Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] init];
        elcPicker.maximumImagesCount = 100; //Set the maximum number of images to select to 100
        elcPicker.imagePickerDelegate = self;
        [self presentViewController:elcPicker animated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    //Add action to alertCtrl
    [alertCtrl addAction:camera];
    [alertCtrl addAction:imageGallery];
    [alertCtrl addAction:cancel];
    [self presentViewController:alertCtrl animated:YES completion:nil];
}





- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
    [selectedImages addObject:selectedImage];
    [self.documentsCollectionView reloadData];
    NSData *webData = [self compress:selectedImage];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:@"test.png"];
    [webData writeToFile:localFilePath atomically:YES];
    self.filePath = localFilePath;

    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark ELCImagePickerControllerDelegate Methods
- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info{
    for (NSDictionary *dict in info) {
        UIImage *selectedImage = dict[UIImagePickerControllerOriginalImage];
        [selectedImages addObject:selectedImage];
    }
    [self.documentsCollectionView reloadData];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    NSString* localPath = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    self.filePath = localPath;
}

-(void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2) {
        return 60;
    }
    return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Add Job Log";
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.text= @"Add Job Log";
}

-(IBAction)emailToParties:(UIButton*)sender {
    UIStoryboard *dashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:[NSBundle mainBundle]];
    MailListController *controller = [dashboard instantiateViewControllerWithIdentifier:NSStringFromClass([MailListController class])];
    [self.navigationController pushViewController:controller animated:YES];
}

-(IBAction)edit:(id)sender{
    printf("edit");
    [self.jobTitle becomeFirstResponder];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    printf("test");
}


//Update Resume
-(void)addJobWithTitle:(NSString*)title withDate:(NSString*)date andResumePath:imageUrl{

    FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Jobs"] child:@"Jobs"];
    FIRDatabaseReference* contactRef = [correspondenceRef childByAutoId];
    UserModel* userData = [UserDetails fetchUserData];
    NSDictionary *requestDict = @{@"jobTitle": title,
                                  @"jobDate": date,
                                  @"jobId": contactRef.key,
                                  @"filePath": imageUrl,
                                  @"userId": userData.userId
                                  };

    [SVProgressHUD showWithStatus:@"Please Wait..."];
    [contactRef setValue:requestDict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        [SVProgressHUD dismiss];
        if (error) {
            [self showSuccessAlert:false];
        } else {
            [self showSuccessAlert:true];
            
            self.jobTitle.text = @"";
            self.filePath = @"";
            [selectedImages removeAllObjects];
            [self.documentsCollectionView reloadData];
        }
    }];

}

-(void)showSuccessAlert:(Boolean)success{
    
    NSString *title = @"Success";
    NSString *message = @"Job Saved Successfully";
    
    if(!success){
        title = @"Failure";
        message = @"There was an error in saving job";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return selectedImages.count + 1;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    UIImageView *previousImageView = [cell viewWithTag:100];
    [previousImageView removeFromSuperview];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    imageView.clipsToBounds = YES;
    if (indexPath.row == selectedImages.count) {
        imageView.image = [UIImage imageNamed:@"upload_small"];
    }
    else{
        imageView.image = selectedImages[indexPath.row];
    }
    imageView.tag = 100;
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    [cell addSubview:imageView];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(50 , 50);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == selectedImages.count) {
        [self upload:nil];
    }
    else{
        UIAlertController* alertCtrl = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        //Create an action
        UIAlertAction *camera = [UIAlertAction actionWithTitle:@"View" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
            UIImageView *imageView = [cell viewWithTag:100];
            [[TestSwift new] showTheFullImageWithImageView:imageView viewController:self];
        }];
        UIAlertAction *imageGallery = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
            [selectedImages removeObjectAtIndex:indexPath.row];
            [self.documentsCollectionView reloadData];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        
        //Add action to alertCtrl
        [alertCtrl addAction:camera];
        [alertCtrl addAction:imageGallery];
        [alertCtrl addAction:cancel];
        [self presentViewController:alertCtrl animated:YES completion:nil];
    }
}


-(NSData*)compress:(UIImage*)image {
    int kMaxUploadSize = 150000;
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    NSData* imageData;
    do {
        imageData = UIImageJPEGRepresentation(image, compression);
        compression -= 0.10;
    } while ([imageData length] > kMaxUploadSize && compression > maxCompression);
    
    return imageData;
}


/*
 
 
 -(IBAction)uploadImage:(id)sender {
 
 UIImagePickerController *picker = [[UIImagePickerController alloc] init];
 picker.delegate = self;
 picker.allowsEditing = YES;
 picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
 [self presentViewController:picker animated:YES completion:NULL];
 }
 
 - (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
 
 UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
 
 NSData *webData = [self compress:selectedImage];
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:@"test.png"];
 [webData writeToFile:localFilePath atomically:YES];
 [self upload:localFilePath];
 [picker dismissViewControllerAnimated:YES completion:NULL];
 }
 
 - (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
 [picker dismissViewControllerAnimated:YES completion:NULL];
 }
 
 
 -(void)upload:(NSString*)filePath {
 
 NSURL *localFile = [[NSURL alloc] initFileURLWithPath:filePath];
 FIRStorageReference *storageRef = [[FIRStorage storage] reference];
 
 if (![filePath isEqualToString:@""]) {
 
 FIRStorageReference *imageRef = [storageRef child:[NSString stringWithFormat:@"%@.png", [self randomStringWithLength:10]]];
 
 [SVProgressHUD showWithStatus:@"Please Wait..."];
 [imageRef putFile:localFile metadata:nil completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
 
 [SVProgressHUD dismiss];
 NSLog(@"imageAdded");
 NSString *imageUrl = [metadata.downloadURLs[0] absoluteString];
 [self saveDocumentDetails:imageUrl];
 
 }];
 }
 
 }
 
 -(void)saveDocumentDetails:(NSString*)imageUrl {
 
 //    DocumentationModel *docModel = [[DocumentationModel alloc] initWithPlanDate:nil withPlanName:nil andPlanImage:imageUrl withExtraNote:@"" andPlanType:self.planType];
 //    [DocumentationAPIManager saveDocumentsForRTWWithDocumentDetails:docModel];
 
 //api call
 [self addExerciseWithImageUrl:imageUrl];
 }

 
 
 */

@end
