//
//  JobListingController.m
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "JobListingController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "JobListingCell.h"
#import "ResumeHomeController.h"

#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "Return_to_Work-Swift.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NewAllDocumentsViewController.h"
#import "MailListController.h"



@import Firebase;
@import FirebaseAuth;

@interface JobListingController ()

@end

@implementation JobListingController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.jobsTbl setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self fetchAllJobs:^(JobsList *jobs) {
        
        if (jobs == nil || jobs.jobArr.count == 0) {

        } else {
            self.jobList = jobs.jobArr;
            [self.jobsTbl setDelegate:self];
            [self.jobsTbl setDataSource:self];
            [self.jobsTbl reloadData];
        }
    }];
}

-(void)showErrorAlert{
    
    NSString *title = @"Oops";
    NSString *message = @"No previous jobs found";
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:true completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)fetchAllJobs:(void(^)(JobsList*))completion {
        
        [SVProgressHUD showWithStatus:@"Please wait..."];
        
        FIRDatabaseReference* contactRef = [[FIRDatabase database] reference];
        [[[contactRef child:@"Jobs"] child:@"Jobs"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            [SVProgressHUD dismiss];
            JobsList *jobs = [[JobsList alloc] initWithDict:snapshot.value];
            completion(jobs);
        }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.jobList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JobListingCell *cell = [self.jobsTbl dequeueReusableCellWithIdentifier:@"JobListingCell"];
    Jobs *job = self.jobList[indexPath.row];
    cell.jobDate.text = job.jobDate;
    cell.jobTitle.text = job.jobTitle;
    [cell.randomColorView setBackgroundColor:[self randomNiceColor]];
    
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:job.jobDocPath] placeholderImage:[UIImage imageNamed:@"placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        printf("image downloaded");
    }];
    
    
    
    cell.emailBtn.tag = indexPath.row;
    [cell.emailBtn addTarget:self action:@selector(emailToParties:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Jobs *job = self.jobList[indexPath.row];
    NewAllDocumentsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([NewAllDocumentsViewController class])];
    vc.job = job;
    vc.titleNavigation = @"Job Seeking";
    vc.titleMain = @"Job logs";
    vc.subTitle = @"";
    vc.specialTitle = @"Review Job Log";
    vc.iconImage = [UIImage imageNamed:@"job_loga"];
    
    [self.navigationController pushViewController:vc animated:YES];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        NSLog(@"Action to perform with Delete");
        
        
        
        
        Jobs *job = self.jobList[indexPath.row];

        FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Jobs"] child:@"Jobs"];
        FIRDatabaseReference* contactRef = [correspondenceRef child:job.jobId];
        
        [SVProgressHUD showWithStatus:@"Deleting..."];
        
        [contactRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            
            [SVProgressHUD dismiss];
            if (error) {
                //[self showSuccessAlert:false];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                
                
            } else {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Tracking has been deleted." andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
                
                
                [self fetchAllJobs:^(JobsList *jobs) {
                    
                    if (jobs == nil || jobs.jobArr.count == 0) {
                        [self.jobsTbl reloadData];
                    } else {
                        self.jobList = jobs.jobArr;
                        [self.jobsTbl reloadData];
                    }
                }];
                
            }
        }];
        
        
        
        
    }];
    button.backgroundColor = [UIColor redColor]; //arbitrary color
    
    
    
    return @[button]; //array with all the buttons you want. 1,2,3, etc...
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES; //tableview must be editable or nothing will work...
}

-(void)emailToParties:(UIButton*)sender {
    
    Jobs *job = self.jobList[sender.tag];

    UIStoryboard *dashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:[NSBundle mainBundle]];
    MailListController *controller = [dashboard instantiateViewControllerWithIdentifier:NSStringFromClass([MailListController class])];
    controller.contactType = nil;
    controller.selectedJob = job;
    [self.navigationController pushViewController:controller animated:YES];
}





-(UIColor *)randomNiceColor {
    // This method returns a random color in a range of nice ones,
    // using HSB coordinates.
    
    // Random hue from 0 to 359 degrees.
    CGFloat hue = (arc4random() % 360) / 359.0f;
    
    // Random saturation from 0.0 to 1.0
    CGFloat saturation = (float)arc4random() / UINT32_MAX;
    
    // Random brightness from 0.0 to 1.0
    CGFloat brightness = (float)arc4random() / UINT32_MAX;
    
    // Limit saturation and brightness to get a nice colors palette.
    // Remove the following 2 lines to generate a color from the full range.
    saturation = saturation < 0.5 ? 0.5 : saturation;
    brightness = brightness < 0.9 ? 0.9 : brightness;
    
    // Return a random UIColor.
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

@end
