//
//  JobListingCell.h
//  Return to Work
//
//  Created by Anshumaan on 15/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobListingCell : UITableViewCell
@property (nonatomic, weak)IBOutlet UILabel *jobTitle;
@property (nonatomic, weak)IBOutlet UILabel *jobDate;
@property (nonatomic, weak)IBOutlet UIView *randomColorView;


@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (weak, nonatomic) IBOutlet UIButton *emailBtn;



@end
