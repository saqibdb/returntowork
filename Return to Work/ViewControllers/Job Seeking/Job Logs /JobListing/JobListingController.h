//
//  JobListingController.h
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jobs.h"

@interface JobListingController : UIViewController<UITableViewDelegate , UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *jobsTbl;

//@property (nonatomic) Contact *selectedContact;
//@property (nonatomic, strong) Jobs *jobList;
@property (nonatomic, strong) NSMutableArray<Jobs*> *jobList;
-(void)fetchAllJobs:(void(^)(JobsList*))completion;


@end
