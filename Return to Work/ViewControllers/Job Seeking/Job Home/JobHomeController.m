//
//  JobHomeController.m
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "JobHomeController.h"
#import "JobLogHome.h"
#import "ResumeHomeController.h"
#import "CoverLetterHome.h"

@import Firebase;

@interface JobHomeController ()
@property (nonatomic, weak)IBOutlet UILabel *jobLogLbl;
@property (nonatomic, weak)IBOutlet UILabel *resumeLbl;
@property (nonatomic, weak)IBOutlet UILabel *coverLetterLbl;

@property (nonatomic, weak) IBOutlet UIView *jobBorderVw;
@property (nonatomic, weak) IBOutlet UIView *resumeBorderVw;
@property (nonatomic, weak) IBOutlet UIView *coverBorderVw;

@property (strong, nonatomic) FIRDatabaseReference *ref;
@end

@implementation JobHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createNavigationTitleViewWithTitle:@"Job Seeking"];
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    self.ref = [[FIRDatabase database] reference];
    
    [self addShadow:self.jobBorderVw];
    [self addShadow:self.resumeBorderVw];
    [self addShadow:self.coverBorderVw];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addShadow:(UIView*)borderVw {
    borderVw.layer.masksToBounds = NO;
    borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    borderVw.layer.shadowRadius = 2;
    borderVw.layer.shadowOpacity = 0.5;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIViewController *controller = nil;
    
    switch (indexPath.row) {
        case 0:
            controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([JobLogHome class])];
            break;
            
        case 1:
            controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ResumeHomeController class])];
            break;
            
        case 2:
            controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CoverLetterHome class])];
            break;
            
        default:
            break;
    }
    
    [self.navigationController pushViewController:controller animated:true];
    
}



-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"navigationLogo"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(20, -13, 60, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    // label
    UILabel *tmpTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 100, 20)];
    tmpTitleLabel.text = title;
    tmpTitleLabel.backgroundColor = [UIColor clearColor];
    tmpTitleLabel.textColor = [UIColor whiteColor];
    tmpTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGRect applicationFrame = CGRectMake(0, 0, 100, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    [newView addSubview:tmpTitleLabel];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    self.navigationItem.titleView = newView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
