//
//  NewAllDocumentsViewController.h
//  Return to Work
//
//  Created by iBuildx-Macbook on 08/02/2018.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jobs.h"
#import "Resume.h"
#import "DocumentationModel.h"


@interface NewAllDocumentsViewController : UIViewController


//Jobs *job CoverLetter *resume
@property (nonatomic, strong) Jobs *job;
@property (nonatomic, strong) CoverLetter *resumeMain;
@property (nonatomic, strong) Resume *resumeAnother;



@property (nonatomic, strong) DocumentationModel *medicalDocs;




@property (nonatomic, strong) NSString* titleNavigation;
@property (nonatomic, strong) NSString* titleMain;
@property (nonatomic, strong) NSString* subTitle;
@property (nonatomic, strong) NSString* specialTitle;

@property (nonatomic, strong) UIImage* iconImage;



@property (weak, nonatomic) IBOutlet UILabel *mainTitleText;

@property (weak, nonatomic) IBOutlet UILabel *subTitletext;

@property (weak, nonatomic) IBOutlet UIImageView *imageIconImageView;

@property (weak, nonatomic) IBOutlet UILabel *specialTitleText;



@end
