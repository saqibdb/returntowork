//
//  DashboardViewController.h
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBTileButton.h"




@interface DashboardViewController : UIViewController

@property (weak, nonatomic) IBOutlet DBTileButton *contactsBtn;
@property (weak, nonatomic) IBOutlet DBTileButton *correspondenceBtn;
@property (weak, nonatomic) IBOutlet DBTileButton *exersiceBtn;
@property (weak, nonatomic) IBOutlet DBTileButton *jobSeekingBtn;

@property (weak, nonatomic) IBOutlet DBTileButton *trackBtn;



- (IBAction)contactsAction:(DBTileButton *)sender;
- (IBAction)correspondenceAction:(DBTileButton *)sender;
- (IBAction)exersiceAction:(DBTileButton *)sender;
- (IBAction)jobSeekingAction:(id)sender;
- (IBAction)trackAction:(DBTileButton *)sender;









@end
