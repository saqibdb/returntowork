//
//  ViewController.h
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *userNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;


@property (weak, nonatomic) IBOutlet UIButton *signInBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;

- (IBAction)signInAction:(UIButton *)sender;
- (IBAction)registerAction:(UIButton *)sender;
- (IBAction)forgetAction:(UIButton *)sender;








@end

