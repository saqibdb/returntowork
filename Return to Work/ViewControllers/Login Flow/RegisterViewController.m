//
//  RegisterViewController.m
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "RegisterViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "UserDetails.h"
#import "AppDelegate.h"

@import Firebase;
@import FirebaseAuth;

@interface RegisterViewController ()

@property (nonatomic, strong) FIRDatabaseReference *ref;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ref = [[FIRDatabase database] reference];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)moveBack:(UIButton*)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)registerAction:(UIButton *)sender {
    
    
    if (self.emailTxt.text.length == 0) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Warning!" andText:@"No Email Entered." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    
    if (![self isValidEmail:self.emailTxt.text]) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Warning!" andText:@"Email is not valid." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    
    
    if (self.passwordTxt.text.length == 0) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Warning!" andText:@"No Password Entered." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }

    [SVProgressHUD showWithStatus:@"Signing In..."];
    
    
    NSData *plainData = [self.passwordTxt.text dataUsingEncoding:NSUTF8StringEncoding];
    NSData *base64Data = [plainData base64EncodedDataWithOptions:0];
    NSString *base64String = [NSString stringWithUTF8String:[base64Data bytes]];
    [[FIRAuth auth] createUserWithEmail:self.emailTxt.text password:[NSString stringWithFormat:@"%@\n", base64String] completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
        NSLog(@"%@", user);
        [SVProgressHUD dismiss];

        if (error) {
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Failed" andText:error.localizedDescription andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        } else {
            FIRDatabaseReference* userRef = [[[self.ref child:@"user"] child:@"users"] childByAutoId];
            NSDictionary* userDict = @{@"userEmail": self.emailTxt.text,
                                       @"userId": [NSString stringWithFormat:@"%@", userRef.key],
                                       @"userName": self.userNameTxt.text,
                                       @"userPassword": [NSString stringWithFormat:@"%@\n", base64String]
                                       };
            
            [userRef setValue:userDict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                [UserDetails saveUserData:userDict];
                AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
                [appDelegate startPage];
            }];
        }
    }];

 
    
//    [[FIRAuth auth] signInWithEmail:self.userNameTxt.text password:self.passwordTxt.text completion:^(FIRUser *_Nullable user, NSError *_Nullable error) {
//        [SVProgressHUD dismiss];
//        if (error) {
//        }
//        else{
//            FIRUserProfileChangeRequest *changeRequest = [user profileChangeRequest];
//            changeRequest.displayName = self.userNameTxt.text;
//            [changeRequest commitChangesWithCompletion:^(NSError *_Nullable error) {
//
//            }];
//            //[self performSegueWithIdentifier:@"ToDashboard" sender:self];
//        }
//    }];
    
    
}

-(BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
@end
