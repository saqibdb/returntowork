//
//  ResetPasswordViewController.m
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/02/2018.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "UserDetails.h"
#import "AppDelegate.h"

@import Firebase;
@import FirebaseAuth;


@interface ResetPasswordViewController ()
@property (nonatomic, strong) FIRDatabaseReference *ref;

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.ref = [[FIRDatabase database] reference];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sendResetAction:(UIButton *)sender {
    if (self.emailText.text.length == 0) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Warning!" andText:@"No Email Entered." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    
    if (![self isValidEmail:self.emailText.text]) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Warning!" andText:@"Email is not valid." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Sending reset Email...."];
    [[FIRAuth auth] sendPasswordResetWithEmail:self.emailText.text completion:^(NSError *_Nullable error) {
        [SVProgressHUD dismiss];
        if (error) {
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Failed" andText:error.localizedDescription andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        } else {
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Success" andText:@"Please check your email for the password Reset instructions." andCancelButton:NO forAlertType:AlertSuccess];
            [alert show];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];

}


-(BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
