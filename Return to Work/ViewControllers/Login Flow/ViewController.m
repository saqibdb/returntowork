//
//  ViewController.m
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "ViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "UserModel.h"
#import "UserDetails.h"
#import "AppDelegate.h"

@import Firebase;
@import FirebaseAuth;
@import FirebaseDatabase;


@interface ViewController ()

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)signInAction:(UIButton *)sender {
    
    
    if (self.userNameTxt.text.length == 0) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Warning!" andText:@"No Email Entered." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    
    if (![self isValidEmail:self.userNameTxt.text]) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Warning!" andText:@"Email is not valid." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    
    
    if (self.passwordTxt.text.length == 0) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Warning!" andText:@"No Password Entered." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    
    
    
    
    
    
    [SVProgressHUD showWithStatus:@"Signing In..."];
    if ([self.userNameTxt.text isEqualToString:@"test@ibuildx.com"]) {
        [[FIRAuth auth] signInWithEmail:self.userNameTxt.text password:self.passwordTxt.text completion:^(FIRUser *_Nullable user, NSError *_Nullable error) {
            [SVProgressHUD dismiss];
            
            if (error) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Failed" andText:error.localizedDescription andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }
            else{
                [self performSegueWithIdentifier:@"ToDashboard" sender:self];
            }
        }];
    } else {
    
    NSData *plainData = [self.passwordTxt.text dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainData base64EncodedStringWithOptions:0];
        [[FIRAuth auth] signInWithEmail:self.userNameTxt.text password:[NSString stringWithFormat:@"%@\n", base64String] completion:^(FIRUser *_Nullable user, NSError *_Nullable error) {
            
            
            if (error) {
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Failed" andText:error.localizedDescription andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }
            else{
                FIRDatabaseReference* userRef = [[[[FIRDatabase database] reference] child:@"user"] child:@"users"];
                [userRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                    [SVProgressHUD dismiss];
                    UserList* userList = [[UserList alloc] initWithDict:snapshot.value];
                    
                    UserModel* model = [userList getUsetDetailsFromEmail:self.userNameTxt.text andPassword:[NSString stringWithFormat:@"%@\n", base64String]];
                    if (model != nil) {
                        NSDictionary* userDict = @{@"userId": model.userId,
                                                   @"userName": model.userName,
                                                   @"userEmail": model.userEmail,
                                                   @"userPassword": model.userPassword};
                        [UserDetails saveUserData:userDict];
//                        [self performSegueWithIdentifier:@"ToDashboard" sender:self];
                        AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
                        [appDelegate startPage];
                    } else {
                        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initFadeAlertWithTitle:@"Failed" andText:@"No details found for the user." andCancelButton:NO forAlertType:AlertFailure];
                        [alert show];
                    }
                }];
  
            }
        }];
    }
}

- (IBAction)registerAction:(UIButton *)sender {
}

- (IBAction)forgetAction:(UIButton *)sender {
    
    
    NSLog(@"Forgot password");
}

-(BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
@end
