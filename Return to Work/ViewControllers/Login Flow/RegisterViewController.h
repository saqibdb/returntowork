//
//  RegisterViewController.h
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *userNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;
@property (weak, nonatomic) IBOutlet UITextField *emailTxt;


@property (weak, nonatomic) IBOutlet UIButton *registerBtn;

- (IBAction)registerAction:(UIButton *)sender;






@end
