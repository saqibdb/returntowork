//
//  ResetPasswordViewController.h
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/02/2018.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPasswordViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *emailText;






- (IBAction)backAction:(UIButton *)sender;
- (IBAction)sendResetAction:(UIButton *)sender;



@end
