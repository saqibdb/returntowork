//
//  CorrespondenceAPIManager.h
//  Return to Work
//
//  Created by Anshumaan on 21/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Correspondence.h"
#import "Contact.h"

@interface CorrespondenceAPIManager : NSObject

+(void)fetchAllAppointmentsWithCompletion:(void(^)(Appointments*))completion;

+(void)addAppointmentWithDate:(NSString*)date withTime:(NSString*)time andLocation:(NSString*)location appointmrntType:(NSString*)type withAppontment:(Contact*)appointment onError:(void (^)(NSError *err))errorBlock;

+(void)fetchAllContactWithCompletion:(void(^)(ContactList*))completion;


+(void)fetchContactWithType :(NSString *)type andValue :(NSString *)value andCompletion:(void(^)(ContactList*))completion;
@end
