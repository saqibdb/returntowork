//
//  CorrespondenceAPIManager.m
//  Return to Work
//
//  Created by Anshumaan on 21/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "CorrespondenceAPIManager.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "UserModel.h"
#import "UserDetails.h"

@import Firebase;
@import FirebaseAuth;

@implementation CorrespondenceAPIManager

+(void)fetchAllAppointmentsWithCompletion:(void(^)(Appointments*))completion {
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    FIRDatabaseReference* correspondenceRef = [[FIRDatabase database] reference];
    [[[correspondenceRef child:@"Appointment"] child:@"Appointment"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [SVProgressHUD dismiss];
        Appointments *appointments = [[Appointments alloc] initWithDict:snapshot.value];
        completion(appointments);
    }];
}

+(void)addAppointmentWithDate:(NSString*)date withTime:(NSString*)time andLocation:(NSString*)location appointmrntType:(NSString*)type withAppontment:(Contact*)appointment onError:(void (^)(NSError *err))errorBlock {
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    FIRDatabaseReference* correspondenceRef = [[[[FIRDatabase database] reference] child:@"Appointment"] child:@"Appointment"];
    FIRDatabaseReference* appointmentRef = [correspondenceRef childByAutoId];
    
    UserModel* userData = [UserDetails fetchUserData];
    
    NSDictionary *requestDict = @{@"appointmentDate": date,
                                  @"appointmentLocation": location,
                                  @"appointmentTime": time,
                                  @"appointmentType": type,
                                  @"contactId": appointmentRef.key,
                                  @"userId": userData.userId,
                                  @"appointContactName": appointment.contactName
                                  };
    [appointmentRef setValue:requestDict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        [SVProgressHUD dismiss];
        errorBlock(error);
    }];

    printf("test");
}


-(void)showSuccessAlert:(Boolean)success{
    
    NSString *title = @"Success";
    NSString *message = @"Progress Saved Successfully";
    
    if(!success){
        title = @"Failure";
        message = @"There was an error in saving progress";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:ok];
}

+(void)fetchAllContactWithCompletion:(void(^)(ContactList*))completion {
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    FIRDatabaseReference* contactRef = [[FIRDatabase database] reference];
    [[[contactRef child:@"Contact"] child:@"Contact"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [SVProgressHUD dismiss];
        ContactList *contacts = [[ContactList alloc] initWithDict:snapshot.value];
        completion(contacts);
    }];
}



+(void)fetchContactWithType :(NSString *)type andValue :(NSString *)value andCompletion:(void(^)(ContactList*))completion {
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    FIRDatabaseReference* contactRef = [[FIRDatabase database] reference];
    
    FIRDatabaseQuery *mainAll = [[contactRef child:@"Contact"] child:@"Contact"];
    
    
    FIRDatabaseQuery *allSubOrder = [mainAll queryOrderedByChild:type];
    FIRDatabaseQuery *specificContacts = [allSubOrder queryEqualToValue:value];
    
    
    [specificContacts observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [SVProgressHUD dismiss];
        ContactList *contacts = [[ContactList alloc] initWithDict:snapshot.value];
        
        
    
        completion(contacts);
    }];
}




@end
