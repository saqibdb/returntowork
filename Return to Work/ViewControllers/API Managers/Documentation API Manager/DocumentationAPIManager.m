//
//  DocumentationAPIManager.m
//  Return to Work
//
//  Created by Anshumaan on 24/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "DocumentationAPIManager.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "UserModel.h"
#import "UserDetails.h"

@implementation DocumentationAPIManager

+(void)saveDocumentsForRTWWithDocumentDetails:(DocumentationModel*)docDetails onError:(void (^)(NSError *err))errorBlock {
    
    FIRDatabaseReference *ref = [[[[FIRDatabase database] reference] child:@"RTWPlan"] child:@"RTWPlan"];
    FIRDatabaseReference *docRef = [ref childByAutoId];
    
    UserModel* userData = [UserDetails fetchUserData];
    
    NSDictionary* requestDict = @{@"planDate": docDetails.planDate,
                                  @"planId": docRef.key,
                                  @"planImage": docDetails.planImage,
                                  @"planName": docDetails.planName,
                                  @"userId": userData.userId,
                                  @"extraNote": docDetails.extraNote,
                                  @"planType": docDetails.planType
                                  };
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [docRef setValue:requestDict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        [SVProgressHUD dismiss];
        
        errorBlock(error);
    }];
    
}

+(void)fetchAllDocumentationWithCompletion:(void(^)(DocumentationList*))completion {
    FIRDatabaseReference *ref = [[[[FIRDatabase database] reference] child:@"RTWPlan"] child:@"RTWPlan"];
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [ref observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [SVProgressHUD dismiss];
        DocumentationList *documentations = [[DocumentationList alloc] initWithDict:snapshot.value];
        completion(documentations);
    }];
}

+(void)saveTrackingDetails:(TrackingModel*)trackModel {
    
    FIRDatabaseReference *ref = [[[[FIRDatabase database] reference] child:@"Tracking"] child:@"Tracking"];
    FIRDatabaseReference *docRef = [ref childByAutoId];
    
    UserModel* userData = [UserDetails fetchUserData];
    
    NSDictionary* requestDict = @{@"trackId": docRef.key,
                                  @"trackDate": trackModel.trackDate,
                                  @"userId": userData.userId,
                                  @"hours": trackModel.hours
                                  };
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [docRef setValue:requestDict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        [SVProgressHUD dismiss];
    }];
}

+(void)fetchAllTrackedDateWithCompletion:(void(^)(TrackingList* trackList))completion {
    FIRDatabaseReference *ref = [[[[FIRDatabase database] reference] child:@"Tracking"] child:@"Tracking"];
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [ref observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [SVProgressHUD dismiss];
        TrackingList *trackingList = [[TrackingList alloc] initWithDict:snapshot.value];
        completion(trackingList);
    }];
}

@end
