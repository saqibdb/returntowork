//
//  DocumentationAPIManager.h
//  Return to Work
//
//  Created by Anshumaan on 24/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentationModel.h"
#import "TrackingModel.h"

@import Firebase;
@import FirebaseAuth;

@interface DocumentationAPIManager : NSObject

+(void)saveDocumentsForRTWWithDocumentDetails:(DocumentationModel*)docDetails onError:(void (^)(NSError *err))errorBlock;

+(void)fetchAllDocumentationWithCompletion:(void(^)(DocumentationList*))completion;

+(void)saveTrackingDetails:(TrackingModel*)trackModel;

+(void)fetchAllTrackedDateWithCompletion:(void(^)(TrackingList* trackList))completion;

@end
