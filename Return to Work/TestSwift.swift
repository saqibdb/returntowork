//
//  TestSwift.swift
//  Return to Work
//
//  Created by iBuildx-Macbook on 25/01/2018.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

import UIKit
import SimpleImageViewer

class TestSwift: NSObject {

    
    public func showTheFullImage(imageView : UIImageView , viewController : UIViewController){
        let configuration = ImageViewerConfiguration { config in
            config.imageView = imageView
        }
        
        let imageViewerController = ImageViewerController(configuration: configuration)
        
        viewController.present(imageViewerController, animated: true)
    }
    
    
}
