//
//  ImageViewer.m
//  Return to Work
//
//  Created by Anshumaan on 31/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "ImageViewer.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ImageViewer ()

@property (nonatomic, weak) IBOutlet UIImageView *image;

@property (nonatomic, strong) NSString *imageUrl;

@end

@implementation ImageViewer

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.image sd_setImageWithURL:[NSURL URLWithString:self.imageUrl]];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchImageUrl:(NSString*)imgUrl {
    self.imageUrl = imgUrl;
}

-(IBAction)removeImgaeViewer:(UIButton*)sender {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
