//
//  ImageViewer.h
//  Return to Work
//
//  Created by Anshumaan on 31/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewer : UIViewController

-(void)fetchImageUrl:(NSString*)imgUrl;

@end
