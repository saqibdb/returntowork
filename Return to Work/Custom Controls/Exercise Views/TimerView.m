//
//  TimerView.m
//  Return to Work
//
//  Created by Anshumaan on 28/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "TimerView.h"

@interface TimerView () {
    NSTimer *timer;
    int timeInSeconds;
}

@property (nonatomic, weak) IBOutlet UILabel *timerLbl;

@end



@implementation TimerView

-(IBAction)startTimer:(UIButton*)sender {
    
    if(sender.tag == 0){
        timeInSeconds = 0;
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
        [sender setTitle:@"Stop Timer" forState:UIControlStateNormal];
        [sender setTag:1];
    }else{
        [timer invalidate];
        timer = nil;
        [self.timerLbl setText:[NSString stringWithFormat:@"%02d:%02d:%02d", 0, 0, 0]];
        [sender setTitle:@"Start Timer" forState:UIControlStateNormal];
        [sender setTag:0];
    }
}


- (void)timerTick:(NSTimer *)timer {
    timeInSeconds++;
    int hours = timeInSeconds/3600;
    int minutes = (timeInSeconds%3600)/60;
    int seconds = timeInSeconds%60;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.timerLbl setText:[NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds]];
    });
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
