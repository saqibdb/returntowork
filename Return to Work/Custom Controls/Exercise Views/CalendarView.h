//
//  CalendarView.h
//  Return to Work
//
//  Created by Anshumaan on 28/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKitUI/EventKitUI.h>
#import <EventKit/EventKit.h>

@interface CalendarView : UIView

@property (nonatomic, strong) EKEventStore *eventStore;

@end
