//
//  UserDetails.m
//  Return to Work
//
//  Created by Anshumaan on 07/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import "UserDetails.h"

@implementation UserDetails

+(void)saveUserData:(NSDictionary*)userDict {
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:userDict options:0 error:&err];
    NSString * jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:jsonString forKey:@"user"];
    [defaults synchronize];
}

+(UserModel*)fetchUserData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* jsonString = [defaults objectForKey:@"user"];
    if (jsonString != nil) {
        NSError *jsonError;
        NSData* jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* userDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&jsonError];
        UserModel* userDetails = [[UserModel alloc] initWithDict:userDict];
        return userDetails;
    }
    return nil;
}

@end
