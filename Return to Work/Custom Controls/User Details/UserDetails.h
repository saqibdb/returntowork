//
//  UserDetails.h
//  Return to Work
//
//  Created by Anshumaan on 07/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"

@interface UserDetails : NSObject

+(void)saveUserData:(NSDictionary*)userDict;
+(UserModel*)fetchUserData;

@end
