//
//  UserModel.m
//  Return to Work
//
//  Created by Anshumaan on 07/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

-(id)initWithDict:(NSDictionary*)dict {
    if (![dict isEqual:[NSNull null]]) {
        self.userId = dict[@"userId"];
        self.userName = dict[@"userName"];
        self.userEmail = dict[@"userEmail"];
        self.userPassword = dict[@"userPassword"];
    }
    return self;
}

@end



@implementation UserList

-(id)initWithDict:(NSDictionary*)dict {
    self.userList = [[NSMutableArray alloc] init];
    if (![dict isEqual:[NSNull null]]) {
        for(int index = 0; index < dict.allKeys.count; index++) {
            
            UserModel* userDetails = [[UserModel alloc] initWithDict:dict[dict.allKeys[index]]];
            [self.userList addObject:userDetails];
        }
    }
    return self;
}

-(NSDictionary*)getDictionaryFromId:(NSString*)userId andDict:(NSDictionary*)dict {
    for(int index = 0; index < dict.allKeys.count; index++) {
        if([userId isEqualToString:dict.allKeys[index]]) {
            return dict[userId];
        }
    }
    return nil;
}

-(UserModel*)getUsetDetailsFromEmail: (NSString*)emailId andPassword:(NSString*)password {
    for(int index = 0; index < self.userList.count; index++) {
        UserModel* model = self.userList[index];
        if ([model.userEmail isEqualToString:emailId] && [model.userPassword isEqualToString:password]) {
            return model;
        }
    }
    return nil;
}

@end
