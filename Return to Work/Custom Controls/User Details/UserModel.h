//
//  UserModel.h
//  Return to Work
//
//  Created by Anshumaan on 07/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userEmail;
@property (nonatomic, strong) NSString *userPassword;

-(id)initWithDict:(NSDictionary*)dict;

@end



@interface UserList: NSObject

@property (nonatomic, strong) NSMutableArray<UserModel*>* userList;

-(NSDictionary*)getDictionaryFromId:(NSString*)userId andDict:(NSDictionary*)dict;

-(UserModel*)getUsetDetailsFromEmail: (NSString*)emailId andPassword:(NSString*)password;

-(id)initWithDict:(NSDictionary*)dict;

@end
