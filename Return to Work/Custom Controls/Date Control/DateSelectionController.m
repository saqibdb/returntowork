//
//  DateSelectionController.m
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "DateSelectionController.h"

@interface DateSelectionController ()<UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, weak) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, strong, nullable) void (^callback)(id  _Nullable responseObject);
@end

@implementation DateSelectionController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadDatePicker :(void (^_Nullable)(id  _Nullable responseObject))completion {
    self.callback = completion;
}

-(IBAction)doneBtnTapped:(UIBarButtonItem*)sender {
    NSDate* selectedDate = self.datePicker.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Convert to new Date Format
    [dateFormatter setDateFormat:@"MMMM dd yyyy"];
    NSString *newDate = [dateFormatter stringFromDate:selectedDate];
    self.callback(newDate);
    [self.view removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
