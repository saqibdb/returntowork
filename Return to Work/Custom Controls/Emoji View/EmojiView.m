//
//  EmojiView.m
//  Return to Work
//
//  Created by Anshumaan on 07/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import "EmojiView.h"

@implementation EmojiView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)showImageHighlight:(BOOL)isHighlighted {
    [self.emojiImgVw setHighlighted:isHighlighted];
}

@end
