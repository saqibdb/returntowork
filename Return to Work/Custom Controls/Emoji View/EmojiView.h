//
//  EmojiView.h
//  Return to Work
//
//  Created by Anshumaan on 07/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmojiView : UIView

@property (nonatomic, weak) IBOutlet UIImageView* emojiImgVw;
-(void)showImageHighlight:(BOOL)isHighlighted;


@end
