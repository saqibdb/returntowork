//
//  ProgressListCell.h
//  Return to Work
//
//  Created by Anshumaan on 08/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InjuryProgressModel.h"

@interface ProgressListCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *progressNameLbl;
@property (nonatomic, weak) IBOutlet UILabel *feelingLbl;
@property (nonatomic, weak) IBOutlet UILabel *painIndexLbl;

@property (nonatomic, weak) IBOutlet UIView *colorView;

-(void)addDataOnUI:(InjuryProgressModel*)model andIndex:(int)index;

@end
