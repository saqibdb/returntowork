//
//  ProgressListCell.m
//  Return to Work
//
//  Created by Anshumaan on 08/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import "ProgressListCell.h"

@implementation ProgressListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)addDataOnUI:(InjuryProgressModel*)model andIndex:(int)index {
    [self.progressNameLbl setText:[NSString stringWithFormat:@"%@", model.progressDate]];
    
    
    /*
    if ([model.feelingIndex isEqual:[NSNull null]] || model.feelingIndex == nil) {
        [self.feelingLbl setText:[self getFeelingFromPainIndex:model.painIndex]];
    }
    else{
        [self.feelingLbl setText:[self getFeelingFromPainIndex:model.feelingIndex]];
    }
    
    [self.painIndexLbl setText:[NSString stringWithFormat:@"Pain = %@", model.painIndex]];
     */
}

-(NSString*)getFeelingFromPainIndex:(NSString*)painIndex {
    
    
    
    if ([painIndex isEqualToString:@"1"]) {
        return @"Very Happy";
    } else if ([painIndex isEqualToString:@"2"]) {
        return @"Happy";
    } else if ([painIndex isEqualToString:@"3"] ) {
        return @"OK";
    } else if ([painIndex isEqualToString:@"4"]) {
        return @"Sad";
    } else if ([painIndex isEqualToString:@"5"] ) {
        return @"Very Sad";
    }
    return @"";
}

@end
