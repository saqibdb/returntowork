//
//  ContactsTableViewCell.m
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "ContactsTableViewCell.h"

@implementation ContactsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)addDataOnUI:(Contact*)contactDetails {
    self.contactName.text = contactDetails.name;
    [self addShadow];
    self.imageCellIcon.image = contactDetails.icon;
}

-(void)addData:(Correspondence*)details {
    self.contactName.text = details.name;
    [self addShadow];
    [self.imageCellIcon setImage:[UIImage imageNamed:details.icon]];
}


-(void)addShadow {
    self.borderVw.layer.masksToBounds = NO;
    self.borderVw.layer.shadowOffset = CGSizeMake(1, 1);
    self.borderVw.layer.shadowRadius = 2;
    self.borderVw.layer.shadowOpacity = 0.5;
}

@end
