//
//  DocumentsCollectionViewCell.h
//  Return to Work
//
//  Created by iBuildx-Macbook on 08/02/2018.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocumentsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *documentImageView;

@end
