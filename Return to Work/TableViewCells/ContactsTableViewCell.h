//
//  ContactsTableViewCell.h
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CCMBorderView/CCMBorderView.h>
#import "Contact.h"
#import "Correspondence.h"

@interface ContactsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageCellIcon;
@property (weak, nonatomic) IBOutlet UILabel *contactName;

@property (weak, nonatomic) IBOutlet UILabel *contactEdit;

@property (weak, nonatomic) IBOutlet UIView *colorView;

@property (weak, nonatomic) IBOutlet CCMBorderView *borderVw;
@property (weak, nonatomic) IBOutlet UILabel *emailSentLbl;

@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@property (weak, nonatomic) IBOutlet UIButton *emailBtn;




-(void)addShadow;
-(void)addDataOnUI:(Contact*)contactDetails;
-(void)addData:(Correspondence*)details;

@end
