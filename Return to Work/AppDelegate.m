//
//  AppDelegate.m
//  Return to Work
//
//  Created by iBuildx-Macbook on 07/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "AppDelegate.h"
#import "UserDetails.h"
#import "UserModel.h"

@interface AppDelegate ()

@property(strong, nonatomic) UITabBarController* tabBarController;

@end

@import Firebase;

@implementation AppDelegate



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [FIRApp configure];
    [self checkUserData];
    return YES;
}

-(void)checkUserData {
    
    UserModel* model = [UserDetails fetchUserData];
    if (model != nil) {
        [self startPage];
    } else {
        
    }
}

-(void)startPage {
    self.tabBarController = [[UITabBarController alloc] init];
    UIStoryboard* mainStoryboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    self.tabBarController = [mainStoryboard instantiateViewControllerWithIdentifier:@"RTWTabBar"];
    self.window.rootViewController = self.tabBarController;
    
    [UIView transitionWithView:_window duration:0.6 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.window.rootViewController = self.tabBarController;
    } completion:nil];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
