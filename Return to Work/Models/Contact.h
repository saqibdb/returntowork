//
//  Contact.h
//  Return to Work
//
//  Created by iBuildx-Macbook on 08/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UserModel.h"
#import "UserDetails.h"

@interface Contact : NSObject


@property (nonatomic) NSString *name;
@property (nonatomic) NSString *lastEdit;
@property (nonatomic) UIImage *icon;


@property (nonatomic) NSString *address;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *phone;



@property (nonatomic) NSString *contactAddress;
@property (nonatomic) NSString *contactEmail;
@property (nonatomic) NSString *contactId;
@property (nonatomic) NSString *contactName;
@property (nonatomic) NSString *contactPhone;
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *userId;
@property (nonatomic) BOOL isSelected;
@property (nonatomic) BOOL isEmailSent;



- (id)initWithName:(NSString *)_name andLastEdit :(NSString *)_lastEdit andIcon :(UIImage *)_icon;
- (id)initWithName:(NSString *)_name andAddress :(NSString *)_address andEmail :(NSString *)_email andPhone :(NSString *)_phone;
- (id)initWithDict:(NSDictionary*)contactDict;


@end


@interface ContactList: NSObject

@property (nonatomic, strong) NSMutableArray<Contact*>* contactArr;

-(id)initWithDict:(NSDictionary*)contacts;

-(NSMutableArray*)fetchContactsWithType:(NSString*)type;

-(NSMutableArray*)fetchContactsFromUserID:(NSString*)userId;

@end
