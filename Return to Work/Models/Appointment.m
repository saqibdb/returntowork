//
//  Appointment.m
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "Appointment.h"

@interface Appointment()
@property (nonatomic) NSString *appointmentDate;
@property (nonatomic) NSString *appointmentTime;
@property (nonatomic) NSString *appointmentLocation;
//@property (nonatomic) NSString *
@end

@implementation Appointment


+(Appointment *)appointmentWithDate:(NSString *)date time:(NSString *)time andLocation:(NSString *)location{
    
    Appointment *appointment = [[Appointment alloc] init];
    appointment.appointmentDate = date;
    appointment.appointmentTime = time;
    appointment.appointmentLocation = location;
    return appointment;
}

@end
