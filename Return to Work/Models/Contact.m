//
//  Contact.m
//  Return to Work
//
//  Created by iBuildx-Macbook on 08/12/2017.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "Contact.h"

@implementation Contact

- (id)initWithName:(NSString *)_name andLastEdit :(NSString *)_lastEdit andIcon :(UIImage *)_icon {
    self = [super init];
    if(self){
        self.name = _name;
        self.lastEdit = _lastEdit;
        self.icon = _icon;
    }
    return self;
}



- (id)initWithName:(NSString *)_name andAddress :(NSString *)_address andEmail :(NSString *)_email andPhone :(NSString *)_phone {
    self = [super init];
    if(self){
        self.name = _name;
        self.address = _address;
        self.email = _email;
        self.phone = _phone;
    }
    return self;
}

//contactAddress:
//"vzvzz"
//close
//contactEmail:
//"gzvss"
//contactId:
//"-L0iauEp7dUXfCNW92te"
//contactName:
//"DSS and"
//contactPhone:
//"vzvz"
//type:
//"Employer"
//userId:
//"-L0hIaZqY06JUKiKsRBO"

-(id)initWithDict:(NSDictionary*)contactDict {
    if (![contactDict isEqual:[NSNull null]]) {
        self.contactAddress = contactDict[@"contactAddress"];
        self.contactEmail = contactDict[@"contactEmail"];
        self.contactId = contactDict[@"contactId"];
        self.contactName = contactDict[@"contactName"];
        self.contactPhone = contactDict[@"contactPhone"];
        self.type = contactDict[@"type"];
        self.userId = contactDict[@"userId"];
        self.isSelected = false;
    }
    return self;
}

@end


@implementation ContactList

-(id)initWithDict:(NSDictionary*)contacts {
    
    UserModel* userData = [UserDetails fetchUserData];
    self.contactArr = [[NSMutableArray alloc] init];
    if (![contacts isEqual:[NSNull null]]) {
        for(int index = 0; index < contacts.allKeys.count; index++) {
            Contact* contact = [[Contact alloc] initWithDict:contacts[contacts.allKeys[index]]];
            if ([contact.userId isEqualToString:userData.userId]) {
                [self.contactArr addObject:contact];
            }
        }
    }
    return self;
}

-(NSMutableArray*)fetchContactsWithType:(NSString*)type {
    
    NSMutableArray* employerArr = [[NSMutableArray alloc] init];
    for(int index = 0; index < self.contactArr.count; index++) {
        Contact* appointment = self.contactArr[index];
        if ([appointment.type  isEqual:type]) {
            [employerArr addObject:appointment];
        }
    }
    return employerArr;
}

-(NSMutableArray*)fetchContactsFromUserID:(NSString*)userId {
    
    NSMutableArray* employerArr = [[NSMutableArray alloc] init];
    for(Contact* contact in self.contactArr) {
        if (contact.userId == userId) {
            [employerArr addObject:contact];
        }
    }
    return employerArr;
}



@end
