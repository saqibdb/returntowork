//
//  InjuryProgressModel.m
//  Return to Work
//
//  Created by Anshumaan on 08/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import "InjuryProgressModel.h"

@implementation InjuryProgressModel


-(id)initWithDict:(NSDictionary *)injuryDict {
    if (![injuryDict isEqual:[NSNull null]]) {
        self.injuryId = injuryDict[@"injuryId"];
        self.painIndex = injuryDict[@"painIndex"];
        self.notes = injuryDict[@"notes"];
        self.feelingIndex = injuryDict[@"feelingIndex"];
        self.progressDate = injuryDict[@"progressDate"];
    }
    return self;
}

@end


@implementation InjuryProgressList

-(id)initWithDict:(NSDictionary *)dict {
    self.progressList = [[NSMutableArray alloc] init];
    if (![dict isEqual:[NSNull null]]) {
        for(int index = 0; index < dict.allKeys.count; index++) {
            InjuryProgressModel* progress = [[InjuryProgressModel alloc] initWithDict:dict[dict.allKeys[index]]];
            [self.progressList addObject:progress];
        }
    }
    return self;
}

@end


