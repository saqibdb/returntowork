//
//  InjuryProgressModel.h
//  Return to Work
//
//  Created by Anshumaan on 08/01/18.
//  Copyright © 2018 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InjuryProgressModel : NSObject

@property (nonatomic, strong) NSString *injuryId;
@property (nonatomic, strong) NSString *painIndex;
@property (nonatomic, strong) NSString *notes;

@property (nonatomic, strong) NSString *feelingIndex;
@property (nonatomic, strong) NSString *progressDate;

//progressDate

-(id)initWithDict:(NSDictionary*)injuryDict;

@end


@interface InjuryProgressList: NSObject

@property (nonatomic, strong) NSMutableArray<InjuryProgressModel*>* progressList;

-(id)initWithDict:(NSDictionary*)dict;

@end
