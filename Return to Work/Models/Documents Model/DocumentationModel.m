//
//  DocumentationModel.m
//  Return to Work
//
//  Created by Anshumaan on 24/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "DocumentationModel.h"
#import "UserDetails.h"
#import "UserModel.h"

@implementation DocumentationModel

-(id)initWithPlanDate:(NSString*)planDate withPlanName:(NSString*)planName andPlanImage:(NSString*)planImage withExtraNote:(NSString*)extraNote andPlanType:(NSString*)planType {
    
    self.planId = @"";
    self.planDate = planDate;
    self.planName = planName;
    self.planImage = planImage;
    self.userId = @"";
    self.extraNote = extraNote;
    self.planType = planType;
    
    return self;
}


-(id)initWithDictionary:(NSDictionary*)documentDict {
    if (![documentDict isEqual:[NSNull null]]) {
        self.planName = documentDict[@"planName"];
        self.planId = documentDict[@"planId"];
        self.planImage = documentDict[@"planImage"];
        self.planDate = documentDict[@"planDate"];
        self.userId = documentDict[@"userId"];
        self.planType = documentDict[@"planType"];
        self.extraNote = documentDict[@"extraNote"];
    }
    return self;
}

@end


@implementation DocumentationList

-(id)initWithDict:(NSDictionary*)documentList {
    UserModel* userData = [UserDetails fetchUserData];
    self.documentArr = [[NSMutableArray alloc] init];
    if (![documentList isEqual:[NSNull null]]) {
        for(int index = 0; index < documentList.allKeys.count; index++) {
            DocumentationModel* document = [[DocumentationModel alloc] initWithDictionary:documentList[documentList.allKeys[index]]];
            if ([userData.userId isEqualToString:document.userId]) {
                [self.documentArr addObject:document];
            }
        }
    }
    return self;
}

-(NSMutableArray<DocumentationModel*>*)fetchDocumentation:(NSString*)type {
    
    NSMutableArray* docArr = [[NSMutableArray alloc] init];
    for(int index = 0; index < self.documentArr.count; index++) {
        DocumentationModel* documentation = self.documentArr[index];
        if ([documentation.planType containsString:type.lowercaseString]) {
            [docArr addObject:documentation];
        }
    }
    return docArr;
}

@end


