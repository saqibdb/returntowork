//
//  DocumentationModel.h
//  Return to Work
//
//  Created by Anshumaan on 24/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DocumentationModel : NSObject

@property (nonatomic, strong) NSString* planDate;
@property (nonatomic, strong) NSString* planId;
@property (nonatomic, strong) NSString* planImage;
@property (nonatomic, strong) NSString* planName;
@property (nonatomic, strong) NSString* userId;
@property (nonatomic, strong) NSString* extraNote;
@property (nonatomic, strong) NSString* planType;


-(id)initWithDictionary:(NSDictionary*)documentDict;
-(id)initWithPlanDate:(NSString*)planDate withPlanName:(NSString*)planName andPlanImage:(NSString*)planImage withExtraNote:(NSString*)extraNote andPlanType:(NSString*)planType;

@end


@interface DocumentationList: NSObject

@property (nonatomic, strong) NSMutableArray<DocumentationModel*>* documentArr;
-(id)initWithDict:(NSDictionary*)documentList;
-(NSMutableArray<DocumentationModel*>*)fetchDocumentation:(NSString*)type;

@end
