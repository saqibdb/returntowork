//
//  Execise.m
//  Return to Work
//
//  Created by Ramesh Kumar on 25/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "Exercise.h"
#import "UserModel.h"
#import "UserDetails.h"

@implementation Exercise

-(id)initWithDict:(NSDictionary*)exerciseDict {
    if (![exerciseDict isEqual:[NSNull null]]) {
        self.exerciseId = exerciseDict[@"ExerciseId"];
        self.exerciseUrl = exerciseDict[@"URL"];
        self.userId = exerciseDict[@"userId"];
    }
    return self;
}
@end


@implementation ExerciseList

-(id)initWithDict:(NSDictionary*)exercises {
    
    UserModel* userData = [UserDetails fetchUserData];
    self.exerciseArr = [[NSMutableArray alloc] init];
    if (![exercises isEqual:[NSNull null]]) {
        for(int index = 0; index < exercises.allKeys.count; index++) {
            Exercise* exercise = [[Exercise alloc] initWithDict:exercises[exercises.allKeys[index]]];
            if ([userData.userId isEqualToString:exercise.userId])
                [self.exerciseArr addObject:exercise];
        }
    }
    return self;
}

@end
