//
//  Resume.h
//  Return to Work
//
//  Created by Ramesh Kumar on 25/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>

//@interface Resume : NSObject
//
//@end



@interface Resume : NSObject
@property (nonatomic) NSString *resumeId;
@property (nonatomic) NSString *resumeTitle;
@property (nonatomic) NSString *resumeUploadDate;
@property (nonatomic) NSString *resumePath;
@property (nonatomic) NSString *userId;

-(id)initWithDict:(NSDictionary*)resumeDict;

@end

@interface ResumeList: NSObject
@property (nonatomic, strong) NSMutableArray<Resume*>* resumeArr;
-(id)initWithDict:(NSDictionary*)resume;
@end


@interface CoverLetter : NSObject
@property (nonatomic) NSString *coverLetterId;
@property (nonatomic) NSString *coverLetterTitle;
@property (nonatomic) NSString *coverLetterUploadDate;
@property (nonatomic) NSString *coverLetterPath;
@property (nonatomic) NSString *userId;

-(id)initWithDict:(NSDictionary*)coverLetterDict;

@end

@interface CoverLetterList: NSObject
@property (nonatomic, strong) NSMutableArray<CoverLetter*>* coverListArr;
-(id)initWithDict:(NSDictionary*)coverLetterDict;
@end
