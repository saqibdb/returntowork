//
//  Correspondence.m
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "Correspondence.h"
#import "UserModel.h"
#import "UserDetails.h"

@implementation Correspondence


- (id)initWithName:(NSString *)_name withDescription: (NSString *)_description andLastEdit :(NSString *)_lastEdit andIcon :(NSString *)_icon {
    self = [super init];
    if(self){
        self.name = _name;
        self.desc = _description;
        self.lastEdit = _lastEdit;
        self.icon = _icon;
    }
    return self;
}


-(id)initWithDict:(NSDictionary *)appointmentDict {
    if (![appointmentDict isEqual:[NSNull null]]) {
        self.appointmentDate = appointmentDict[@"appointmentDate"];
        self.appontmentLocation = appointmentDict[@"appointmentLocation"];
        self.appointmentTime = appointmentDict[@"appointmentTime"];
        self.appointmentType = appointmentDict[@"appointmentType"];
        self.contactId = appointmentDict[@"contactId"];
        self.userId = appointmentDict[@"userId"];
        self.appointmentContactName = appointmentDict[@"appointContactName"];
    }
    return self;
}

@end

@implementation Appointments

-(id)initWithDict:(NSDictionary *)appointments {
    UserModel* userData = [UserDetails fetchUserData];
    self.appointmentArr = [[NSMutableArray alloc] init];
    if (![appointments isEqual:[NSNull null]]) {
        for(int index = 0; index < appointments.allKeys.count; index++) {
            Correspondence* appointment = [[Correspondence alloc] initWithDict:appointments[appointments.allKeys[index]]];
            if ([userData.userId isEqualToString:appointment.userId]) {
                [self.appointmentArr addObject:appointment];
            }
        }
    }
    return self;
}

-(NSMutableArray*)fetchAppointmentsWithType:(NSString*)type {
    
    NSMutableArray* employerArr = [[NSMutableArray alloc] init];
    for(int index = 0; index < self.appointmentArr.count; index++) {
        Correspondence* appointment = self.appointmentArr[index];
        if ([appointment.appointmentType  isEqual:type]) {
            [employerArr addObject:appointment];
        }
    }
    return employerArr;
}


@end
