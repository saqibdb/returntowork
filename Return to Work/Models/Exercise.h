//
//  Execise.h
//  Return to Work
//
//  Created by Ramesh Kumar on 25/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Exercise : NSObject
@property (nonatomic) NSString *exerciseId;
@property (nonatomic) NSString *exerciseUrl;
@property (nonatomic) NSString *userId;

-(id)initWithDict:(NSDictionary*)exercise;

@end

@interface ExerciseList: NSObject
@property (nonatomic, strong) NSMutableArray<Exercise*>* exerciseArr;
-(id)initWithDict:(NSDictionary*)exercises;
@end
