//
//  Appointment.h
//  Return to Work
//
//  Created by Anshumaan on 14/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Appointment : NSObject{
    
}

+(Appointment *)appointmentWithDate:(NSString *)date time:(NSString *)time andLocation:(NSString *)location;

@end
