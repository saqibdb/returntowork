//
//  Correspondence.h
//  Return to Work
//
//  Created by Anshumaan on 17/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Correspondence : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *lastEdit;
@property (nonatomic) NSString *icon;
@property (nonatomic) NSString *desc;


#pragma Response Parameters

@property (nonatomic) NSString *appointmentDate;
@property (nonatomic) NSString *appontmentLocation;
@property (nonatomic) NSString *appointmentTime;
@property (nonatomic) NSString *appointmentType;
@property (nonatomic) NSString *contactId;
@property (nonatomic) NSString *userId;
@property (nonatomic) NSString *appointmentContactName;

- (id)initWithName:(NSString *)_name withDescription: (NSString*)_description andLastEdit :(NSString *)_lastEdit andIcon :(NSString *)_icon;

-(id)initWithDict:(NSDictionary *)appointmentDict;

@end


@interface Appointments: NSObject

@property (nonatomic, strong) NSMutableArray<Correspondence*>* appointmentArr;

-(id)initWithDict:(NSDictionary *)appointments;

-(NSMutableArray*)fetchAppointmentsWithType:(NSString*)type;

@end

