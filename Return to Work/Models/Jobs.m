//
//  Jobs.m
//  Return to Work
//
//  Created by Ramesh Kumar on 25/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "Jobs.h"
#import "UserDetails.h"
#import "UserModel.h"

@implementation Jobs

-(id)initWithDict:(NSDictionary*)jobDict {
    if (![jobDict isEqual:[NSNull null]]) {
        self.jobId = jobDict[@"jobId"];
        self.jobTitle = jobDict[@"jobTitle"];
        self.jobDate = jobDict[@"jobDate"];
        self.jobDocPath = jobDict[@"filePath"];
        self.userId = jobDict[@"userId"];
    }
    return self;
}
@end


@implementation JobsList

-(id)initWithDict:(NSDictionary*)jobs {
    
    UserModel* userData = [UserDetails fetchUserData];
    self.jobArr = [[NSMutableArray alloc] init];
    if (![jobs isEqual:[NSNull null]]) {
        for(int index = 0; index < jobs.allKeys.count; index++) {
            Jobs* job = [[Jobs alloc] initWithDict:jobs[jobs.allKeys[index]]];
            if ([job.userId isEqualToString:userData.userId]) {
                [self.jobArr addObject:job];
            }
        }
    }
    return self;
}

@end
