//
//  Jobs.h
//  Return to Work
//
//  Created by Ramesh Kumar on 25/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Jobs : NSObject
@property (nonatomic) NSString *jobId;
@property (nonatomic) NSString *jobTitle;
@property (nonatomic) NSString *jobDate;
@property (nonatomic) NSString *jobDocPath;
@property (nonatomic) NSString *userId;

-(id)initWithDict:(NSDictionary*)jobDict;

@end

@interface JobsList: NSObject
@property (nonatomic, strong) NSMutableArray<Jobs*>* jobArr;
-(id)initWithDict:(NSDictionary*)jobs;
@end
