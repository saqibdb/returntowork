//
//  Resume.m
//  Return to Work
//
//  Created by Ramesh Kumar on 25/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "Resume.h"

@implementation Resume

-(id)initWithDict:(NSDictionary*)resumeDict {
    if (![resumeDict isEqual:[NSNull null]]) {
        self.resumeId = resumeDict[@"resumeId"];
        self.resumeTitle = resumeDict[@"resumeTitle"];
        self.resumeUploadDate = resumeDict[@"resumeUploadDate"];
        self.resumePath = resumeDict[@"resumePath"];
        self.userId = resumeDict[@"userId"];
    }
    return self;
}
@end


@implementation ResumeList

-(id)initWithDict:(NSDictionary*)resume {
    
    self.resumeArr = [[NSMutableArray alloc] init];
    if (![resume isEqual:[NSNull null]]) {
        for(int index = 0; index < resume.allKeys.count; index++) {
            Resume* rsume = [[Resume alloc] initWithDict:resume[resume.allKeys[index]]];
            [self.resumeArr addObject:rsume];
        }
    }
    return self;
}

@end

@implementation CoverLetter

-(id)initWithDict:(NSDictionary*)coverLetterDict {
    if (![coverLetterDict isEqual:[NSNull null]]) {
        self.coverLetterId = coverLetterDict[@"coverLetterId"];
        self.coverLetterTitle = coverLetterDict[@"coverLetterTitle"];
        self.coverLetterUploadDate = coverLetterDict[@"coverLetterUploadDate"];
        self.coverLetterPath = coverLetterDict[@"coverLetterPath"];
        self.userId = coverLetterDict[@"userId"];
    }
    return self;
}
@end


@implementation CoverLetterList

-(id)initWithDict:(NSDictionary*)coverLetterDict {
    
    self.coverListArr = [[NSMutableArray alloc] init];
    if (![coverLetterDict isEqual:[NSNull null]]) {
        for(int index = 0; index < coverLetterDict.allKeys.count; index++) {
            CoverLetter* coverLetter = [[CoverLetter alloc] initWithDict:coverLetterDict[coverLetterDict.allKeys[index]]];
            [self.coverListArr addObject:coverLetter];
        }
    }
    return self;
}

@end


