//
//  TrackingModel.h
//  Return to Work
//
//  Created by Anshumaan on 25/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrackingModel : NSObject

@property (nonatomic, strong) NSString *hours;
@property (nonatomic, strong) NSString *trackDate;
@property (nonatomic, strong) NSString *trackId;

-(id)initWithTrackDate:(NSString*)date andTrackHours:(NSString*)hours;
-(id)initWithDict:(NSDictionary*)trackDict;

@end


@interface TrackingList : NSObject

@property (nonatomic, strong) NSMutableArray<TrackingModel*>* trackingArr;
-(id)initWithDict:(NSDictionary*)trackList;

@end
