//
//  TrackingModel.m
//  Return to Work
//
//  Created by Anshumaan on 25/12/17.
//  Copyright © 2017 iBuildx-Macbook. All rights reserved.
//

#import "TrackingModel.h"

@implementation TrackingModel

-(id)initWithTrackDate:(NSString*)date andTrackHours:(NSString*)hours {
    self.hours = hours;
    self.trackDate = date;
    return self;
}

-(id)initWithDict:(NSDictionary*)trackDict {
    if (![trackDict isEqual:[NSNull null]]) {
        self.hours = trackDict[@"hours"];
        self.trackDate = trackDict[@"trackDate"];
        self.trackId = trackDict[@"trackId"];
    }
    return self;
}

@end


@implementation TrackingList

-(id)initWithDict:(NSDictionary*)trackList {
    self.trackingArr = [[NSMutableArray alloc] init];
    if (![trackList isEqual:[NSNull null]]) {
        for(int index = 0; index < trackList.allKeys.count; index++) {
            TrackingModel* trackModel = [[TrackingModel alloc] initWithDict:trackList[trackList.allKeys[index]]];
            [self.trackingArr addObject:trackModel];
        }
    }
    return self;
}

@end
